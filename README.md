<p align="center">
    <a href="https://laravel.com" target="_blank">
        <img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400">
    </a>
    <a href="https://getstisla.com">
        <img src="https://avatars2.githubusercontent.com/u/45754626?s=75&v=4" alt="Stisla logo" width="100">
    </a>
</p>

<!-- <p align="center">
  <a href="https://getstisla.com">
    <img src="https://avatars2.githubusercontent.com/u/45754626?s=75&v=4" alt="Stisla logo" width="75" height="75">
  </a>
</p> -->

<!-- <h1 align="center">Stisla</h1> -->

## How to Install This Package
- composer install
- set your .env it can be installed with mariadb or postgresql
- php artisan optimize
- php artisan migrate
- php artisan db:seed
- php artisan serve

## Login & Password
- username/email = admin/admin@admin.com,  password = password
- username/email = agent01/agent@agent.com,  password = password
- username/email = branch01/branch@branch.com , password = password

## Flow Ticket
- Agar bisa menggunakan program ini silahkan login menggunakan user admin
- lalu masuk ke menu Setting->User lalu edit Branch
- ubah Kolom Branch menjadi 0100-Will Sunter

## On Progress Menu
- All Reporting
- Dynamic Role Menu

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

Thank you for stisla for this template.
[Stisla Documentation](https://github.com/stisla/stisla)

<html>
<head>
	<meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<style type="text/css">
	body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:x-small }
	a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  }
	a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  }
	comment { display:none;  }
</style>
<body>
<table>
	<colgroup width="73"></colgroup>
	<colgroup width="184"></colgroup>
	<colgroup width="12"></colgroup>
	<colgroup width="7"></colgroup>
	<colgroup span="5" width="68"></colgroup>
	<colgroup width="86"></colgroup>
	<colgroup width="19"></colgroup>
	<colgroup width="11"></colgroup>
	<colgroup width="48"></colgroup>
	<colgroup width="44"></colgroup>
	<colgroup width="14"></colgroup>
	<colgroup width="25"></colgroup>
	<colgroup span="6" width="26"></colgroup>
	<colgroup width="42"></colgroup>
	<colgroup width="19"></colgroup>
	<colgroup width="64"></colgroup>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" rowspan=3 align="center" valign=middle><font face="Arial Narrow" color="#000000"><br></font></td>
		<td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=10 rowspan=3 align="center" valign=middle><b><font face="Arial Narrow" size=4 color="#000000">TIKET KOMPLAIN </font></b></td>
		<td style="border-left: 2px solid #000000" align="left" valign=middle><b><font face="Arial Narrow" color="#000000">NO. REF</font></b></td>
		<td align="left" valign=middle><font face="Arial Narrow" color="#000000"><br></font></td>
		<td align="left" valign=middle><b><font face="Arial Narrow" color="#000000">:</font></b></td>
		<td style="border-right: 2px solid #000000" colspan=9 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">{{ $data->no_ref }}</font></b></td>
		<td style="border-left: 2px solid #000000" align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-left: 2px solid #000000" colspan=2 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">CABANG/SP/FS</font></b></td>
		<td align="left" valign=middle><b><font face="Arial Narrow" color="#000000">:</font></b></td>
		<td style="border-bottom: 1px solid #000000; border-right: 2px solid #000000" colspan=9 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">{{ $data->getBranch['name'] }}</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000" colspan=2 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">TANGGAL</font></b></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=middle><b><font face="Arial Narrow" color="#000000">:</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan=9 align="center" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-left: 2px solid #000000" align="left" valign=middle><font face="Arial Narrow" color="#000000">NAMA PENELEPON</font></td>
		<td align="left" valign=middle><font face="Arial Narrow" color="#000000">:</font></td>
		<td style="border-top: 2px solid #000000; border-right: 2px solid #000000" colspan=21 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">Ibu Azizah</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=middle><font face="Arial Narrow">NOMOR TELEPON</font></td>
		<td align="left" valign=middle><font face="Arial Narrow" color="#000000">:</font></td>
		<td style="border-right: 2px solid #000000" colspan=21 align="left" valign=middle sdnum="1033;0;0"><b><font face="Arial Narrow" color="#000000">0878-3168-1647</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=middle><font face="Arial Narrow">JAM KOMPLAIN</font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=middle><font face="Arial Narrow" color="#000000">:</font></td>
		<td style="border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan=21 align="left" valign=middle sdval="0.600694444444444" sdnum="1033;1033;H:MM"><b><font face="Arial Narrow" color="#000000">14:25</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=23 align="left" valign=middle bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000">A. IDENTITAS PELANGGAN </font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-left: 2px solid #000000" colspan=2 align="left" valign=middle><font face="Arial Narrow" color="#000000">NOMOR PELANGGAN </font></td>
		<td align="left" valign=middle><b><font face="Arial Narrow" color="#000000">:</font></b></td>
		<td style="border-bottom: 1px solid #000000; border-right: 2px solid #000000" colspan=4 align="left" valign=middle><b><font face="Arial Narrow">3703-0000006</font></b></td>
		<td style="border-left: 2px solid #000000" colspan=3 align="left" valign=middle><font face="Arial Narrow" color="#000000">NAMA PEMILIK OUTLET</font></td>
		<td align="left" valign=middle><font face="Arial Narrow" color="#000000">:</font></td>
		<td style="border-bottom: 1px solid #000000; border-right: 2px solid #000000" colspan=12 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">ABDUL KARIM</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-left: 2px solid #000000" colspan=2 align="left" valign=middle><font face="Arial Narrow" color="#000000">NAMA PELANGGAN</font></td>
		<td align="left" valign=middle><b><font face="Arial Narrow" color="#000000">:</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 2px solid #000000" colspan=4 align="left" valign=middle><b><font face="Arial Narrow">Wenny</font></b></td>
		<td style="border-left: 2px solid #000000" colspan=3 align="left" valign=middle><font face="Arial Narrow" color="#000000">NAMA SALESMAN</font></td>
		<td align="left" valign=middle><font face="Arial Narrow" color="#000000">:</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 2px solid #000000" colspan=12 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">3703-MSRA02 ( RENDI PRATAMA )</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 1px solid #000000; border-left: 2px solid #000000" colspan=2 rowspan=2 align="left" valign=middle><font face="Arial Narrow" color="#000000">ALAMAT PELANGGAN</font></td>
		<td style="border-bottom: 1px solid #000000" rowspan=2 align="center" valign=middle><b><font face="Arial Narrow" color="#000000">:</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 2px solid #000000" colspan=4 rowspan=2 align="left" valign=middle><b><font face="Arial Narrow">Jl Letjen S. Parman no 61</font></b></td>
		<td style="border-left: 2px solid #000000" colspan=3 align="left" valign=middle><font face="Arial Narrow" color="#000000">NAMA SPV SALES</font></td>
		<td align="left" valign=middle><font face="Arial Narrow" color="#000000">:</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 2px solid #000000" colspan=12 align="left" valign=middle><b><font face="Arial Narrow" color="#000000"> MARDONA</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=16 rowspan=2 align="center" valign=middle><font face="Arial Narrow" color="#000000"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000" colspan=2 align="left" valign=middle><font face="Arial Narrow" color="#000000">TYPE OUTLET</font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=middle><b><font face="Arial Narrow" color="#000000">:</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan=4 align="center" valign=middle><b><font face="Arial Narrow"><br></font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=23 align="left" valign=middle bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000">B. INFORMASI KOMPLAIN</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-left: 2px solid #000000" rowspan=2 align="left" valign=middle><font face="Arial Narrow" color="#000000">VARIABEL KOMPLAIN</font></td>
		<td style="border-bottom: 2px solid #000000" rowspan=2 align="center" valign=middle><b><font face="Arial Narrow" size=1 color="#000000">:</font></b></td>
		<td style="border-right: 2px solid #000000" colspan=21 rowspan=2 align="center" valign=middle><font face="Arial Narrow" size=1 color="#000000"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=23 align="left" valign=middle bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000">DETAIL PERMASALAHAN</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=23 rowspan=2 align="left" valign=middle><font face="Arial Narrow" color="#000000">pemilik toko melakukan order tanggal 7 February sampai dengan tanggal 22 February 2021 product yang di pesan tidak sampai ke toko</font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=7 align="center" valign=middle><b><font face="Arial Narrow" color="#000000">ITEM PESANAN</font></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=16 align="center" valign=middle><b><font face="Arial Narrow" color="#000000">JUMLAH PESANAN</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=7 align="center" valign=middle><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
		<td style="border-bottom: 1px solid #000000; border-right: 2px solid #000000" colspan=16 align="left" valign=middle><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=7 align="left" valign=middle><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 2px solid #000000" colspan=16 align="left" valign=middle><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=7 align="left" valign=middle><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=16 align="left" valign=middle><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=23 align="left" valign=middle bgcolor="#BFBFBF"><b><font face="Arial Narrow" color="#000000">C. ANALISA AKAR MASALAH (Gunakan Analisa dengan 5W / Why 5 kali, atau metode lainnya)</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=23 rowspan=2 align="left" valign=top><font face="Arial Narrow" color="#000000"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=23 align="left" valign=middle bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000">D. TINDAK LANJUT</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="left" valign=middle><b><font face="Arial Narrow" color="#000000">D1. REALISASI TINDAK LANJUT</font></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=22 align="center" valign=middle><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" rowspan=4 align="center" valign=middle><b><font face="Arial Narrow" color="#000000">HARI 1</font></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000" colspan=3 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">Tanggal :</font></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan=11 align="left" valign=middle sdval="0" sdnum="1033;1033;D-MMM-YY"><b><font face="Arial Narrow" color="#000000">30-Dec-99</font></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">Status</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=14 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">Tindak Lanjut :</font></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 rowspan=3 align="left" valign=middle><font face="Arial Narrow" color="#000000">nantitaruhdisini</font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=14 rowspan=2 align="left" valign=top sdnum="1033;1033;M/D/YYYY H:MM"><b><font face="Arial Narrow" color="#000000">1. CS Pusat sudah melakukan konfirmasi ke ADM BM Pekanbaru perihal order toko yang terpending</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=23 align="left" valign=middle><b><font face="Arial Narrow">D2. HASIL TINDAK LANJUT</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=23 rowspan=2 align="left" valign=top><b><font face="Trebuchet MS">Toko sudah menerima sesuai dengan produk yang di pesan dan sudah menerima penjelasan dari pihak FB terkait adanya keterlambatan penerimaan barang ke Toko</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=23 align="left" valign=middle bgcolor="#D9D9D9"><b><font face="Arial Narrow">E. TINDAKAN PENCEGAHAN</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=23 rowspan=2 align="left" valign=top><b><font face="Trebuchet MS">nantitaruhsinilagi</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 align="left" valign=middle><b><font face="Arial Narrow">CATATAN :</font></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=4 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">Dibuat Oleh,</font></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=5 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">Ditindaklanjuti Oleh,</font></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=6 align="left" valign=middle><b><font face="Arial Narrow" color="#000000">Disetujui Oleh,</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 rowspan=5 align="left" valign=top><font face="Arial Narrow">Point A &amp; B diisi oleh CS KP sedangkan point C s.d E diisi oleh cabang.</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000" colspan=4 align="left" valign=middle><font face="Arial Narrow" color="#000000">Customer Service Staff</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=5 align="left" valign=middle><font face="Arial Narrow" color="#000000">SM / BM</font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan=6 align="left" valign=middle><font face="Arial Narrow" color="#000000">Customer Service Manajer</font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=4 rowspan=2 align="center" valign=middle><font face="Arial Narrow" color="#000000"><br></font></td>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=5 rowspan=2 align="center" valign=middle><font face="Arial Narrow" color="#000000"><br></font></td>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=6 rowspan=2 align="center" valign=middle><font face="Arial Narrow" color="#000000"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000" align="left" valign=middle><font face="Arial Narrow" color="#000000">Tanggal :</font></td>
		<td style="border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan=3 align="left" valign=middle sdnum="1033;1033;D-MMM-YY"><font face="Arial Narrow" color="#000000"><br></font></td>
		<td style="border-bottom: 2px solid #000000" align="left" valign=middle><font face="Arial Narrow" color="#000000">Tanggal :</font></td>
		<td style="border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan=4 align="left" valign=middle sdval="44250" sdnum="1033;1033;D-MMM-YY"><font face="Arial Narrow" color="#000000">23-Feb-21</font></td>
		<td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000" colspan=2 align="center" valign=middle><font face="Arial Narrow" color="#000000">Tanggal :</font></td>
		<td style="border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan=4 align="center" valign=middle><font face="Arial Narrow" color="#000000"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="22" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=4 align="center" valign=middle><b><font face="Arial Narrow" color="#000000">Nama: Jullya</font></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=5 align="center" valign=middle><b><font face="Arial Narrow" color="#000000">Nama: Bruslan</font></b></td>
		<td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=6 align="center" valign=middle><b><font face="Arial Narrow" color="#000000">Nama: Dina Novianti</font></b></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
	<tr>
		<td height="21" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px solid #000000" align="left" valign=top><i><font face="Arial Narrow">Hardcopy/Softcopy by scan -1 Rangkap - Adm BM/Customer Service Staff </font></i></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
		<td align="left" valign=bottom><font color="#000000"><br></font></td>
	</tr>
</table>
</body>

</html>

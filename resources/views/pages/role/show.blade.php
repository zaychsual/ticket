<div id="display-errors"></div>
<input type="hidden" name="id" id="id" value="{{ $role->id }}">
<input type="hidden" name="action_post" id="action_post">
<div class="form-group">
    <label>Role Name <b class="label-required">*</b></label>
    <input type="text" id="name" name="name" class="form-control update" value="{{ old('name', $role->name) }}" readonly>
    <span class="text-danger error-text name_err">
        <strong id="name-error"></strong>
    </span>
</div>
<div class="table-responsive">
    <table id="index-table" class="table table-bordered table-striped table-hover datatable datatable-Role">
        <thead>
            <tr>
                <th>No.</th>
                <th>Menu</th>
                <th>Access</th>
                <th>Add</th>
                <th>Edit</th>
                <th>Destroy</th>
            </tr>
        </thead>
        <tbody>
            {{-- @php
                dd($menu_role);
            @endphp --}}
            @php
                $num = 0;
            @endphp
            @foreach($result as $key)
                @if (isset($key->akses_id))
                    @if($key->role_id == $role->id || $key->role_id == null)
                        <tr data-entry-id="{{ $key->id }}">
                            <td>
                                {{ $num+1 }}
                            </td>
                            <td>
                                {{ $key->menu ?? '' }}
                                <input type="hidden" name="menu_id[]" id="menu_id_{{ $key->id }}" value="{{ $key->id }}">
                                <input type="hidden" name="akses_id[]" id="akses_id_{{ $key->id }}" value="{{ $key->akses_id }}">
                            </td>
                            <td>
                                <select class="select2 form-control" style="width: 100%;" name="akses[]" id="akses_{{ $num+1 }}" readonly>
                                    <option value="0" @if($key->akses == 0) selected @endif>Non Aktif</option>
                                    <option value="1" @if($key->akses == 1) selected @endif>Aktif</option>
                                </select>
                            </td>
                            <td>
                                <select class="select2 form-control" style="width: 100%;" name="add[]" id="add_{{ $num+1 }}" readonly>
                                    <option value="0" @if($key->add == 0) selected @endif>Non Aktif</option>
                                    <option value="1" @if($key->add == 1) selected @endif>Aktif</option>
                                </select>
                            </td>
                            <td>
                                <select class="select2 form-control" style="width: 100%;" name="edit[]" id="edit_{{ $num+1 }}" readonly>
                                    <option value="0" @if($key->edit == 0) selected @endif>Non Aktif</option>
                                    <option value="1" @if($key->edit == 1) selected @endif>Aktif</option>
                                </select>
                            </td>
                            <td>
                                <select class="select2 form-control" style="width: 100%;" name="destroy[]" id="destroy_{{ $num+1 }}" readonly>
                                    <option value="0" @if($key->destroy == 0) selected @endif>Non Aktif</option>
                                    <option value="1" @if($key->destroy == 1) selected @endif>Aktif</option>
                                </select>
                            </td>
                        </tr>
                    @endif
                @else
                    <tr data-entry-id="{{ $key->id }}">
                        <td>
                            {{ $num+1 }}
                        </td>
                        <td>
                            {{ $key->name ?? '' }}
                            <input type="hidden" name="menu_id[]" id="menu_id_{{ $key->id }}" value="{{ $key->id }}">
                        </td>
                        <td>
                            <select class="select2 form-control" style="width: 100%;" name="akses[]" id="akses_{{ $num+1 }}" readonly>
                                <option value="0">Non Aktif</option>
                                <option value="1">Aktif</option>
                            </select>
                        </td>
                        <td>
                            <select class="select2 form-control" style="width: 100%;" name="add[]" id="add_{{ $num+1 }}" readonly>
                                <option value="0">Non Aktif</option>
                                <option value="1">Aktif</option>
                            </select>
                        </td>
                        <td>
                            <select class="select2 form-control" style="width: 100%;" name="edit[]" id="edit_{{ $num+1 }}" readonly>
                                <option value="0">Non Aktif</option>
                                <option value="1">Aktif</option>
                            </select>
                        </td>
                        <td>
                            <select class="select2 form-control" style="width: 100%;" name="destroy[]" id="destroy_{{ $num+1 }}" readonly>
                                <option value="0">Non Aktif</option>
                                <option value="1">Aktif</option>
                            </select>
                        </td>
                    </tr>
                @endif
                @php
                    $num++;
                @endphp
            @endforeach
        </tbody>
    </table>
    {{-- {!! $menu->links() !!} --}}
</div>

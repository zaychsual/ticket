<div id="display-errors"></div>
<div class="form-group">
    <label>Role Name <b class="label-required">*</b></label>
    <input type="text" id="name" name="name" class="form-control update" value="{{ old('name') }}">
    <span class="text-danger error-text name_err">
        <strong id="name-error"></strong>
    </span>
</div>
<div class="table-responsive">
    <table id="index-table" class="table table-bordered table-striped table-hover datatable datatable-Role">
        <thead>
            <tr>
                <th>No.</th>
                <th>Menu</th>
                <th>Access</th>
                <th>Add</th>
                <th>Edit</th>
                <th>Destroy</th>
            </tr>
        </thead>
        <tbody>
            @php
                $num = 0;
            @endphp
            @foreach($akses as $key)
                <tr data-entry-id="{{ $key->id }}">
                    <td>
                        {{ $num+1 }}
                    </td>
                    <td>
                        {{ $key->name ?? '' }}
                        <input type="hidden" name="menu_id[]" id="menu_id_{{ $num+1 }}" value="{{ $key->id }}">
                    </td>
                    <td>
                        <select class="select2 form-control" style="width: 100%;" name="akses[]" id="akses_{{ $num+1 }}">
                            <option value="0">Non Aktif</option>
                            <option value="1">Aktif</option>
                        </select>
                    </td>
                    <td>
                        <select class="select2 form-control" style="width: 100%;" name="add[]" id="add_{{ $num+1 }}">
                            <option value="0">Non Aktif</option>
                            <option value="1">Aktif</option>
                        </select>
                    </td>
                    <td>
                        <select class="select2 form-control" style="width: 100%;" name="edit[]" id="edit_{{ $num+1 }}">
                            <option value="0">Non Aktif</option>
                            <option value="1">Aktif</option>
                        </select>
                    </td>
                    <td>
                        <select class="select2 form-control" style="width: 100%;" name="destroy[]" id="destroy_{{ $num+1 }}">
                            <option value="0">Non Aktif</option>
                            <option value="1">Aktif</option>
                        </select>
                    </td>
                </tr>
                @php
                    $num++;
                @endphp
            @endforeach
        </tbody>
    </table>
    {{-- {!! $menu->links() !!} --}}
</div>

<div id="display-errors"></div>
<input type="hidden" name="id" id="id" value="{{ $edit->id }}">
<input type="hidden" name="action_post" id="action_post">
<div class="form-group">
    <label>Variable Name <b class="label-required">*</b></label>
    <input type="text" id="name" name="name" class="form-control update" value="{{ $edit->name }}">
    <span class="text-danger error-text name_err">
        <strong id="name-error"></strong>
    </span>
</div>

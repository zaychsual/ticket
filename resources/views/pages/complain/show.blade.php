<div id="display-errors"></div>
<input type="hidden" name="id" id="id" value="{{ $variableComplain->id }}">
<div class="form-group">
    <label>Variable Name <b class="label-required">*</b></label>
    <input type="text" id="name" name="name" class="form-control update" value="{{ $variableComplain->name }}" disabled>
    <span class="text-danger error-text name_err"></span>
</div>

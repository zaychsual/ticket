@section('title', 'Customer')
@extends('layouts.master')
@push('custom-css')
<link rel="stylesheet" href="{{ asset('assets/modules/selectric/public/selectric.css') }}">
@endpush
@section('header', 'Customer')
@section('content')
<div class="section-body">
    <div class="row mt-4">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="section-header-button">
                        <button class="btn btn-primary add-new" id="add-index">
                            <i class="far fa-plus-square"></i> Add New
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="ajax-table" class="table table-bordered table-striped table-hover datatable datatable-Role">
                            <thead>
                                <tr>
                                    <th>Nomor Pelanggan</th>
                                    <th>Name</th>
                                    <th>Outlet Name</th>
                                    <th>Phone Number</th>
                                    <th>Author</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item => $raw)
                                <tr>
                                    <td>{{ $raw->no_pelanggan }}</td>
                                    <td>{{ $raw->realname }}</td>
                                    <td>{{ $raw->outlet_name }}</td>
                                    <td>{{ $raw->phone_no }}</td>
                                    <td>{{ \Penolong::getName($raw->updated_by)->name ?? \Penolong::getName($raw->created_by)->name }}</td>
                                    <td>
                                        <a class="badge badge-info show-index" id="show_{{ $item }}" data-id="{{ $raw->id }}" href="#">
                                            <i class="fa fa-eye"></i> Show
                                        </a> &nbsp;
                                        <a class="badge badge-warning edit-index" data-name="update" id="edit_{{ $item }}" data-id="{{ $raw->id }}" href="#">
                                            <i class="fa fa-edit"></i> Edit
                                        </a> &nbsp;
                                        <a class="badge del-index badge-danger" data-name="{{ $raw->outlet_name }}" data-id="{{ $raw->id }}" href="#">
                                            <form action="{{ route('admin.cust.destroy', $raw->id) }}" id="delete_{{ $raw->id }}" method="POST">
                                                @csrf
                                                @method('delete')
                                            </form>
                                            <i class="fa fa-trash"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-right">
                            {!! $data->links() !!}
                        </div>
                        {{-- {{ $data->links() }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('pages.cust.modal')
@endsection
@push('custom-js')
<script src="{{ asset('assets/js/pages/cust.js') }}"></script>
@endpush

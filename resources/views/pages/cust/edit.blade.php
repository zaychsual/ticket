<div id="display-errors"></div>
<input type="hidden" name="id" id="id" value="{{ $customer->id }}">
<div class="form-group">
    <label>Customer Number <b class="label-required">*</b></label>
    <input type="text" id="no_pelanggan" name="no_pelanggan" class="form-control add" value="{{ old('no_pelanggan') }}">
    <span class="text-danger error-text no_pelanggan_err">
        <strong id="no_pelanggan-error"></strong>
    </span>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>Customer Name <b class="label-required">*</b></label>
            <input type="text" id="realname" name="realname" class="form-control add" value="{{ old('realname', $customer->realname) }}">
            <span class="text-danger error-text realname_err">
                <strong id="realname-error"></strong>
            </span>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Outlet Name <b class="label-required">*</b></label>
            <input type="text" id="outlet_name" name="outlet_name" class="form-control add" value="{{ old('outlet_name', $customer->outlet_name) }}">
            <span class="text-danger error-text name_err">
                <strong id="outlet_name-error"></strong>
            </span>
        </div>
    </div>
</div>
<div class="form-group">
    <label>Phone Number <b class="label-required">*</b></label>
    <input type="text" id="phone_no" name="phone_no" onkeyup="numericFilter(this);" class="form-control add" value="{{ old('phone_no', $customer->phone_no) }}">
    <span class="text-danger error-text phone_no_err">
        <strong id="phone_no-error"></strong>
    </span>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>SPV Name <b class="label-required">*</b></label>
            <input type="text" id="spv_sales" name="spv_sales" class="form-control add" value="{{ old('spv_sales', $customer->spv_sales) }}">
            <span class="text-danger error-text spv_sales_err">
                <strong id="spv_sales-error"></strong>
            </span>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Sales Name <b class="label-required">*</b></label>
            <input type="text" id="salesname" name="salesname" class="form-control add" value="{{ old('salesname', $customer->salesname) }}">
            <span class="text-danger error-text salesname_err">
                <strong id="salesname-error"></strong>
            </span>
        </div>
    </div>
</div>
<div class="form-group">
    <label>Type Outlet <b class="label-required">*</b></label>
    <select class="select2 form-control" style="width: 100%;" name="type_outlet_id" id="type_outlet_id">
        <option value=""> Pilih </option>
        @foreach ($type_outlet as $id => $item)
            <option value="{{ $id }}" @if($customer->type_outlet_id == $id) selected @endif >{{ $item }}</option>
        @endforeach
    </select>
    <span class="text-danger error-text type_outlet_id_err">
        <strong id="type_outlet_id-error"></strong>
    </span>
</div>

<div class="form-group">
    <label>Address <b class="label-required">*</b></label>
    <textarea class="form-control add" id="alamat" name="alamat">{{ old('alamat', $customer->alamat) }}</textarea>
    <span class="text-danger error-text alamat_err">
        <strong id="alamat-error"></strong>
    </span>
</div>

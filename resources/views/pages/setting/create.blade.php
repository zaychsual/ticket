<div id="display-errors"></div>
<div class="form-group">
    <label>Name <b class="label-required">*</b></label>
    <input type="text" id="name" name="name" class="form-control add" value="{{ old('name') }}">
    <span class="text-danger error-text name_err">
        <strong id="name-error"></strong>
    </span>
</div>
<div class="form-group">
    <label>Prefix <b class="label-required">*</b></label>
    <input type="text" id="prefix" name="prefix" class="form-control add" value="{{ old('prefix') }}">
    <span class="text-danger error-text prefix_err">
        <strong id="prefix-error"></strong>
    </span>
</div>




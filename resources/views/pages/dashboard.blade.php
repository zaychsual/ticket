@section('title', 'Dashboard')
@extends('layouts.master')
@section('header', 'Dashboard')
@push('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/modules/owl.carousel/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/owl.carousel/dist/assets/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/jqvmap/dist/jqvmap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/weathericons/css/weather-icons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/weathericons/css/weather-icons-wind.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/summernote/dist/summernote-bs4.css') }}">
@endpush
@section('content')
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
                <i class="far fa-newspaper"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Ticket Open</h4>
                </div>
                <div class="card-body">
                    {{ $open }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
                <i class="far fa-newspaper"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Ticket On Progress</h4>
                </div>
                <div class="card-body">
                    {{ $progress }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-secondary">
                <i class="far fa-newspaper"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Ticket To Be Revised</h4>
                </div>
                <div class="card-body">
                    {{ $revised }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-success">
                <i class="far fa-newspaper"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Ticket Closed</h4>
                </div>
                <div class="card-body">
                    {{ $closed }}
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-success">
                <i class="fas fa-circle"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Online Users</h4>
                </div>
                <div class="card-body">
                    47
                </div>
            </div>
        </div>
    </div> --}}
</div>
{{-- <div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header">
                <h4>Budget vs Sales</h4>
            </div>
            <div class="card-body">
                <div class="chartjs-size-monitor">
                    <div class="chartjs-size-monitor-expand">
                        <div class=""></div>
                    </div>
                    <div class="chartjs-size-monitor-shrink">
                        <div class=""></div>
                    </div>
                </div>
                <canvas id="myChart" height="528" style="display: block; width: 1003px; height: 528px;" width="1003" class="chartjs-render-monitor"></canvas>
                <div class="card-footer pt-3 d-flex justify-content-center">
                    <div class="budget-price justify-content-center">
                        <div class="budget-price-square bg-primary" data-width="20" style="width: 20px;"></div>
                        <div class="budget-price-label">Selling Price</div>
                    </div>
                    <div class="budget-price justify-content-center">
                        <div class="budget-price-square bg-danger" data-width="20" style="width: 20px;"></div>
                        <div class="budget-price-label">Budget Price</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card gradient-bottom">
            <div class="card-header">
                <h4>Top 5 Products</h4>
                <div class="card-header-action dropdown">
                    <a href="#" data-toggle="dropdown" class="btn btn-danger dropdown-toggle" aria-expanded="false">Month</a>
                    <ul class="dropdown-menu dropdown-menu-sm dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(76px, 32px, 0px);">
                        <li class="dropdown-title">Select Period</li>
                        <li><a href="#" class="dropdown-item">Today</a></li>
                        <li><a href="#" class="dropdown-item">Week</a></li>
                        <li><a href="#" class="dropdown-item active">Month</a></li>
                        <li><a href="#" class="dropdown-item">This Year</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body" id="top-5-scroll" tabindex="2" style="height: 315px; overflow: hidden; outline: none;">
                <ul class="list-unstyled list-unstyled-border">
                    <li class="media">
                        <img class="mr-3 rounded" width="55" src="../assets/img/products/product-3-50.png" alt="product">
                        <div class="media-body">
                            <div class="float-right"><div class="font-weight-600 text-muted text-small">86 Sales</div></div>
                            <div class="media-title">oPhone S9 Limited</div>
                            <div class="mt-1">
                                <div class="budget-price">
                                    <div class="budget-price-square bg-primary" data-width="64%" style="width: 64%;"></div>
                                    <div class="budget-price-label">$68,714</div>
                                </div>
                                <div class="budget-price">
                                    <div class="budget-price-square bg-danger" data-width="43%" style="width: 43%;"></div>
                                    <div class="budget-price-label">$38,700</div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="media">
                        <img class="mr-3 rounded" width="55" src="../assets/img/products/product-4-50.png" alt="product">
                        <div class="media-body">
                            <div class="float-right">
                                <div class="font-weight-600 text-muted text-small">67 Sales</div>
                            </div>
                            <div class="media-title">
                                iBook Pro 2018
                            </div>
                            <div class="mt-1">
                                <div class="budget-price">
                                    <div class="budget-price-square bg-primary" data-width="84%" style="width: 84%;"></div>
                                    <div class="budget-price-label">$107,133</div>
                                </div>
                                <div class="budget-price">
                                    <div class="budget-price-square bg-danger" data-width="60%" style="width: 60%;"></div>
                                    <div class="budget-price-label">$91,455</div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="media">
                        <img class="mr-3 rounded" width="55" src="../assets/img/products/product-1-50.png" alt="product">
                        <div class="media-body">
                            <div class="float-right">
                                <div class="font-weight-600 text-muted text-small">63 Sales</div>
                            </div>
                            <div class="media-title">Headphone Blitz</div>
                            <div class="mt-1">
                                <div class="budget-price">
                                    <div class="budget-price-square bg-primary" data-width="34%" style="width: 34%;"></div>
                                    <div class="budget-price-label">$3,717</div>
                                </div>
                                <div class="budget-price">
                                    <div class="budget-price-square bg-danger" data-width="28%" style="width: 28%;"></div>
                                    <div class="budget-price-label">$2,835</div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="media">
                        <img class="mr-3 rounded" width="55" src="../assets/img/products/product-3-50.png" alt="product">
                        <div class="media-body">
                            <div class="float-right">
                                <div class="font-weight-600 text-muted text-small">28 Sales</div>
                            </div>
                            <div class="media-title">oPhone X Lite</div>
                            <div class="mt-1">
                                <div class="budget-price">
                                    <div class="budget-price-square bg-primary" data-width="45%" style="width: 45%;"></div>
                                    <div class="budget-price-label">$13,972</div>
                                </div>
                                <div class="budget-price">
                                    <div class="budget-price-square bg-danger" data-width="30%" style="width: 30%;"></div>
                                    <div class="budget-price-label">$9,660</div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="media">
                        <img class="mr-3 rounded" width="55" src="../assets/img/products/product-5-50.png" alt="product">
                        <div class="media-body">
                            <div class="float-right">\
                                <div class="font-weight-600 text-muted text-small">19 Sales</div>
                            </div>
                            <div class="media-title">Old Camera</div>
                            <div class="mt-1">
                                <div class="budget-price">
                                    <div class="budget-price-square bg-primary" data-width="35%" style="width: 35%;"></div>
                                    <div class="budget-price-label">$7,391</div>
                                </div>
                                <div class="budget-price">
                                    <div class="budget-price-square bg-danger" data-width="28%" style="width: 28%;"></div>
                                    <div class="budget-price-label">$5,472</div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="card-footer pt-3 d-flex justify-content-center">
                <div class="budget-price justify-content-center">
                    <div class="budget-price-square bg-primary" data-width="20" style="width: 20px;"></div>
                    <div class="budget-price-label">Selling Price</div>
                </div>
                <div class="budget-price justify-content-center">
                    <div class="budget-price-square bg-danger" data-width="20" style="width: 20px;"></div>
                    <div class="budget-price-label">Budget Price</div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection

@push('custom-js')
    <script src="{{ asset('assets/modules/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/modules/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('assets/modules/owl.carousel/dist/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/modules/summernote/dist/summernote-bs4.js') }}"></script>
    <script src="{{ asset('assets/modules/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>
    <script src="{{ asset('assets/js/page/index.js') }}"></script>
@endpush

<input type="hidden" id="id" name="id" class="form-control update" value="{{ $branch->id }}">
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>Region <b class="label-required">*</b></label>
            <select class="select2 form-control" style="width: 100%;" name="region_id" id="region_id">
                <option value=""> Pilih </option>
                @foreach ($region as $id => $item)
                    <option value="{{ $id }}" @if($branch->region_id == $id) selected @endif>{{ $item }}</option>
                @endforeach
            </select>
            <span class="text-danger error-text region_err">
                <strong id="region-error"></strong>
            </span>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Code <b class="label-required">*</b></label>
            <input type="text" id="code" name="code" class="form-control update" value="{{ old('code', $branch->code) }}">
            <span class="text-danger error-text code_err">
                <strong id="code-error"></strong>
            </span>
        </div>
    </div>
</div>
<div class="form-group">
    <label>Name <b class="label-required">*</b></label>
    <input type="text" id="name" name="name" class="form-control update" value="{{ old('name', $branch->name) }}">
    <span class="text-danger error-text name_err">
        <strong id="name-error"></strong>
    </span>
</div>

@section('title', 'Branch')
@extends('layouts.master')
@push('custom-css')
{{-- <link rel="stylesheet" href="{{ asset('assets/modules/selectric/public/selectric.css') }}"> --}}
@endpush
@section('header', 'Branch')
@section('content')
<div class="section-body">
    <div class="row mt-4">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="section-header-button">
                        <button class="btn btn-primary add-new" id="add-index">
                            <i class="far fa-plus-square"></i> Add New
                        </button>
                    </div>
                    <hr>
                    @if(session('message'))
                        <div class="alert alert-success alert-has-icon">
                            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Success</div>
                                {{ session('message') }}
                            </div>
                        </div>
                    @endif
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="index-table" class="table table-bordered table-striped table-hover datatable datatable-Role">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Region</th>
                                    <th>Created By</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key => $item)
                                    <tr data-entry-id="{{ $item->id }}">
                                        <td>
                                            {{ $key+1 }}
                                        </td>
                                        <td>
                                            {{ $item->code ?? '' }}
                                        </td>
                                        <td>
                                            {{ $item->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $item->getRegional['name'] ?? '' }}
                                        </td>
                                        <td>
                                            {{ \Penolong::getName($item->created_by)->name ?? '' }}
                                        </td>
                                        <td>
                                            <a class="badge badge-info show-index" data-id="{{ $item->id }}" id="show_{{ $item->id }}" href="#">
                                                <i class="fa fa-eye"></i> Show
                                            </a>
                                            <a class="badge badge-warning edit-index" data-id="{{ $item->id }}" id="edit_{{ $item->id }}" href="#">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                            <a class="badge del-index badge-danger" data-name="{{ $item->name }}" data-id="{{ $item->id }}" href="#">
                                                <form action="{{ route('admin.branch.destroy', $item->id) }}" id="delete_{{ $item->id }}" method="POST">
                                                    @csrf
                                                    @method('delete')
                                                </form>
                                                <i class="fa fa-trash"></i> Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{-- {{ $data->links() }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('pages.branch.modal')
@endsection
@push('custom-js')
    <script src="{{ asset('assets/js/pages/branch.js') }}"></script>
@endpush

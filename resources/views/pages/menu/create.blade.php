<div id="display-errors"></div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>Name <b class="label-required">*</b></label>
            <input type="text" id="name" name="name" class="form-control add" value="{{ old('name') }}">
            <span class="text-danger error-text name_err">
                <strong id="name-error"></strong>
            </span>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Level Menu <b class="label-required">*</b></label>
            <select class="select2 form-control" style="width: 100%;" name="level_menu" id="level_menu">
                <option value=""> Pilih </option>
                <option value="main_menu">Main Menu</option>
                <option value="sub_menu">Sub Menu</option>
            </select>
            <span class="text-danger error-text region_err">
                <strong id="region-error"></strong>
            </span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>Master Menu <b class="label-required">*</b></label>
            <select class="select2 form-control" style="width: 100%;" name="master_menu" id="master_menu">
                <option value="0"> Pilih </option>
                @foreach ($main as $id => $item)
                    <option value="{{ $id }}">{{ $item }}</option>
                @endforeach
            </select>
            <span class="text-danger error-text master_menu_err">
                <strong id="master_menu-error"></strong>
            </span>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Nomor Urut <b class="label-required">*</b></label>
            <input type="text" id="no_urut" name="no_urut" onKeyUp="numericFilter(this);" class="form-control add" value="{{ old('no_urut') }}">
            <span class="text-danger error-text no_urut_err">
                <strong id="no_urut-error"></strong>
            </span>
        </div>
    </div>
</div>
<div class="form-group">
    <label>Menu Aktif</label>
    <input type="text" id="menu_aktif" name="menu_aktif" class="form-control add" value="{{ old('menu_aktif') }}">
    <span class="text-danger error-text menu_aktif_err">
        <strong id="menu_aktif-error"></strong>
    </span>
</div>
<div class="form-group">
    <label>Url</label>
    <input type="text" id="url" name="url" class="form-control add" value="{{ old('url') }}">
    <span class="text-danger error-text url_err">
        <strong id="url-error"></strong>
    </span>
</div>
<div class="form-group">
    <label>Icon <b class="label-required">*</b></label>
    <input type="text" id="icon" name="icon" class="form-control add" value="{{ old('icon') }}">
    <span class="text-danger error-text icon_err">
        <strong id="icon-error"></strong>
    </span>
</div>

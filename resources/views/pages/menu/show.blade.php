<div id="display-errors"></div>
<input type="hidden" name="id" id="id" value="{{ $menu->id }}">
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>Name <b class="label-required">*</b></label>
            <input type="text" id="name" name="name" class="form-control add" value="{{ old('name', $menu->name) }}" readonly>
            <span class="text-danger error-text name_err">
                <strong id="name-error"></strong>
            </span>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Level Menu <b class="label-required">*</b></label>
            <select class="select2 form-control" style="width: 100%;" name="level_menu" id="level_menu" readonly>
                <option value=""> Pilih </option>
                <option value="main_menu" @if($menu->level_menu == 'main_menu') selected @endif>Main Menu</option>
                <option value="sub_menu" @if($menu->level_menu == 'sub_menu') selected @endif>Sub Menu</option>
            </select>
            <span class="text-danger error-text region_err">
                <strong id="region-error"></strong>
            </span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>Master Menu <b class="label-required">*</b></label>
            <select class="select2 form-control" style="width: 100%;" name="master_menu" id="master_menu" readonly>
                <option value="0"> Pilih </option>
                @foreach ($main as $id => $item)
                    <option value="{{ $id }}" @if($menu->master_menu == $id) selected @endif>{{ $item }}</option>
                @endforeach
            </select>
            <span class="text-danger error-text master_menu_err">
                <strong id="master_menu-error"></strong>
            </span>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Nomor Urut <b class="label-required">*</b></label>
            <input type="text" id="no_urut" name="no_urut" onKeyUp="numericFilter(this);" class="form-control add" value="{{ old('no_urut', $menu->no_urut) }}" readonly>
            <span class="text-danger error-text no_urut_err">
                <strong id="no_urut-error"></strong>
            </span>
        </div>
    </div>
</div>
<div class="form-group">
    <label>Menu Aktif</label>
    <input type="text" id="menu_aktif" name="menu_aktif" class="form-control add" value="{{ old('menu_aktif', $menu->menu_aktif) }}" readonly>
    <span class="text-danger error-text menu_aktif_err">
        <strong id="menu_aktif-error"></strong>
    </span>
</div>
<div class="form-group">
    <label>Url</label>
    <input type="text" id="url" name="url" class="form-control add" value="{{ old('url', $menu->url) }}" readonly>
    <span class="text-danger error-text url_err">
        <strong id="url-error"></strong>
    </span>
</div>
<div class="form-group">
    <label>Icon <b class="label-required">*</b></label>
    <input type="text" id="icon" name="icon" class="form-control add" value="{{ old('icon', $menu->icon) }}" readonly>
    <span class="text-danger error-text icon_err">
        <strong id="icon-error"></strong>
    </span>
</div>

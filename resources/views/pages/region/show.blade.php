<div id="display-errors"></div>
<input type="hidden" name="id" id="id" value="{{ old('id') }}">
<div class="form-group">
    <label>Name <b class="label-required">*</b></label>
    <input type="text" id="name" name="name" class="form-control update" value="{{ $region->name }}" disabled>
    <span class="text-danger error-text name_err"></span>
</div>

@section('title', 'Report Wilayah')
@extends('layouts.master')
@push('custom-css')
{{-- <link rel="stylesheet" href="{{ asset('assets/modules/selectric/public/selectric.css') }}"> --}}
@endpush
@section('header', 'Report Wilayah')
@section('content')
<div class="accordion" id="accordionExample">
    <div class="card">
        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <form method="post">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Start Date : </label>
                                    <div class='input-group mb-3'>
                                        <input type='text' class="form-control datepicker add" id="start" name="start" value="{{ date('Y-m-d'), old('start') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>End Date : </label>
                                    <div class='input-group mb-3'>
                                        <input type='text' class="form-control datepicker add" id="end" name="end" value="{{ date('Y-m-d'), old('end') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Branch</label>
                                    <div class='input-group mb-3'>
                                        <select class="select2 form-control" style="width: 100%;" name="kategori" id="kategori">
                                            <option value=""> Pilih </option>
                                            @foreach ($data as $id => $item)
                                                <option value="{{ $item->code }}">{{ $item->code }}-{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                {{-- <a type="button" class="btn btn-primary" id="show"><i class="fa fa-search"></i> Show</a> --}}
                                <a type="button" class="btn btn-warning" id="export"><i class="fas fa-download"></i> Export</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header text-white">
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Branch</th>
                        <th>Total Tiket</th>
                        <th>New Data</th>
                        <th>Progress</th>
                        <th>To Be Revised</th>
                        <th>Complete</th>
                    </tr>
                </thead>
                <tbody class="tbody-data">
                </tbody>
                <tfoot>
                    <tr class="bg-gray font-17">
                        <th><b>Total:</b></th>
                        <th class="sum_total_tiket"></th>
                        <th class="sum_total_new_data"></th>
                        <th class="sum_total_progress"></th>
                        <th class="sum_total_revised"></th>
                        <th class="sum_total_complete"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@push('custom-js')
    <script src="{{ asset('assets/js/pages/report-wilayah.js') }}"></script>
@endpush

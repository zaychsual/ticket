<input type="hidden" id="id" name="id" class="form-control update" value="{{ $user->id }}">
@if (\Penolong::cekRole(1))
    <div class="form-group">
        <label>Role</label>
        <select class="select2 form-control" style="width: 100%;" name="role_id" id="role_id" readonly>
            <option value=""> Pilih </option>
            @foreach ($role as $id => $item)
                <option value="{{ $id }}" @if($user->role_id == $id) selected @endif>{{ $item }}</option>
            @endforeach
        </select>
        <span class="text-danger error-text role_err">
            <strong id="role-error"></strong>
        </span>
    </div>
@endif
<div class="form-group">
    <label>Name</label>
    <input type="text" id="name" name="name" class="form-control update" value="{{ old('name', $user->name) }}">
    <span class="text-danger error-text name_err">
        <strong id="name-error"></strong>
    </span>
</div>
<div class="form-group">
    <label>Username</label>
    <input type="text" id="username" name="username" class="form-control update" value="{{ old('username', $user->username) }}">
    <span class="text-danger error-text username_err">
        <strong id="username-error"></strong>
    </span>
</div>
<div class="form-group">
    <label>Email</label>
    <input type="text" id="email" name="email" class="form-control update" value="{{ old('email', $user->email) }}">
    <span class="text-danger error-text email_err">
        <strong id="email-error"></strong>
    </span>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>Password</label>
            <input type="password" id="password" name="password" class="form-control update" value="{{ old('password') }}">
            <span class="text-danger error-text password_err">
                <strong id="password-error"></strong>
            </span>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Password Again</label>
            <input type="password" id="c_password" name="c_password" class="form-control update" value="{{ old('c_password') }}">
            <span class="text-danger error-text c_password_err">
                <strong id="c_password-error"></strong>
            </span>
        </div>
    </div>
</div>
@if (\Penolong::cekRole(1) && $user->role_id == 3)
@php
    $code = explode(",",$user->branch_code);
@endphp
<div id="branch_detail">
    <div class="form-group">
        <label>Branch <b class="label-required">*</b></label>
        <select class="form-control select2" style="width: 100%;" name="branch_id[]" id="branch_id" placeholder="Pilih" multiple>
            @foreach ($branch as $id => $item)
                @if (in_array($item->code, $code))
                    <option value="{{ $item->code }}" selected="true">{{ $item->code }}-{{ $item->name }}</option>
                @else
                    <option value="{{ $item->code }}">{{ $item->code }}-{{ $item->name }}</option>
                @endif
                {{-- <option value="{{ $item->id }}">{{ $item->code }}-{{ $item->name }}</option> --}}
            @endforeach
        </select>
        <span class="text-danger error-text branch_id_err">
            <strong id="branch_id-error"></strong>
        </span>
    </div>
    <div class="row">
        <div class="col">
            <label>Email BOM</label>
            <input class="form-control" input="text" name="email_bom" id="email_bom" value="{{ $user->email_bom }}">
            <span class="text-danger error-text email_bom_err">
                <strong id="email_bom-error"></strong>
            </span>
        </div>
        <div class="col">
            <label>Email Admin</label>
            <input class="form-control" input="text" name="email_admin" id="email_admin" value="{{ $user->email_admin }}">
            <span class="text-danger error-text email_admin_err">
                <strong id="email_admin-error"></strong>
            </span>
        </div>
    </div>
</div>
@endif

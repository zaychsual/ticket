<div id="display-errors"></div>
<div class="form-group">
    <label>Role</label>
    <select class="select2 form-control" style="width: 100%;" name="role_id" id="role_id">
        <option value=""> Pilih </option>
        @foreach ($role as $id => $item)
            <option value="{{ $id }}">{{ $item }}</option>
        @endforeach
    </select>
    <span class="text-danger error-text role_err">
        <strong id="role-error"></strong>
    </span>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>Name</label>
            <input type="text" id="name" name="name" class="form-control add" value="{{ old('name') }}">
            <span class="text-danger error-text name_err">
                <strong id="name-error"></strong>
            </span>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Username</label>
            <input type="text" id="username" name="username" class="form-control add" value="{{ old('username') }}">
            <span class="text-danger error-text username_err">
                <strong id="username-error"></strong>
            </span>
        </div>
    </div>
</div>
<div class="form-group">
    <label>Email</label>
    <input type="text" id="email" name="email" class="form-control add" value="{{ old('email') }}">
    <span class="text-danger error-text email_err">
        <strong id="email-error"></strong>
    </span>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>Password</label>
            <input type="password" id="password" name="password" class="form-control add" value="{{ old('password') }}">
            <span class="text-danger error-text password_err">
                <strong id="password-error"></strong>
            </span>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Password Again</label>
            <input type="password" id="c_password" name="c_password" class="form-control add" value="{{ old('c_password') }}">
            <span class="text-danger error-text c_password_err">
                <strong id="c_password-error"></strong>
            </span>
        </div>
    </div>
</div>
<div id="branch_detail" style="display: none;">
    <div class="form-group">
        <label>Branch <b class="label-required">*</b></label>
        <select class="form-control select2" name="branch_id[]" id="branch_id" placeholder="Pilih" multiple="">
            @foreach ($branch as $id => $item)
                <option value="{{ $item->code }}">{{ $item->code }}-{{ $item->name }}</option>
            @endforeach
        </select>
        <span class="text-danger error-text branch_id_err">
            <strong id="branch_id-error"></strong>
        </span>
    </div>
    <div class="row">
        <div class="col">
            <label>Email BOM</label>
            <input class="form-control" input="text" name="email_bom" id="email_bom" value="">
            <span class="text-danger error-text email_bom_err">
                <strong id="email_bom-error"></strong>
            </span>
        </div>
        <div class="col">
            <label>Email Admin</label>
            <input class="form-control" input="text" name="email_admin" id="email_admin" value="">
            <span class="text-danger error-text email_admin_err">
                <strong id="email_admin-error"></strong>
            </span>
        </div>
    </div>
</div>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>

	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.4.7.2 (Linux)"/>
	<meta name="author" content="zaynd"/>
	<meta name="created" content="2021-07-29T22:48:41"/>
	<meta name="changedby" content="zaynd"/>
	<meta name="changed" content="2021-07-29T22:52:55"/>
	<meta name="KSOProductBuildVer" content="1033-11.1.0.10161"/>

	<style type="text/css">
		body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:x-small }
		a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  }
		a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  }
		comment { display:none;  }
	</style>

</head>

<body>
<table align="left" cellspacing="0" border="0">
	<colgroup width="68"></colgroup>
	<colgroup width="361"></colgroup>
	<colgroup width="329"></colgroup>
	<colgroup width="110"></colgroup>
	<colgroup span="2" width="68"></colgroup>
	<tr>
		<td style="border-top: 2px double #000000; border-left: 2px double #000000" height="20" align="left" valign=bottom></td>
		<td style="border-top: 2px double #000000" align="left" valign=bottom></td>
		<td style="border-top: 2px double #000000" align="left" valign=bottom></td>
		<td style="border-top: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 2px double #000000; border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
    </tr>
	<tr>
        @php
            if($data->ticket_category == 'FRM-CSD-019') {
                $title = "NON KOMPLAIN";
            } else {
                $title = "KOMPLAIN";
            }
        @endphp
		<td style="border-left: 2px double #000000" height="35" align="left" valign=bottom><img src="{{ asset('avatars/logo-fastrata.png') }}" width="90" height="54"></td>
		<td style="border-right: 2px double #000000" colspan=5 align="center" valign=bottom><b><font face="Trebuchet MS" size=6>FORM TIKET {{ $title }}</font></b></td>
		</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Lead by</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=top><b><font face="Trebuchet MS">Toll Free - {{ \Penolong::getName($data->created_by)->name ?? '' }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="11" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">No Ref</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=middle><b><font face="Trebuchet MS">{{ $data->no_ref }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Hari / Tanggal</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" colspan=2 align="left" valign=bottom sdnum="1033;0;DDDD&quot;, &quot;MMMM DD&quot;, &quot;YYYY"><b><font face="Trebuchet MS">{{ \Penolong::dmy($data->created_at) }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Waktu</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom sdnum="1033;1033;H:MM"><b><font face="Trebuchet MS">{{ $data->jam_komplain }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">FB/SP/FS/MM</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ $data->getBranch['name'] }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
    @php
        $no_pelanggan = $dataCust->no_pelanggan;
        $outlet_name = $dataCust->outlet_name;
        $branch = $data->getBranch['name'];
    @endphp
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Kode Pelanggan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ $no_pelanggan }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Nomer Pelanggan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ $no_pelanggan }}</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Nama Penelepon</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ $data->nama_penelpon }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Nama Outlet</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ $outlet_name }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">SR/RT/SG/G</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ $dataCust->getOutlet['name'] }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Nama Pemilik/Penanggung Jawab Toko</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ $dataCust->realname }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Alamat</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ $dataCust->alamat }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Nomer Telepon</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom sdnum="1033;0;0"><b><font face="Trebuchet MS">{{ $dataCust->phone_no }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Nama Salesman</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ $dataCust->salesname }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Nama Sales Supervisor</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ $dataCust->spv_sales }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
    @if (count($dt) > 0)
    <tr>
		<td style="border-left: 2px double #000000" height="11" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=middle><b><font face="Trebuchet MS"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
    <tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Item Pesanan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Jumlah Pesanan</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom bgcolor="#C0C0C0"><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#C0C0C0"><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
        @foreach ($dt as $key => $item)
        <tr>
            <td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><b><font face="Trebuchet MS">{{ $item->item }}</font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ $item->qty }}</font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
            <td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
        </tr>
        @endforeach
    @endif
	<tr>
		<td style="border-left: 2px double #000000" height="12" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=middle><b><font face="Trebuchet MS"></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="center" valign=bottom><b><font face="Trebuchet MS"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=4 align="left" valign=top bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Keluhan/Saran/Permasalahan Yang Di sampaikan</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 rowspan=4 align="left" valign=top><b><font face="Trebuchet MS" color="#000000">{{ $data->description }}</font></b></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Variabel Non Komplain</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=top><b><font face="Trebuchet MS" color="#000000">{{ $data->getComplain['name'] }}</font></b></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Keterangan Non Komplain</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" colspan=2 align="left" valign=top><b><font face="Trebuchet MS" color="#000000">{{ $data->description }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top><b><font face="Trebuchet MS" color="#000000"><br></font></b></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
    <tr>
		<td style="border-left: 2px double #000000" height="11" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><b><font face="Trebuchet MS"><br></font></b></td>
		<td align="left" valign=bottom><b><font face="Trebuchet MS"><br></font></b></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
    @foreach ($action as $item => $row)
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=middle bgcolor="#C0C0C0">
            <b>
                <font face="Trebuchet MS">Follow Up Tanggal</font>
            </b>
        </td>
        @php
            $tglindo = date_create($row->trx_date);
        @endphp
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" colspan=2 align="left" valign=bottom bgcolor="#C0C0C0" sdnum="1033;0;DDDD&quot;, &quot;MMMM DD&quot;, &quot;YYYY"><b><font face="Trebuchet MS">{{ date_format($tglindo,"d-m-Y") }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom bgcolor="#C0C0C0"><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=middle rowspan="2" bgcolor="#C0C0C0" >
            <b>
                <font face="Trebuchet MS">Tindak Lanjut</font>
            </b>
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 rowspan=2 align="left" valign=bottom>
            <b>
                <font face="Trebuchet MS">
                    CS:
                    <p>
                        {{ $row->cs_action }}
                    </p>
                    Branch:
                    <p>
                        {{ $row->bm_action }}
                    </p>
                </font>
            </b>
        </td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
    @endforeach

	<tr>
		<td style="border-left: 2px double #000000" height="11" align="left" valign="bottom"><font face="Trebuchet MS"></font></td>
		<td style="border-right: 2px double #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
	</tr>

	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=top bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Hasil</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 rowspan=2 align="left" valign=top>
            <b>
                <font face="Trebuchet MS">
                    {{ $data->hasil_investigasi }}
                </font>
            </b>
        </td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C0C0C0">
            <b>
                <font face="Trebuchet MS"></font>
            </b>
        </td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Status</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ \App\Models\Ticket::TypeStatus[$data->main_status_id] }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=top bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Lead Time</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS">{{ \Penolong::time_elapsed_print($data->created_at) }}</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td align="left" valign=top><b><font face="Trebuchet MS"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="center" valign=bottom><b><font face="Trebuchet MS"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="center" valign=bottom><b><font face="Trebuchet MS"><br></font></b></td>
		<td style="border-top: 1px solid #000000" align="center" valign=bottom><b><font face="Trebuchet MS"><br></font></b></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="20" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 1px solid #000000" align="left" valign=bottom><b><font face="Trebuchet MS"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=bottom bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Dibuat Oleh</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=bottom bgcolor="#C0C0C0"><b><font face="Trebuchet MS">Diperiksa Oleh</font></b></td>
		<td style="border-left: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-left: 2px double #000000" height="87" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 1px solid #000000" align="center" valign=bottom><b><font face="Trebuchet MS"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=bottom><b><font face="Trebuchet MS">{{ \Penolong::getName($data->created_by)->name ?? '' }}
		</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=bottom><b><font face="Trebuchet MS"><br></font></b></td>
		<td style="border-left: 1px solid #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
	<tr>
		<td style="border-bottom: 2px double #000000; border-left: 2px double #000000" height="21" align="left" valign=bottom><font face="Trebuchet MS" color="#C0C0C0"><br></font></td>
		<td style="border-bottom: 2px double #000000" align="left" valign=bottom><i><font face="Trebuchet MS">FRM-CSD-020 REV. 00</font></i></td>
		<td style="border-bottom: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
		<td style="border-bottom: 2px double #000000; border-right: 2px double #000000" align="left" valign=bottom><font face="Trebuchet MS"><br></font></td>
	</tr>
</table>
<br clear=left>

</body>
<script>
   window.print()
</script>
</html>

@section('title', 'Ticket Action')
@extends('layouts.master')

@section('header', 'Ticket Action')
@section('content')
<div class="section-body">
    <form id="action_store" enctype="multipart/form-data" action="{{ route('admin.ticket.update', $data->id) }}" method="POST">
        @method('PUT')
        @csrf
        <div id="display-errors"></div>
        <div class="row sortable-card">
        <div class="col-12 col-md-6 col-lg-4">
            {{-- Tiket Head --}}
            <div class="card card-warning">
                <div class="card-header">
                    <h5>
                        @if ($data->ticket_category == 'FRM-CSD-007') Tiket Komplain @else Tiket Non Komplain @endif  - {{ $data->ticket_category }}
                    </h5>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>No. Reff : </label>
                        <input type='text' class="form-control show" id="no_reff" name="no_reff" value="{{ old('no_ref', $data->no_ref) }}" readonly>
                        <input type='hidden' id="id" name="id" value="{{ $data->id }}" readonly>
                        <input type='hidden' id="ticket_id" name="ticket_id" value="{{ $data->id }}" readonly>
                        <input type='hidden' id="customer_id" name="customer_id" value="{{ $data->customer_id }}" readonly>
                        <input type='hidden' id="complain_id" name="complain_id" value="{{ $data->complain_id }}" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tanggal & Waktu Komplain : </label>
                        <div class="row">
                            <div class="col">
                                <input type='text' class="form-control show" id="trx_date" name="trx_date" value="{{ \Penolong::dmy($data->created_at) }}" readonly>
                            </div>
                            <div class="col">
                                <input type='text' class="form-control timepicker show" id="trx_time" name="trx_time" value="{{ old('trx_time', $data->jam_komplain) }}" readonly>
                            </div>
                        </div>
                        <span class="text-danger error-text customer_id_err">
                            <strong id="customer_id-error"></strong>
                        </span>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Nama Penelpon : </label>
                                <input type="text" id="nama_penelpon" name="nama_penelpon" class="form-control show" value="{{ old('nama_penelpon', $data->nama_penelpon) }}" readonly>
                                <span class="text-danger error-text name_err">
                                    <strong id="name-error"></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Nomor Telepon : <b class="label-required">*</b></label>
                                <input type="text" id="no_telp" name="no_telp" class="form-control show" value="{{ old('no_telp', $data->no_telp) }}" readonly>
                                <span class="text-danger error-text no_telp_err">
                                    <strong id="no_telp-error"></strong>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-8">
            {{-- customer --}}
            <div class="card card-warning">
                <div class="card-header">
                    <h5>Identitas Pelanggan</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Outlet Name : </label>
                                <input type="text" id="cust_id" name="cust_id" class="form-control show" value="{{ $dataCust->no_pelanggan }} - {{ $dataCust->outlet_name }}" readonly>
                                <span class="text-danger error-text customer_id_err">
                                    <strong id="customer_id-error"></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Branch : </label>
                                <input type="text" id="branch" name="branch" class="form-control show" value="{{ $dataBranch->name ?? ''}}" readonly>
                                <input type="hidden" id="branch_id" name="branch_id" value="" readonly>
                                <span class="text-danger error-text branch_err">
                                    <strong id="branch-error"></strong>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Nama Sesuai NIK : </label>
                                <input type="text" id="outlet_owner" name="outlet_owner" class="form-control show" value="{{ old('outlet_owner', $dataCust->realname) }}" readonly>
                                <span class="text-danger error-text outlet_owner_err">
                                    <strong id="outlet_owner-error"></strong>
                                </span>
                            </div>
                            <div class="form-group">
                                <label>Nomor Telepon : </label>
                                <input type="text" id="phone_no" name="phone_no" class="form-control show" value="{{ old('phone_no', $dataCust->phone_no) }}" readonly>
                                <span class="text-danger error-text phone_no_err">
                                    <strong id="phone_no-error"></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Alamat Pelanggan : </label>
                                <textarea class="form-control show" id="alamat" name="alamat" readonly>{{ old('alamat', $dataCust->alamat) }}</textarea>
                                <span class="text-danger error-text alamat_err">
                                    <strong id="alamat-error"></strong>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Type Outlet <b class="label-required">*</b></label>
                                <input type="text" id="type_outlet" name="type_outlet" class="form-control show" value="{{ old('type_outlet', $dataCust->outlet) }}" readonly>
                                <span class="text-danger error-text name_err">
                                    <strong id="type_outlet-error"></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Nama Pemilik <b class="label-required">*</b></label>
                                <input type="text" id="outlet_name" name="outlet_name" class="form-control show" value="{{ old('outlet_name', $dataCust->outlet_name) }}" readonly>
                                <span class="text-danger error-text name_err">
                                    <strong id="outlet_name-error"></strong>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Nama Salesman : </label>
                                <input type="text" id="sales_name" name="sales_name" class="form-control show" value="{{ old('sales_name', $dataCust->salesname) }}" readonly>
                                <span class="text-danger error-text sales_name_err">
                                    <strong id="sales_name-error"></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Nama SPV Sales : </label>
                                <input type="text" id="spv_sales" name="spv_sales" class="form-control show" value="{{ old('spv_sales', $dataCust->spv_sales) }}" readonly>
                                <span class="text-danger error-text spv_sales_err">
                                    <strong id="spv_sales-error"></strong>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($data->main_status_id == \App\Models\Ticket::ToBeRevised)
        <div class="card card-warning">
            <div class="card-header">
                <h5>Update Status Ticket</h5>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label>Catatan</label>
                    <textarea class="form-control show" id="notes" name="notes" disabled>{{ $data->notes }}</textarea>
                </div>
                <div class="form-group">
                    <label>Update Status : </label>
                    <select class="form-control" style="width: 50%;" name="main_status_id" id="main_status_id" disabled>
                        <option value=""> Pilih </option>
                        <option value="102" @if($data->main_status_id == 102) selected @endif>Progress</option>
                        <option value="103" @if($data->main_status_id == 103) selected @endif>To Be Revised</option>
                        <option value="200" @if($data->main_status_id == 200) selected @endif>Closed</option>
                    </select>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col">
            <div class="card card-warning">
                <div class="card-header">
                    <h5>Rincian Komplain</h5>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Variable Complain : </label>
                        <select class="form-control show" style="width: 100%;" name="complain" id="complain" readonly>
                            <option value=""> Pilih </option>
                            @foreach ($complain as $id => $item)
                                <option value="{{ $id }}" @if($data->complain_id == $id) selected @endif>{{ $item }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger error-text complain_id_err">
                            <strong id="complain_id-error"></strong>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Detail Permasalahan : </label>
                        <textarea class="form-control show" id="description" name="description" readonly>{{ old('description', $data->description) }}</textarea>
                        <span class="text-danger error-text description_err">
                            <strong id="description-error"></strong>
                        </span>
                    </div>
                    <span>Diisi apabila pada Variable Complain terdapat tanda *</span>
                    <div class="form-group">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Item Pesanan</th>
                                    <th>Jumlah Pesanan</th>
                                    {{-- <th>
                                        <button type="button" id="add_item" class="btn btn-primary btn-sm show">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </th> --}}
                                </tr>
                            </thead>
                            <tbody id="rn_items" class="add">
                                <tr></tr>
                                @if (count($dt) > 0)
                                    @foreach ($dt as $key => $item)
                                    <tr data-id="{{ $item->id }}">
                                        <td>
                                            <input type="hidden" id="ticket_complain_detail_id_{{ $item->id }}" name="ticket_complain_detail_id[]" class="item form-control" value="{{ $item->id }}">
                                            <input type="text" id="item_{{ $item->id }}" name="item[]" class="item form-control" value="{{ $item->item }}" readonly>
                                            <input type="hidden" id="is_delete_{{ $item->id }}" name="is_delete[]" class="item form-control" value="0">
                                        </td>
                                        <td>
                                            <input type="text" id="order_{{ $item->id }}" name="order[]" class="order form-control" value="{{ $item->qty }}" readonly>
                                        </td>
                                        {{-- <td>
                                            <a href="javascript:;" class="remove-item btn btn-danger btn-sm cancel" id="cancel_{{ $item->id }}" onclick="cancelDetail('{{ $item->id }}');">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td> --}}
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                            <tbody id="rn_items" class="show">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{-- analisa masalah --}}
        <div class="col">
            <div class="card card-warning">
                <div class="card-header">
                    <h5>Analisa Masalah</h5>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Masalah</th>
                                    <th>
                                        <button type="button" id="add_analisa" class="btn btn-primary btn-sm show">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </th>
                                </tr>
                            </thead>
                            <input type="hidden" id="analisa_count" name="analisa_count" value="3">
                            <tbody id="tr_analisa" class="action">
                                <tr data-id="0">
                                    <td>
                                        Why 1
                                        <input type="text" id="analisa_masalah_0" name="analisa_masalah[]" class="item form-control" value="">
                                        <span class="text-danger error-text name_err">
                                            <strong id="outlet_name-error"></strong>
                                        </span>
                                        <span class="text-danger error-text analisa_masalah0_err">
                                            <strong id="analisa_masalah0-error"></strong>
                                        </span>
                                    </td>
                                </tr>
                                <tr data-id="1">
                                    <td>
                                        Why 2
                                        <input type="text" id="analisa_masalah_1" name="analisa_masalah[]" class="item form-control" value="">
                                        <span class="text-danger error-text analisa_masalah1_err">
                                            <strong id="analisa_masalah1-error"></strong>
                                        </span>
                                    </td>
                                </tr>
                                <tr data-id="2">
                                    <td>
                                        Why 3
                                        <input type="text" id="analisa_masalah_2" name="analisa_masalah[]" class="item form-control" value="">
                                        <span class="text-danger error-text analisa_masalah2_err">
                                            <strong id="analisa_masalah2-error"></strong>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <span>(Gunakan Analisa dengan 5W / Why 5 kali, atau metode lainnya)</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-warning">
        <div class="card-header">
            <h5>Realisasi Tindak Lanjut</h5>
        </div>
        <div class="card-body">
            <div class="form-group">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>BM Action</th>
                            <th>CS Action</th>
                            <th>BM Status</th>
                            <th>CS Status</th>
                            <th>
                                <button type="button" id="add_action" class="btn btn-primary btn-sm action">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="tr_action" class="action">
                        @foreach ($action as $item => $row)
                            <tr data-id={{ $row->id }}>
                                <td>
                                    <input type='hidden' id="action_id_{{ $row->id }}" name="action_id[]" value="{{ $row->id }}" readonly>
                                    <input type='text' class="form-control show" id="action_date_{{ $row->id }}" name="action_date[]" value="{{ $row->trx_date }}" readonly>
                                </td>
                                <td>
                                    <input type="hidden" id="is_delete_{{ $row->id }}" name="is_delete[]" value="0">
                                    <textarea class="form-control show" cols="45" rows="10" style="width: 241px; height: 34px;" id="bm_action_{{ $row->id }}" name="bm_action[]">{{ $row->bm_action }}</textarea>
                                </td>
                                <td>
                                    <textarea class="form-control show" cols="45" rows="10" style="width: 241px; height: 34px;" id="cs_action_{{ $row->id }}" name="cs_action[]" readonly>{{ $row->cs_action }}</textarea>
                                </td>
                                <td>
                                    <select class="form-control show" style="width: 100%;" name="bm_status[]" id="bm_status_{{ $row->id }}">
                                        <option value=""> Pilih </option>
                                        <option value="102" @if($row->bm_status == 102) selected @endif>In Progress</option>
                                        <option value="200" @if($row->bm_status == 200) selected @endif>Done</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control show" style="width: 100%;" name="cs_status[]" id="cs_status_{{ $row->id }}" readonly>
                                        <option value=""> Pilih </option>
                                        <option value="102" @if($row->cs_status == 102) selected @endif>In Progress</option>
                                        <option value="200" @if($row->cs_status == 200) selected @endif>Done</option>
                                    </select>
                                </td>
                                <td>
                                    {{-- <a href="javascript:;" class="remove-item btn btn-danger btn-sm cancel" id="cancel_{{ $row->id }}" onclick="cancelAction('{{ $row->id }}');">
                                        <i class="fa fa-trash"></i>
                                    </a> --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        {{-- Hasil Tindak Lanjut --}}
        <div class="col-md-6">
            <div class="card card-warning">
                <div class="card-header">
                    <h5>Hasil Tindak Lanjut</h5>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="code">Upload File Tindak Lanjut *</label>
                        <input class ="form-control" type="file" name="file" id="file" value="{{ old('file','') }}">
                        <span>max 2MB</span>
                    </div>
                    <div class="form-group">
                        <label>Hasil Tindak Lanjut : </label>
                        <textarea class="form-control action" id="result_action" name="result_action">{{ old('result_action', $data->hasil_investigasi) }}</textarea>
                        <span class="text-danger error-text result_action_err">
                            <strong id="result_action-error"></strong>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        {{-- Tindakan Pencegahan --}}
        <div class="col-md-6">
            <div class="card card-warning">
                <div class="card-header">
                    <h5>Tindakan Pencegahan</h5>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Tindakan Pencegahan : </label>
                        <textarea class="form-control action" id="action_pencegahan" name="action_pencegahan">{{ old('action_pencegahan', $data->tindak_pencegahan) }}</textarea>
                        <span class="text-danger error-text action_pencegahan_err">
                            <strong id="action_pencegahan-error"></strong>
                        </span>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" id="is_submit_bm" name="is_submit_bm" type="checkbox" id="gridCheck" value="200">
                        <label class="form-check-label" for="gridCheck">
                            <b>Submitted</b>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="float-right">
        <a class="btn btn-xs btn-danger" href="{{ route('admin.ticket.index') }}">
            <i class="fa fa-angle-double-left"></i> Kembali
        </a>
        <button type="submit" class="btn btn-save btn-primary" disabled>
            <i class="fa fa-save"></i> Submit
        </button>
    </div>
    </form>
</div>

@endsection
@push('custom-js')
    <script src="{{ asset('assets/js/pages/ticket-action.js') }}"></script>
@endpush

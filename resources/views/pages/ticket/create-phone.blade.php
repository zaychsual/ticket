@section('title', 'Ticket Form')
@extends('layouts.master')

@section('header', 'Ticket Form')
@section('content')
<div class="section-body">
    <form id="form_ticket" enctype="multipart/form-data">
        @csrf
        <div id="display-errors"></div>
        <div class="row sortable-card">
        <div class="col-12 col-md-6 col-lg-4">
            {{-- Tiket Head --}}
            <div class="card card-primary">
                <div class="card-header">
                    <h5>
                        Tiket Form
                    </h5>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Ticket Category : </label>
                        <select class="select2 form-control add" style="width: 100%;" name="ticket_category" id="ticket_category">
                            <option value="FRM-CSD-007"> Tiket Komplain</option>
                            <option value="FRM-CSD-019">Tiket Non Komplain</option>
                        </select>
                        <span class="text-danger error-text complain_id_err">
                            <strong id="complain_id-error"></strong>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Tanggal & Waktu Komplain : </label>
                        <div class="row">
                            <div class="col">
                                <input type='text' class="form-control show" id="trx_date" name="trx_date" value="{{date('Y-m-d'), old('trx_date')}}">
                            </div>
                            <div class="col">
                                <input type='text' class="form-control timepicker show" id="trx_time" name="trx_time" value="{{ date('H:i'), old('trx_time') }}">
                            </div>
                        </div>
                        <span class="text-danger error-text customer_id_err">
                            <strong id="customer_id-error"></strong>
                        </span>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Nama Penelpon : </label>
                                <input type="text" id="nama_penelpon" name="nama_penelpon" class="form-control show" value="{{ old('nama_penelpon') }}">
                                <span class="text-danger error-text name_err">
                                    <strong id="name-error"></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Nomor Telepon : <b class="label-required">*</b></label>
                                <input type="text" id="no_telp" name="no_telp" class="form-control show" value="{{ $id }}">
                                <input type="hidden" id="agent" name="agent" value="{{ $agent }}">
                                <input type="hidden" id="call" name="call" value="{{ $call }}">
                                <input type="hidden" id="campaign" name="campaign" value="{{ $campaign }}">
                                <input type="hidden" id="callid" name="callid" value="{{ $callid }}">
                                <input type="hidden" id="remote" name="remote" value="{{ $remote }}">
                                <span class="text-danger error-text no_telp_err">
                                    <strong id="no_telp-error"></strong>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-8">
            {{-- customer --}}
            <div class="card card-primary">
                <div class="card-header">
                    <h5>Identitas Pelanggan</h5>
                </div>
                <div class="card-body" id="identitas_customer">
                    <div id="cust_complain">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Nama Pelanggan : </label>
                                    <select class="select2 form-control add" style="width: 100%;" name="customer_id" id="customer_id">
                                        <option value=""> Pilih </option>
                                        @if ($dtCust)
                                            <option value="{{ $dtCust->id }}" selected >{{ $dtCust->no_pelanggan }}-{{ $dtCust->realname }}</option>
                                        @endif
                                    </select>
                                    <span class="text-danger error-text customer_id_err">
                                        <strong id="customer_id-error"></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Branch : </label>
                                    <input type="text" id="branch" name="branch" class="form-control add" value="{{ $dtCust->branch ?? '' }}" readonly>
                                    <input type="hidden" id="branch_id" name="branch_id" value="" readonly>
                                    <span class="text-danger error-text branch_err">
                                        <strong id="branch-error"></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Nama Pemilik Outlet : </label>
                                    <input type="text" id="outlet_owner" name="outlet_owner" class="form-control add" value="{{ $dtCust->outlet_name ?? '' }}" readonly>
                                    <span class="text-danger error-text outlet_owner_err">
                                        <strong id="outlet_owner-error"></strong>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>Nomor Telepon : </label>
                                    <input type="text" id="phone_no" name="phone_no" class="form-control add" value="{{ $id }}" readonly>
                                    <span class="text-danger error-text phone_no_err">
                                        <strong id="phone_no-error"></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Alamat Pelanggan : </label>
                                    <textarea class="form-control add" id="alamat" name="alamat" readonly>{{ $dtCust->alamat ?? '' }}</textarea>
                                    <span class="text-danger error-text alamat_err">
                                        <strong id="alamat-error"></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Type Outlet <b class="label-required">*</b></label>
                                    <input type="text" id="type_outlet" name="type_outlet" class="form-control add" value="{{ $dtCust->getOutlet['name'] ?? '' }}" readonly>
                                    <span class="text-danger error-text name_err">
                                        <strong id="type_outlet-error"></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Outlet Name <b class="label-required">*</b></label>
                                    <input type="text" id="outlet_name" name="outlet_name" class="form-control add" value="{{ $dtCust->outlet_name ?? '' }}" readonly>
                                    <span class="text-danger error-text name_err">
                                        <strong id="outlet_name-error"></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Nama Salesman : </label>
                                    <input type="text" id="sales_name" name="sales_name" class="form-control add" value="{{ $dtCust->salesname ?? '' }}" readonly>
                                    <span class="text-danger error-text sales_name_err">
                                        <strong id="sales_name-error"></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Nama SPV Sales : </label>
                                    <input type="text" id="spv_sales" name="spv_sales" class="form-control add" value="{{ $dtCust->spv_sales ?? '' }}" readonly>
                                    <span class="text-danger error-text spv_sales_err">
                                        <strong id="spv_sales-error"></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="cust_non_complain" style="display: none;">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Nama Pelanggan : <b>* required</b></label>
                                    <input type="text" id="realname" name="realname" class="form-control add" value="" disabled>
                                    <span class="text-danger error-text realname_err">
                                        <strong id="realname-error"></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Branch : <b>* required</b></label>
                                    <select class="select2 form-control add" style="width: 100%;" name="branch_id" id="branch_id" disabled>
                                        <option value=""> Pilih </option>
                                        @foreach($branch as $id => $item)
                                            <option value="{{ $item->id }}" data-id="{{ $item->code }}">{{ $item->code }}-{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger error-text branch_id_err">
                                        <strong id="branch_id-error"></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>No Pelanggan : </label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div>
                                        <input type="text" class="form-control add" id="branch_code" name="branch_code" value="" readonly disabled>
                                    </div>
                                </div>
                                <input type="text" class="form-control add" id="no_pelanggan" name="no_pelanggan" onKeyUp="numericFilter(this);" value="" disabled>
                            </div>
                            <span class="text-danger error-text no_pelanggan_err">
                                <strong id="no_pelanggan-error"></strong>
                            </span>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Nama Outlet : <b>* required</b></label>
                                    <input type="text" id="outlet_name" name="outlet_name" class="form-control add" value="" disabled>
                                    <span class="text-danger error-text outlet_name_err">
                                        <strong id="outlet_name-error"></strong>
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>Nomor Telepon : <b>* required</b></label>
                                    <input type="text" id="phone_no" name="phone_no" class="form-control add" value="{{ $id }}" disabled>
                                    <span class="text-danger error-text phone_no_err">
                                        <strong id="phone_no-error"></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Alamat Pelanggan : <b>* required</b></label>
                                    <textarea class="form-control add" id="alamat" name="alamat" disabled></textarea>
                                    <span class="text-danger error-text alamat_err">
                                        <strong id="alamat-error"></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>SR/RT/SG/G</label>
                            <select class="select2 form-control add" style="width: 100%;" name="type_outlet_id" id="type_outlet_id" disabled>
                                <option value=""> Pilih </option>
                                @foreach ($outlet as $id => $item)
                                    <option value="{{ $id }}">{{ $item }}</option>
                                @endforeach
                            </select>
                            <span class="text-danger error-text type_outlet_id_err">
                                <strong id="type_outlet_id-error"></strong>
                            </span>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Nama Salesman : </label>
                                    <input type="text" id="sales_name" name="sales_name" class="form-control add" value="{{ old('sales_name') }}" disabled>
                                    <span class="text-danger error-text sales_name_err">
                                        <strong id="sales_name-error"></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Nama Sales Supervisor : </label>
                                    <input type="text" id="spv_sales" name="spv_sales" class="form-control add" value="{{ old('spv_sales') }}" disabled>
                                    <span class="text-danger error-text spv_sales_err">
                                        <strong id="spv_sales-error"></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card card-primary">
                <div class="card-header">
                    <h5>Rincian Komplain</h5>
                </div>
                <div class="card-body">
                    <div class="form-group" id="vb_complain">
                        <label>Variable Complain : </label>
                        <select class="select2 form-control show" style="width: 100%;" name="complain_id" id="complain_id">
                            <option value=""> Pilih </option>
                            @foreach ($qryComplain as $id => $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger error-text complain_id_err">
                            <strong id="complain_id-error"></strong>
                        </span>
                    </div>
                    <div class="form-group" id="vb_noncomplain" style="display: none;">
                        <label>Variable Complain : </label>
                        <select class="form-control show" style="width: 100%;" name="complain_id" id="complain_id" disabled>
                            <option value=""> Pilih </option>
                            @foreach ($qryNonComplain as $id => $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger error-text complain_id_err">
                            <strong id="complain_id-error"></strong>
                        </span>
                    </div>
                    <div class="form-group">
                        <label>Detail Permasalahan : </label>
                        <textarea class="form-control show" id="description" name="description">{{ old('description') }}</textarea>
                        <span class="text-danger error-text description_err">
                            <strong id="description-error"></strong>
                        </span>
                    </div>
                    <span>Diisi apabila pada Variable Complain terdapat tanda *</span>
                    <div class="form-group">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Item Pesanan</th>
                                    <th>Jumlah Pesanan</th>
                                    <th>
                                        <button type="button" id="add_item" class="btn btn-primary btn-sm show">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="rn_items" class="add">
                                <tr></tr>
                            </tbody>
                            <tbody id="rn_items" class="show">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-primary" id="realisasi_tindak_lanjut">
        <div class="card-header">
            <h5>Realisasi Tindak Lanjut</h5>
        </div>
        <div class="card-body">
            <div class="form-group">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Tindak Lanjut</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody id="problem_items" class="add">
                        <tr>
                            <td>
                                <input type='text' class="form-control datepicker add" id="problem_date" name="problem_date" value="{{ old('problem_date') }}">
                            </td>
                            <td>
                                <textarea class="form-control add" cols="45" rows="10" style="width: 241px; height: 34px;" id="masalah" name="masalah"></textarea>
                            </td>
                            <td>
                                <div class="col-sm-9">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="sub_status" id="gridRadios1" value="102">
                                        <label class="form-check-label" for="gridRadios1">
                                            In Progress
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="sub_status" id="gridRadios2" value="200">
                                        <label class="form-check-label" for="gridRadios2">
                                            Done
                                        </label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </form>
</div>
<div class="float-right">
    <a class="btn btn-xs btn-danger" href="{{ route('admin.ticket.index') }}">
        <i class="fa fa-angle-double-left"></i> Kembali
    </a>
    <button type="button" id="btn-action" class="btn btn-save btn-primary">
        <i class="fa fa-save"></i> Submit
    </button>
</div>
@endsection
@push('custom-js')
    <script src="{{ asset('assets/js/pages/ticket-phone.js') }}"></script>
@endpush

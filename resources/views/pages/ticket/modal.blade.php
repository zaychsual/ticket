@section('modal')
{{-- create-modal --}}
<div id="createModal" class="modal fade" data-backdrop="static">
    <div class="modal-dialog modal-huge">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">New Ticket <span id="ticket-category"></span></h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="modal-store" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    {{-- ?? --}}
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="btn-save" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- edit modal --}}
<div class="modal fade" data-backdrop="static" id="updateModal">
    <div class="modal-dialog modal-huge">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h5 class="text-white modal-title">Edit Ticket</h5>
                <button type="button" class="text-white close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="modal-update" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    {{-- ?? --}}
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="btn-update" class="btn btn-primary" @if(\Penolong::cekRole(3)) disabled @endif><i class="fa fa-save"></i> Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- show modal --}}
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="showModal">
    <div class="modal-dialog modal-huge" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="text-white modal-title">Show Ticket</h5>
                <button type="button" class="text-white close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- ?? --}}
            </div>
        </div>
    </div>
</div>

@endsection

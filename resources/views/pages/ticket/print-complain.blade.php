<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
        <title></title>
        <meta name="generator" content="LibreOffice 5.4.7.2 (Linux)">
        <meta name="author" content="Zaynd Zaynd">
        <meta name="created" content="2021-08-03T11:52:28">
        <meta name="changedby" content="Zaynd Zaynd">
        <meta name="changed" content="2021-08-04T13:50:41">
        <meta name="AppVersion" content="12.0000">
        <meta name="Company" content="Grizli777">
        <meta name="DocSecurity" content="0">
        <meta name="HyperlinksChanged" content="false">
        <meta name="LinksUpToDate" content="false">
        <meta name="ScaleCrop" content="false">
        <meta name="ShareDoc" content="false">

        <style type="text/css">
            body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:x-small }
            a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  }
            a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  }
            comment { display:none;  }
        </style>
    </head>
    <body>
        <table cellspacing="0" border="0">
            <colgroup width="73"></colgroup>
            <colgroup width="257"></colgroup>
            <colgroup width="17"></colgroup>
            <colgroup width="149"></colgroup>
            <colgroup span="5" width="68"></colgroup>
            <colgroup width="86"></colgroup>
            <colgroup width="19"></colgroup>
            <colgroup width="20"></colgroup>
            <colgroup width="48"></colgroup>
            <colgroup width="44"></colgroup>
            <colgroup width="14"></colgroup>
            <colgroup width="25"></colgroup>
            <colgroup span="6" width="26"></colgroup>
            <colgroup width="42"></colgroup>
            <colgroup span="2" width="64"></colgroup>
            <tbody>
                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="bottom"><font color="#000000"><br></font></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" rowspan="3" align="center" valign="middle">
                        <font face="Arial Narrow" color="#000000">
                            <img src="{{ asset('avatars/logo-fastrata.png') }}" width="90" height="54">
                        </font>
                    </td>
                    <td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="10" rowspan="3" align="center" valign="middle"><b><font face="Arial Narrow" size="4" color="#000000">TIKET KOMPLAIN </font></b></td>
                    <td style="border-left: 2px solid #000000" align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">NO. REF</font></b></td>
                    <td align="left" valign="middle"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">:</font></b></td>
                    <td style="border-right: 2px solid #000000" colspan="9" align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">{{ $data->no_ref }}</font></b></td>
                    <td align="left" valign="bottom"><font color="#000000"></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-left: 2px solid #000000" colspan="2" align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">CABANG/SP/FS</font></b></td>
                    <td align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">:</font></b></td>
                    <td style="border-right: 2px solid #000000" colspan="9" align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">{{ $data->getBranch['name'] }}</font></b></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000" colspan="2" align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">TANGGAL</font></b></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">:</font></b></td>
                    <td style="border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan="9" align="left" valign="bottom"><font face="Trebuchet MS">{{ \Penolong::dmy($data->created_at) }}</font></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-left: 2px solid #000000" align="left" valign="middle"><font face="Arial Narrow" color="#000000">NAMA PENELEPON</font></td>
                    <td align="left" valign="middle"><font face="Arial Narrow" color="#000000">:</font></td>
                    <td style="border-right: 2px solid #000000" colspan="21" align="left" valign="middle">
                        <b><font face="Arial Narrow" color="#000000">{{ $data->nama_penelpon }}</font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-right: 2px solid #000000" height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="middle"><font face="Arial Narrow">NOMOR TELEPON</font></td>
                    <td align="left" valign="middle"><font face="Arial Narrow" color="#000000">:</font></td>
                    <td style="border-right: 2px solid #000000" colspan="21" align="left" valign="middle" sdnum="1033;0;0">
                        <b><font face="Arial Narrow" color="#000000">{{ $data->no_telp }}</font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-right: 2px solid #000000" height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="middle"><font face="Arial Narrow">JAM KOMPLAIN</font></td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="middle"><font face="Arial Narrow" color="#000000">:</font></td>
                    <td style="border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan="21" align="left" valign="middle" sdval="0.600694444444444" sdnum="1033;1033;H:MM">
                        <b><font face="Arial Narrow" color="#000000">{{ $data->jam_komplain }}</font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-right: 2px solid #000000" height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-top: 2px solid #000000; border-right: 2px solid #000000" colspan="23" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000">A. IDENTITAS PELANGGAN </font></b></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-right: 2px solid #000000" height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="middle"><font face="Arial Narrow" color="#000000">NOMOR PELANGGAN </font></td>
                    <td align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">:</font></b></td>
                    <td style="border-right: 2px solid #000000" colspan="5" align="left" valign="middle"><b><font face="Arial Narrow">{{ $dataCust->no_pelanggan }}</font></b></td>
                    <td colspan="3" align="left" valign="middle"><font face="Arial Narrow" color="#000000">NAMA PEMILIK OUTLET</font></td>
                    <td align="left" valign="middle"><font face="Arial Narrow" color="#000000">:</font></td>
                    <td style="border-right: 2px solid #000000" colspan="12" align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">{{ $dataCust->realname }}</font></b></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-right: 2px solid #000000" height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="middle"><font face="Arial Narrow" color="#000000">NAMA PELANGGAN</font></td>
                    <td align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">:</font></b></td>
                    <td style="border-right: 2px solid #000000" colspan="5" align="left" valign="middle">
                        <b><font face="Arial Narrow">{{ $dataCust->outlet_name }}</font></b></td>
                    <td colspan="3" align="left" valign="middle"><font face="Arial Narrow" color="#000000">NAMA SALESMAN</font></td>
                    <td align="left" valign="middle"><font face="Arial Narrow" color="#000000">:</font></td>
                    <td colspan="12" align="left" valign="middle" style="border-right: 2px solid #000000">
                        <b><font face="Arial Narrow" color="#000000">{{ $dataCust->salesname }}</font></b></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-right: 2px solid #000000" height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td rowspan="2" align="left" valign="middle"><font face="Arial Narrow" color="#000000">ALAMAT PELANGGAN</font></td>
                    <td align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">:</font></b></td>
                    <td style="border-right: 2px solid #000000" colspan="5" rowspan="2" align="left" valign="top">
                        <b><font face="Arial Narrow"><p>{{ $dataCust->alamat }}</p></font></b>
                    </td>
                    <td colspan="3" align="left" valign="middle"><font face="Arial Narrow" color="#000000">NAMA SPV SALES</font></td>
                    <td align="left" valign="middle"><font face="Arial Narrow" color="#000000">:</font></td>
                    <td style="border-right: 2px solid #000000" colspan="12" align="left" valign="middle">
                        <b><font face="Arial Narrow" color="#000000">{{ $dataCust->spv_sales }}</font></b></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-right: 2px solid #000000" height="21" align="left" valign="bottom">
                        <font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="middle"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td style="border-right: 2px solid #000000" colspan="16" rowspan="2" align="center" valign="middle"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-right: 2px solid #000000" height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="middle"><font face="Arial Narrow" color="#000000">TYPE OUTLET</font></td>
                    <td align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">:</font></b></td>
                    <td style="border-right: 2px solid #000000" colspan="5" align="left" valign="middle">
                        <b><font face="Arial Narrow">{{ $dataCust->getOutlet['name'] }}</font></b></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="23" align="left" valign="middle" bgcolor="#D9D9D9">
                        <b><font face="Arial Narrow" color="#000000">B. INFORMASI KOMPLAIN</font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-left: 2px solid #000000" rowspan="2" align="left" valign="middle"><font face="Arial Narrow" color="#000000">VARIABEL KOMPLAIN</font></td>
                    <td rowspan="2" align="center" valign="middle"><b><font face="Arial Narrow" size="1" color="#000000">:</font></b></td>
                    <td style="border-right: 2px solid #000000" colspan="21" rowspan="2" align="left" valign="middle">
                        <font face="Arial Narrow" size=2 color="#000000">
                            <p>
                                @foreach ($variable as $key => $var)
                                    <input type="checkbox" name="variable[]" id="variable_{{ $key+1 }}" value="{{ $var->id }}" @if($data->complain_id == $var->id) checked @endif>
                                    <label for="variable_{{ $key+1 }}">{{ $var->name }}</label>
                                @endforeach
                            </p>
                        </font>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="23" align="left" valign="middle" bgcolor="#D9D9D9">
                        <b><font face="Arial Narrow" color="#000000">DETAIL PERMASALAHAN</font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="23" rowspan="2" align="left" valign="middle">
                        <font face="Arial Narrow" color="#000000"><p>{{ $data->description }}</p></font>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="7" align="center" valign="middle">
                        <b><font face="Arial Narrow" color="#000000">ITEM PESANAN</font></b>
                    </td>
                    <td style="border-bottom: 1px solid #000000; border-right: 2px solid #000000" colspan="16" align="center" valign="middle">
                        <b><font face="Arial Narrow" color="#000000">JUMLAH PESANAN</font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                @if (count($dt) > 0)
                    @foreach ($dt as $key => $var)
                        <tr>
                            <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="7" align="left" valign="middle">
                                <b><font face="Arial Narrow" color="#000000">{{ $var->item }}</font></b>
                            </td>
                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 2px solid #000000" colspan="16" align="left" valign="middle">
                                <b><font face="Arial Narrow" color="#000000">{{ $var->qty }}</font></b>
                            </td>
                            <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                        <td style="border-top: 1px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="7" align="left" valign="middle"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                        <td style="border-top: 1px solid #000000; border-bottom: 2px solid #000000;border-right: 2px solid #000000" colspan="16" align="left" valign="middle"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                        <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                    </tr>
                @endif
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-left: 2px solid #000000" align="left" valign="middle" bgcolor="#BFBFBF">
                        <b><font face="Arial Narrow" color="#000000">C. ANALISA AKAR MASALAH (Gunakan Analisa dengan 5W / Why 5 kali, atau metode lainnya)</font></b>
                    </td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td style="border-right: 2px solid #000000" align="center" valign="middle" bgcolor="#BFBFBF"><font face="Arial Narrow" color="#000000"></font></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="23" rowspan="2" align="left" valign="top">
                        <font face="Arial Narrow" color="#000000">
                            @if (count($analisa) > 0)
                                @foreach ($analisa as $item => $lisa)
                                    {{-- Why {{ $item+1 }} : <br> --}}
                                    {{ $lisa->analisa_masalah }} <br>
                                @endforeach
                            @else
                                {{ $data->analisa_masalah }}
                            @endif
                        </font>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>


                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td style="border-right: 2px solid #000000" height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9">
                        <b><font face="Arial Narrow" color="#000000">D. TINDAK LANJUT</font></b>
                    </td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td style="border-right: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow" color="#000000"><br></font></b></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" align="left" valign="middle">
                        <b><font face="Arial Narrow" color="#000000">D1. REALISASI TINDAK LANJUT</font></b>
                    </td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="22" align="center" valign="middle">
                        <b><font face="Arial Narrow" color="#000000"><br></font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                @if (count($action) > 0)
                    @foreach ($action as $key => $var)
                        @php
                            $tglindo = date_create($var->trx_date);
                        @endphp
                        <tr>
                            <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                            <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" rowspan="4" align="center" valign="middle">
                                <b><font face="Arial Narrow" color="#000000">HARI {{ $key+1 }}</font></b>
                            </td>
                            <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000" colspan="2" align="left" valign="middle">
                                <b><font face="Arial Narrow" color="#000000">Tanggal :</font></b>
                            </td>
                            <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan="12" align="left" valign="middle" sdval="0" sdnum="1033;1033;D-MMM-YY">
                                <b><font face="Arial Narrow" color="#000000">{{ date_format($tglindo,"d M Y") }}</font></b>
                            </td>
                            <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="8" align="left" valign="middle">
                                <b><font face="Arial Narrow" color="#000000">Status</font></b>
                            </td>
                            <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                        </tr>
                        <tr>
                            <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                            <td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="14" align="left" valign="middle">
                                <b><font face="Arial Narrow" color="#000000">Tindak Lanjut :</font></b>
                            </td>
                            <td style="border-top: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="8" rowspan="3" align="center" valign="middle">
                                <font face="Arial Narrow" color="#000000">
                                    <p>
                                        <input type="checkbox" name="action_status[]" id="action_status_{{ $key+1 }}" value="102" @if($var->cs_status == 102) checked @endif>
                                        <label for="action_status_{{ $key+1 }}">In Progress</label>
                                        <input type="checkbox" name="action_status[]" id="action_status_{{ $key+1 }}" value="200" @if($var->cs_status == 200) checked @endif>
                                        <label for="action_status_{{ $key+1 }}">Done</label>
                                    </p>
                                </font>
                            </td>
                            <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                        </tr>
                        <tr>
                            <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                            <td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="14" rowspan="2" align="left" valign="top" sdnum="1033;1033;M/D/YYYY H:MM">
                                <b>
                                    <font face="Arial Narrow" color="#000000">
                                        {{ $key+1 }}. CS :<br>
                                        <p>{{ $var->cs_action }}</p>
                                        BM: <br>
                                        <p>{{ $var->bm_action }}</p>
                                    </font>
                                </b>
                            </td>
                            <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                        </tr>
                         <tr>
                            <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                            <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                        </tr>
                    @endforeach
                @endif

                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="23" align="left" valign="middle">
                        <b><font face="Arial Narrow">D2. HASIL TINDAK LANJUT</font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="23" rowspan="2" align="left" valign="top">
                        <b><font face="Trebuchet MS"><p>{{ $data->hasil_investigasi }}</p></font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9">
                        <b><font face="Arial Narrow">E. TINDAKAN PENCEGAHAN</font></b>
                    </td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9"><b><font face="Arial Narrow"><br></font></b></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-right: 2px solid #000000" align="left" valign="middle" bgcolor="#D9D9D9">
                        <b><font face="Arial Narrow"><br></font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="23" rowspan="2" align="left" valign="top">
                        <b><font face="Trebuchet MS"><p>{{ $data->tindak_pencegahan }}</p></font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="8" align="left" valign="middle"><b><font face="Arial Narrow">CATATAN :</font></b></td>
                    <td colspan="4" align="left" valign="middle"><b><font face="Arial Narrow" color="#000000">Dibuat Oleh,</font></b></td>
                    <td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="5" align="left" valign="middle">
                        <b><font face="Arial Narrow" color="#000000">Ditindaklanjuti Oleh,</font></b>
                    </td>
                    <td style="border-right: 2px solid #000000" colspan="6" align="left" valign="middle">
                        <b><font face="Arial Narrow" color="#000000">Disetujui Oleh,</font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="8" rowspan="5" align="left" valign="top">
                        <font face="Arial Narrow">Point A &amp; B diisi oleh CS KP sedangkan point C s.d E diisi oleh cabang.</font>
                    </td>
                    <td style="border-top: 2px solid #000000" colspan="4" align="left" valign="middle"><font face="Arial Narrow" color="#000000">Customer Service Staff</font></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan="5" align="left" valign="middle">
                        <font face="Arial Narrow" color="#000000">SM / BM</font>
                    </td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan="6" align="left" valign="middle">
                        <font face="Arial Narrow" color="#000000">Customer Service Manajer</font>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-top: 2px solid #000000; border-right: 2px solid #000000" colspan="4" rowspan="2" align="center" valign="middle"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td style="border-right: 2px solid #000000" colspan="5" rowspan="2" align="center" valign="middle"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td style="border-right: 2px solid #000000" colspan="6" rowspan="2" align="center" valign="middle"><font face="Arial Narrow" color="#000000"><br></font></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="middle"><font face="Arial Narrow" color="#000000">Tanggal :</font></td>
                    <td style="border-right: 2px solid #000000" colspan="3" align="left" valign="middle" sdnum="1033;1033;D-MMM-YY">
                        <font face="Arial Narrow" color="#000000">{{ \Penolong::dmy($data->created_at) }}</font>
                    </td>
                    <td align="left" valign="middle"><font face="Arial Narrow" color="#000000">Tanggal :</font></td>
                    <td style="border-right: 2px solid #000000" colspan="4" align="left" valign="middle" sdval="44250" sdnum="1033;1033;D-MMM-YY">
                        <font face="Arial Narrow" color="#000000">{{ \Penolong::dmy($data->updated_at) }}</font>
                    </td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="middle">
                        <font face="Arial Narrow" color="#000000">Tanggal</font>
                    </td>
                    <td style="border-bottom: 2px solid #000000" align="left" valign="middle">
                        <font face="Arial Narrow" color="#000000">:</font>
                    </td>
                    <td style="border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan="4" align="left" valign="middle">
                        <font face="Arial Narrow" color="#000000">{{ \Penolong::dmy($data->updated_at) }}</font></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="22" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan="4" align="left" valign="middle">
                        <b><font face="Arial Narrow" color="#000000">Nama: {{ \Penolong::getName($data->created_by)->name ?? '' }}</font></b>
                    </td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan="5" align="left" valign="middle">
                        <b><font face="Arial Narrow" color="#000000">Nama: {{ \Penolong::getBranchUser($data->branch_code)->name ?? '' }}</font></b>
                    </td>
                    <td style="border-top: 2px solid #000000; border-bottom: 2px solid #000000; border-right: 2px solid #000000" colspan="6" align="left" valign="middle">
                        <b><font face="Arial Narrow" color="#000000">Nama: Dina Novianti</font></b>
                    </td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
                <tr>
                    <td height="21" align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="left" valign="bottom"><font face="Trebuchet MS"><br></font></td>
                    <td align="right" valign="top"><i><font face="Arial Narrow">Hardcopy/Softcopy by scan -1 Rangkap - Adm BM/Customer Service Staff<br>{{ date('d-m-Y') }}</font></i></td>
                    <td align="left" valign="bottom"><font color="#000000"><br></font></td>
                </tr>
            </tbody>
        </table>
    </body>
    <script>
        window.print()
    </script>
</html>

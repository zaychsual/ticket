@section('title', 'Ticket')
@extends('layouts.master')

@section('header', 'Ticket')
@section('content')
<div class="section-body">
    <div class="row mt-4">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @if (\Penolong::cekAkses('Tiket','add'))
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Add New
                            </button>
                            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(79px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <a class="dropdown-item add-new" data-id="11" href="#" value="11">Ticket Complain</a>
                                <a class="dropdown-item add-new" data-id="22" href="#" value="22">Ticket Non Complain</a>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="ajax-table" class="table table-bordered table-striped table-hover datatable datatable-Role">
                            <thead>
                                <tr>
                                    <th>No. Reff</th>
                                    <th>Category</th>
                                    <th>Caller Name</th>
                                    <th>Customer Name</th>
                                    <th>Branch</th>
                                    <th>Complain</th>
                                    <th>Progress</th>
                                    <th>Created By</th>
                                    <th>Created At</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        {{-- {{ $data->links() }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('pages.ticket.modal')
@endsection
@push('custom-js')
    <script src="{{ asset('assets/js/pages/ticket.js') }}"></script>
@endpush

<div class="main-sidebar">
	<aside id="sidebar-wrapper">
		<div class="sidebar-brand">
			<a href="{{ route('admin.dashboard') }}" class="navbar-brand">
                <img src="{{ asset('logo/logo.png') }}" class="sidebar-logo" width="100" class="shadow-light rounded-circle" alt="{{ env('APP_NAME') }}">
            </a><br>
            <span>{{ config('app.name') }}</span>
            {{-- <a href="index.html">Stisla</a> --}}
		</div>
		<div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('admin.dashboard') }}">
                <img src="{{ asset('logo/logo.png') }}" class="sidebar-logo" width="35" class="shadow-light rounded-circle">
            </a>
		</div>
        <ul class="sidebar-menu">
            @foreach (\Penolong::mainMenu() as $row => $mm)
                @php
                    $subMenu = \Penolong::cekSubMenu($mm->id);
                    $menuAktif = \Penolong::cekMenuAktif(Request::segment(2));
                @endphp
                <li @if(Request::segment(2) == $mm->menu_aktif || $menuAktif->master_menu == $mm->id) class="active" @endif>
                    <a href="@if(isset($mm->url)) {{ route('admin.'.$mm->url.'') }} @else # @endif"
                        @if($subMenu) class="nav-link has-dropdown" @endif>
                        <i class="{{ $mm->icon }}"></i>
                        <span>{{ $mm->name }}</span>
                    </a>
                    @if($subMenu)
                        <ul class="dropdown-menu">
                    @endif
                    @foreach (\Penolong::subMenu() as $sm)
                        @if ($sm->master_menu == $mm->id)
                        <li @if(Request::segment(2) == $sm->menu_aktif) class="active" @endif>
                            <a class="nav-link" href="{{ route('admin.'.$sm->url.'') }}">
                                {{ $sm->name }}
                            </a>
                        </li>
                        @endif
                    @endforeach
                    @if($subMenu)
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
	</aside>
</div>

<nav class="navbar navbar-expand-lg main-navbar">
    <form class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
            <li>
                <a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a>
            </li>
            <li>
                <a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a>
            </li>
        </ul>
    </form>
    <ul class="navbar-nav ml-auto">
        <li class="dropdown dropdown-list-toggle">
            <a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg @if(count(\Penolong::cekNotif()) > 0) beep @endif">
                <span class="badge badge-danger ml-2">{{ count(\Penolong::cekNotif()) }}</span> {{-- notif --}}
                <i class="far fa-bell"></i>
            </a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
                <div class="dropdown-header">
                    Notifications
                </div>
                <div class="dropdown-list-content dropdown-list-icons">
                    @foreach (\Penolong::cekNotif() as $key => $item )
                        <a href="{{ route('admin.ticket.action', $item->id) }}" class="dropdown-item dropdown-item-unread">
                            <div class="dropdown-item-icon
                                @if($item->main_status_id == 300) bg-danger
                                @elseif ($item->main_status_id == 102) bg-warning
                                @elseif ($item->main_status_id == 200) bg-success @endif
                            text-white">
                                <i class="fas fa-ticket-alt"></i>
                            </div>
                            <div class="dropdown-item-desc">
                                No. Reff: {{ $item->no_ref }}<br>Pelanggan : @if($item->customer_id == "") {{ $item->realname }} @else  {{ \Penolong::getCust($item->customer_id)->realname }} @endif
                                <div class="time text-primary">{{ \Penolong::time_elapsed_string($item->created_at) }}</div>
                            </div>
                        </a>
                    @endforeach
                </div>
                <div class="dropdown-footer text-center">
                    Notifications
                </div>
            </div>
        </li>
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="{{ asset('avatars/luffy-avatar.png') }}" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, {{ \Auth::user()->name }}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
                {{-- <a href="features-profile.html" class="dropdown-item has-icon">
                    <i class="far fa-user"></i> Profile
                </a> --}}
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item has-icon text-danger" onclick="event.preventDefault();document.getElementById('formLogOut').submit();">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>
                <form id="formLogOut" action="{{ route('logout') }}" method="POST">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</nav>

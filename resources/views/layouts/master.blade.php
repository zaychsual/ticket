<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>{{ config('app.name') }} - @yield('title')</title>

    {{-- General CSS Files --}}
    {{-- <link rel="stylesheet" href="{{ asset('assets/bootstrap/bootstrap.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('assets/bootstrap-4.6.0-dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/fontawesome/all.css') }}">
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('logo/logo.png') }}">

    {{-- CSS Modules --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/modules/DataTables/datatables.min.css')}}"/>
    <link href="{{asset('assets/modules/select2/dist/css/select2.min.css')}}" rel="stylesheet" />
    <link href="{{ asset('assets/modules/jquery-ui-1.12.1/jquery-ui.css')}}" rel="stylesheet" />
    {{-- <link href="{{ asset('assets/modules/daterangepicker/daterangepicker.css')}}" rel="stylesheet" /> --}}
    <link rel="stylesheet" href="{{ asset('assets/modules/sweetalert2/dist/sweetalert2.min.css') }}" />
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" /> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/modules/jquery-datetimepicker/jquery.datetimepicker.css') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {{-- Template CSS --}}
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    <link href="{{ asset('assets/css/global.css') }}" rel="stylesheet">
    @stack('custom-css')
</head>
<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>

            @include('layouts.header')

            @include('layouts.sidebar')
            {{-- Main Content --}}
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1>@yield('header')</h1>
                    </div>
                    @yield('content')
                </section>
                @yield('modal')
            </div>
            <footer class="main-footer">
                <div class="footer-left">
                    Copyright &copy; {{ date("Y") }} <div class="bullet"></div> Design By <a href="https://gitlab.com/zaychsual" target="_blank">Fastrata</a>
                </div>
            </footer>
        </div>
    </div>
    @stack('before-script')
    {{--  General JS Scripts --}}
    <script src="{{ asset('assets/javascript/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/javascript/popper.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/javascript/bootstrap.min.js') }}"></script> --}}
    <script src="{{ asset('assets/bootstrap-4.6.0-dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/javascript/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('assets/javascript/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/stisla.js') }}"></script>

    {{-- JS Modules --}}
    <script src="{{asset('assets/modules/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{ asset('assets/modules/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/modules/DataTables/datatables.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/modules/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script> --}}
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{ asset('assets/modules/jquery-ui-1.12.1/jquery-ui.js') }}"></script>
    {{-- <script src="{{ asset('assets/modules/daterangepicker/daterangepicker.js') }}"></script> --}}
    <script src="{{ asset('assets/modules/php-date-formatter/js/php-date-formatter.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    {{-- <script src="{{ asset('assets/modules/jquery-datetimepicker/jquery-datetimepicker.js') }}"></script> --}}
    <script src="{{ asset('assets/modules/jquery-datetimepicker/jquery.datetimepicker.js') }}"></script>
    {{-- Template JS File --}}
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{ asset('assets/js/global.js') }}"></script>
    {{-- Page Specific Custom Javascript --}}
    @stack('custom-js')
</body>
</html>

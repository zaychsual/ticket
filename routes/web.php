<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\TicketController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\WilayahController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\ComplainController;
use App\Http\Controllers\Admin\HelperController;
use App\Http\Controllers\Admin\BranchController;
use App\Http\Controllers\Admin\RegionController;
use App\Http\Controllers\Admin\SendMailController;
use App\Http\Controllers\Admin\ReportController;

Route::get('/', [LoginController::class, 'showLoginForm']);
Route::redirect('/home', '/admin');

Auth::routes();
Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    //Ticket
    Route::resource('/ticket', TicketController::class);
    Route::get('/ticket-ajax', [TicketController::class, 'ticketAjax'])->name('ticket-ajax');
    Route::get('/ticket/option/{id}', [TicketController::class, 'ticketOption'])->name('ticket.option');
    Route::get('/ticket/action/{id}', [TicketController::class, 'action'])->name('ticket.action');
    Route::get('/ticket/issabel/{id}/{agent}/{call}/{campaign}/{callid}/{remote}', [TicketController::class, 'tiketPhone'])->name('ticket.issabel');
    Route::get('ticket/print/{id}', [TicketController::class, 'print'])->name('ticket.print');
    Route::get('ticket/xls/{id}', [TicketController::class, 'xls'])->name('ticket.xls');

    //Report
    Route::get('/report-komplain', [ReportController::class, 'reportKomplain'])->name('report-komplain');
    Route::get('/report-komplain-data', [ReportController::class, 'reportKomplainData'])->name('report-komplain-data');
    Route::get('/report-komplain-export', [ReportController::class, 'reportKomplainExport'])->name('report-komplain-export');
    Route::get('/report-wilayah', [ReportController::class, 'reportWilayah'])->name('report-wilayah');
    Route::get('/report-wilayah-data', [ReportController::class, 'reportWilayahData'])->name('report-wilayah-data');
    Route::get('/report-wilayah-export', [ReportController::class, 'reportWilayahExport'])->name('report-wilayah-export');
    Route::get('/report-kategori', [ReportController::class, 'reportKategori'])->name('report-kategori');
    Route::get('/report-kategori-data', [ReportController::class, 'reportKategoriData'])->name('report-kategori-data');
    Route::get('/report-kategori-export', [ReportController::class, 'reportKategoriExport'])->name('report-kategori-export');

    Route::view('/maintenance', 'maintenance.index')->name('maintenance');

    //Email
    Route::get('/kirim-email', [SendMailController::class, 'send']);

    //Helpers
    Route::get('/cust-details', [HelperController::class, 'custDetail'])->name('cust-details');
    Route::get('/select-cust', [HelperController::class, 'selectCust'])->name('select-cust');

    //Settings
    Route::resource('/cust', CustomerController::class);
    Route::get('/cust-ajax', [CustomerController::class, 'custAjax'])->name('cust-ajax');
    Route::resource('/branch', BranchController::class);
    Route::resource('/region', RegionController::class);
    Route::resource('/role', RoleController::class);
    Route::get('/role/edit-menu-access', [RoleController::class, 'roleMenu'])->name('role.edit-menu-access');
    Route::post('/role/store-menu-access', [RoleController::class, 'postRoleMenu'])->name('role.store-menu-access');
    Route::get('/complain/edit-alternative/{id}', [ComplainController::class, 'editAlterNative'])->name('complain.edit-alternative');
	Route::resource('/complain', ComplainController::class);
    Route::resource('/menu', MenuController::class);
    Route::resource('/user', UserController::class);
    Route::resource('/setting', SettingController::class);

});

// Route::get('/home', [HomeController::class, 'index'])->name('home');

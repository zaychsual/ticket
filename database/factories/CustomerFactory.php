<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;
use Ramsey\Uuid\Uuid as Generator;
use Carbon\Carbon;
class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $dt = $this->faker->dateTimeBetween($startDate = '-13 years', $endDate = '-12 years');
        $dob = $dt->format("Y-m-d"); // 1994-09-24
        
        return [
            'id' => strtoupper(Generator::uuid4()->toString()),
            'no_pelanggan' => $this->faker->unique()->numerify('0100-#######'),
            'nik'   => $this->faker->unique()->numerify('‭################'),
            'realname' => $this->faker->name(),
            'outlet_name' => $this->faker->company(),
            'type_outlet_id' => $this->faker->randomElement([
                '622f6f9f-6cee-42f7-aa83-887fa5c7a4d8','b1f17e20-7e5f-4765-a325-ba18f994d23f','145e4eaa-4198-4a4d-9e4d-e0ddf70dc6a4',
                'ca5fefa3-0063-425e-be08-82995d9bd1ad','fdd444f0-355c-45c7-a5d1-2a8c8c0d3f3b','e25b14fb-78d4-415e-bbfb-bf151e3fb144',
                'c463581d-4fb3-4d48-b55a-45fdc72b516e','13b3c968-6cc4-47b1-a1bf-67949bef94c0','376b1c0d-2544-44e2-afdf-cda2b953f4e3',
                'd66239d5-b9f4-4ed2-82b9-6fed11d59a4d','1d26478d-4801-4215-8065-1194e09c38da','03dd6885-4ec6-4cc0-8202-9db30f1aa423',
                '3bab1cea-1923-499b-b8c4-1472e86aed51','637441bd-fa9a-4a69-8076-0c98031668e2','5a1ca5bb-5d8b-4ad1-a167-48306a4e3dee',
                'a631b26d-c6a2-4f12-ac66-c76e074cd6e5','84f8b7d3-5f38-4675-a1fc-68feaa171f77','cbe69b71-5621-4e88-bf4f-728030374c26',
                'b0f97d3c-a5f8-43f9-ad5f-4206fbe35ddc','ea1f69b4-0f99-40ce-918c-1b7acdffd0b8','9f537942-c3c8-47ec-ac0a-d9c4a97cd64e',
                '32b7da23-2f0d-4916-bdc1-f2fab553f644','7c898ae3-f878-479a-9c43-ccb2c1303fe2','90aeac99-2190-4db8-852e-796d1d9e5e09',
                'f6cbf38b-51d6-44f7-9aed-7650e8fb6938','0fb9cc91-cee3-4ec9-8a38-2caba2c4ed19','c759049e-1f94-4584-9434-606f3deff05e',
                '798b80c7-b951-46f9-ba9f-a2244fef4104','7841669f-9ee3-4657-957f-91c122470a02','ebae9919-4fa9-4696-9ea7-ea4a60662621',
                'b0006a46-b399-437c-b737-610abc77041f','66127ef1-a271-4f8d-8828-247f0ddf2323','192d8f97-1db4-4629-a7dc-73690dd5f18c',
                'd9ce661b-af16-4b55-b68a-3bdf220e2ab2','d8286215-4fda-4a48-b97d-05822b0575e3','96f50344-e8c6-4b49-b89d-f92a731414a2',
                '129b0d8c-84be-44f8-844a-559839d54bac','8325e57d-88fa-4836-8009-74ed65ecc313','e42512f0-d72e-446e-8d49-eb7cf7026021',
                '9b115a5a-a271-4a4e-bf1c-41c0bef77f41','01caddb9-18ed-4df2-b804-ac606bf7dcb6','92a804c3-2a7c-4f41-929e-03e44ca18d1c',
                '3eeae7b7-3d39-4613-92cd-cbf199d4f5af','3e5bae6a-7c39-41c9-8060-42739fb6aace','d9c85937-7ab5-4672-b998-edd5f33cc65c',
                '67717daa-a42e-4ada-adca-4cc0eb3fd630','d5b91292-06dc-457a-a51f-e8d325cb8fa5','1e8f4458-5fd8-43d5-a774-21aca9417802',
                '1c3cba1b-832e-4685-a280-27909bb4f3c7','b6aa7ec6-adca-4e48-96c4-ad2fa4ef8638','dec9796d-3ba2-4632-a5db-efe2d80f960c',
                'c3f6fb3d-ce27-4715-9b90-87a177f26a47','f75b79ca-8d89-4e32-a1d3-3295275ba937','699e5a18-6157-4ab9-90c5-9f1543de5ace',
            ]),
            'phone_no' => $this->faker->numerify('08##########'),
            'alamat' => $this->faker->address(),
            'kode_sales' => $this->faker->numerify('0###-TSBZ##'),
            'salesname' => $this->faker->name(),
            'spv_sales' => $this->faker->name(),
            'created_at' => Carbon::now(),
            'created_by' => '8924c6fa-3065-4541-8609-8bc1b86a1349',
        ];
    }
}

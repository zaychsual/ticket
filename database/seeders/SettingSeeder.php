<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Setting;
use Carbon\Carbon;
class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seed = [
            [
                'name' => 'FRM-CSD-007',
                'prefix' => 'TF/CS',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'FRM-CSD-019',
                'prefix' => 'TF/CS',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Customer',
                'prefix' => 'FAKER-CUST-',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Customer',
                'prefix' => 'FAKER-CUST-',
                'created_at' => Carbon::now(),
            ],
        ];

        Setting::insert($seed);
    }
}

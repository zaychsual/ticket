<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Status;
use Carbon\Carbon;
class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seed = [
            [
                'id' => '293e5a68-1742-47c6-aaf3-bb2873c93a71',
                'name' => 'In Progress',
                'created_at' => Carbon::now(),
            ],
            [
                'id' => '393542f2-796b-46a7-a3c3-6936e6fb0533',
                'name' => 'Done',
                'created_at' => Carbon::now(),
            ]
        ];
        Status::insert($seed);
    }
}

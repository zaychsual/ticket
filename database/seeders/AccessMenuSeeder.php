<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AccessMenu;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;

class AccessMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        AccessMenu::query()->truncate();
        Schema::enableForeignKeyConstraints();

        if(env('DB_CONNECTION') == 'pgsql') {
            DB::statement('ALTER SEQUENCE access_menus_id_seq RESTART WITH 1;');
        } elseif (env('DB_CONNECTION') == 'mysql') {
            DB::statement('ALTER TABLE access_menus AUTO_INCREMENT = 1;');
        }
        $data = [
            ['role_id' =>	'3'	,'menu_id' =>	'0adc7891-2854-4107-a9d0-77529f140bd6'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'140e30c3-b6a3-43c3-b653-292db0441479'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'3bdf758c-dfe4-41fa-8f5a-726f2d9bee26'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'3f3128f3-43ab-49f1-aee2-dca3af000102'	,'akses' =>	1	,'add' =>	0	,'edit' =>	1	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'58f8700f-dad2-4145-b724-6a9b37e994ba'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'5a55b825-0d1f-498c-b51a-a1f118e4b86b'	,'akses' =>	1	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'904cf889-deb7-444e-8d7e-092f1d5daa8a'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'93f19321-7461-477c-b27e-a1606e67ccba'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'9496ee48-f148-4b45-be9d-2751b1f29757'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'a6e40bfb-0eb2-4919-bc64-67d90559cf38'	,'akses' =>	1	,'add' =>	0	,'edit' =>	1	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'b189682c-9094-458f-939b-381cb512ab20'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'bd016a09-6667-40af-b2d8-d12b84cbf6d8'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'ce8764f0-1d42-4085-8d3f-6bf995f121a6'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'3'	,'menu_id' =>	'e59b674e-d007-45c7-b8cc-c3a0473c8ea3'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'0adc7891-2854-4107-a9d0-77529f140bd6'	,'akses' =>	1	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'140e30c3-b6a3-43c3-b653-292db0441479'	,'akses' =>	1	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'3bdf758c-dfe4-41fa-8f5a-726f2d9bee26'	,'akses' =>	1	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'3f3128f3-43ab-49f1-aee2-dca3af000102'	,'akses' =>	1	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'58f8700f-dad2-4145-b724-6a9b37e994ba'	,'akses' =>	1	,'add' =>	1	,'edit' =>	1	,'destroy' =>	1	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'5a55b825-0d1f-498c-b51a-a1f118e4b86b'	,'akses' =>	1	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'904cf889-deb7-444e-8d7e-092f1d5daa8a'	,'akses' =>	1	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'93f19321-7461-477c-b27e-a1606e67ccba'	,'akses' =>	1	,'add' =>	1	,'edit' =>	1	,'destroy' =>	1	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'9496ee48-f148-4b45-be9d-2751b1f29757'	,'akses' =>	1	,'add' =>	1	,'edit' =>	1	,'destroy' =>	1	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'a6e40bfb-0eb2-4919-bc64-67d90559cf38'	,'akses' =>	1	,'add' =>	1	,'edit' =>	1	,'destroy' =>	1	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'b189682c-9094-458f-939b-381cb512ab20'	,'akses' =>	1	,'add' =>	1	,'edit' =>	1	,'destroy' =>	1	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'bd016a09-6667-40af-b2d8-d12b84cbf6d8'	,'akses' =>	1	,'add' =>	1	,'edit' =>	1	,'destroy' =>	1	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'ce8764f0-1d42-4085-8d3f-6bf995f121a6'	,'akses' =>	1	,'add' =>	1	,'edit' =>	1	,'destroy' =>	1	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'e59b674e-d007-45c7-b8cc-c3a0473c8ea3'	,'akses' =>	1	,'add' =>	1	,'edit' =>	1	,'destroy' =>	1	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'1'	,'menu_id' =>	'729e8fc7-c88b-459c-a948-c5f8b8f5a86c'	,'akses' =>	1	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'0adc7891-2854-4107-a9d0-77529f140bd6'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'140e30c3-b6a3-43c3-b653-292db0441479'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'3bdf758c-dfe4-41fa-8f5a-726f2d9bee26'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'3f3128f3-43ab-49f1-aee2-dca3af000102'	,'akses' =>	1	,'add' =>	0	,'edit' =>	1	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'58f8700f-dad2-4145-b724-6a9b37e994ba'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'5a55b825-0d1f-498c-b51a-a1f118e4b86b'	,'akses' =>	1	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'729e8fc7-c88b-459c-a948-c5f8b8f5a86c'	,'akses' =>	1	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'904cf889-deb7-444e-8d7e-092f1d5daa8a'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'93f19321-7461-477c-b27e-a1606e67ccba'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'9496ee48-f148-4b45-be9d-2751b1f29757'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'a6e40bfb-0eb2-4919-bc64-67d90559cf38'	,'akses' =>	1	,'add' =>	1	,'edit' =>	1	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'b189682c-9094-458f-939b-381cb512ab20'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'bd016a09-6667-40af-b2d8-d12b84cbf6d8'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'ce8764f0-1d42-4085-8d3f-6bf995f121a6'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],
            ['role_id' =>	'2'	,'menu_id' =>	'e59b674e-d007-45c7-b8cc-c3a0473c8ea3'	,'akses' =>	0	,'add' =>	0	,'edit' =>	0	,'destroy' =>	0	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'created_at' => Carbon::now(),],

        ];

        AccessMenu::insert($data);
    }
}

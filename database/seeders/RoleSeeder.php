<?php

namespace Database\Seeders;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'Admin',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Agent',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Branch',
                'created_at' => Carbon::now(),
            ],
        ];
        Role::insert($roles);
    }
}

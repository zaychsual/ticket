<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\VariableComplain;
use Ramsey\Uuid\Uuid as Generator;
use Carbon\Carbon;

class VariableComplainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VariableComplain::query()->truncate();
        $data = [
            ['id' =>	'172A563D-81EF-489A-A6BB-60E240B69CCE'	,'name' =>	'Retur'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	'237116FD-CBBB-4FC1-B974-294F58C9B680'	,'name' =>	'New Cust'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	1	,'created_at' => Carbon::now(),],
            ['id' =>	'2AE5CD98-960F-4AEB-9371-5F5B257B2AD3'	,'name' =>	'Driver/Sales'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	'56C0DAA1-41E0-4448-8121-B72FD8185CA5'	,'name' =>	'Payment'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	'7C234445-B755-43F1-92A7-BD157E4D71D9'	,'name' =>	'Pek'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	'9953669F-0C40-4003-9BEF-BC66E726974E'	,'name' =>	'Delivery Shortage'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	'9AD772EE-539A-4315-A793-C870C3A98F3E'	,'name' =>	'Invoice'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	'AC1BBC79-2CA7-4573-9E04-A1EF449B41D4'	,'name' =>	'Packaging'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	'B54A0D92-ED41-42E8-A433-EDBBF0F3958E'	,'name' =>	'Hologram'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	'B8640BF0-2A80-41A6-8DB9-F2220670C3EE'	,'name' =>	'Delivery'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	'B8CE8125-4720-412B-9742-E42301F0EEF4'	,'name' =>	'Credit Unit'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	'C3FC3EC0-1039-4551-A93F-AE56CF03035B'	,'name' =>	'Order'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	'E39F9453-18D0-4CF3-923E-09BB2B4297D4'	,'name' =>	'Lainnya'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	0	,'created_at' => Carbon::now(),],
            ['id' =>	'E4335440-1E1A-4C23-8902-770143A224F1'	,'name' =>	'Disc. Information'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	'F9B1DB69-B679-4F10-AF6F-CB39A7E65FE3'	,'name' =>	'Gift'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	2	,'created_at' => Carbon::now(),],
            ['id' =>	strtoupper(Generator::uuid4()->toString())	,'name' =>	'Saran'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	1	,'created_at' => Carbon::now(),],
            ['id' =>	strtoupper(Generator::uuid4()->toString())	,'name' =>	'Konfirmasi Pengiriman'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	1	,'created_at' => Carbon::now(),],
            ['id' =>	strtoupper(Generator::uuid4()->toString())	,'name' =>	'Order & Internal'	,'created_by' =>	'a28d1b62-c9fc-4915-93af-a98fe0cfd166'	,'is_item' =>	0	,'category' =>	1	,'created_at' => Carbon::now(),],
        ];
        VariableComplain::insert($data);
    }
}

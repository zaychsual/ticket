<?php

namespace Database\Seeders;
use App\Models\Region;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [	'id' => '3648f243-a9ef-4e51-b828-423ac52d3b9e',	'name' =>	'JATENG UTARA',	'created_at' => Carbon::now(),	],
            [	'id' => '082555c7-b3bd-45ec-b36a-d358c4a1f6d1',	'name' =>	'BMPP',	'created_at' => Carbon::now(),	],
            [	'id' => 'a1dea8f5-9bbe-4f34-89d3-7b9be1ce51ae',	'name' =>	'JABAR 2',	'created_at' => Carbon::now(),	],
            [	'id' => 'fc1edf19-73ed-46c7-8422-7e024d0f9d32',	'name' =>	'JABAR 3',	'created_at' => Carbon::now(),	],
            [	'id' => '48aea57f-7c7f-43e0-9476-641a94ce573f',	'name' =>	'SUMATERA 1',	'created_at' => Carbon::now(),	],
            [	'id' => '1fddadbc-2286-49d9-a2c2-612a3a3fb326',	'name' =>	'JATENG SELATAN',	'created_at' => Carbon::now(),	],
            [	'id' => '4d166aec-3b8b-488b-84d5-f12115874488',	'name' =>	'JABOTABEK 1',	'created_at' => Carbon::now(),	],
            [	'id' => '8aa4c29a-1798-4b97-a172-af32c9940325',	'name' =>	'JATIM SELATAN',	'created_at' => Carbon::now(),	],
            [	'id' => '70174d2d-fc78-4aae-9c67-ba7cf8112988',	'name' =>	'MM TIMUR',	'created_at' => Carbon::now(),	],
            [	'id' => '3c43ed16-6663-4f99-bbef-4c18456e7af2',	'name' =>	'SUMATERA 2',	'created_at' => Carbon::now(),	],
            [	'id' => '617561ad-4391-4f7a-85ec-661f395c9791',	'name' =>	'JABAR 1',	'created_at' => Carbon::now(),	],
            [	'id' => '20eaafbc-e106-4184-b125-dceeb477a8e5',	'name' =>	'MM BARAT',	'created_at' => Carbon::now(),	],
            [	'id' => '6248a14b-028f-4a42-9967-bac71c815037',	'name' =>	'JABOTABEK 2',	'created_at' => Carbon::now(),	],
            [	'id' => 'e044669e-4d91-44c4-8d22-53481c71cf9a',	'name' =>	'MM LUAR PULAU',	'created_at' => Carbon::now(),	],
            [	'id' => '283dee0a-9a75-49f7-9fd9-d426be2d3d20',	'name' =>	'JATIM UTARA',	'created_at' => Carbon::now(),	],
            [	'id' => 'ebe3e189-8fbe-4f26-871e-24f7211d22dc',	'name' =>	'JABOTABEK 3',	'created_at' => Carbon::now(),	],

        ];
        Region::insert($data);
    }
}

<?php

namespace Database\Seeders;
use App\Models\User;
use Ramsey\Uuid\Uuid as Generator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166',
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'username' => strtolower('admin'),
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('password'),
                'remember_token' => Str::random(10),
                'role_id' => 1,
                'updated_by' => null,
                'created_at' => Carbon::now(),
                'deleted_at' => null,

            ],
            [
                'id' => '8924c6fa-3065-4541-8609-8bc1b86a1349',
                'name' => 'faker',
                'email' => 'faker@faker.com',
                'username' => strtolower('faker'),
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('password'),
                'remember_token' => Str::random(10),
                'role_id' => 1,
                'updated_by' => 'a28d1b62-c9fc-4915-93af-a98fe0cfd166',
                'created_at' => Carbon::now(),
                'deleted_at' => Carbon::now(),
            ],
            [
                'id' => strtoupper(Generator::uuid4()->toString()),
                'name' => 'Agent 01',
                'email' => 'agent@agent.com',
                'username' => strtolower('agent01'),
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('password'),
                'remember_token' => Str::random(10),
                'role_id' => 2,
                'updated_by' => null,
                'created_at' => Carbon::now(),
                'deleted_at' => null,
            ],
            [
                'id' => strtoupper(Generator::uuid4()->toString()),
                'name' => 'Jullya',
                'email' => 'jully.tauran@fastratabuana.co.id',
                'username' => strtolower('jullya'),
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('password'),
                'remember_token' => Str::random(10),
                'role_id' => 2,
                'updated_by' => null,
                'created_at' => Carbon::now(),
                'deleted_at' => null,
            ],
            [
                'id' => strtoupper(Generator::uuid4()->toString()),
                'name' => 'Novita',
                'email' => 'novita.kusuma@fastratabuana.co.id ',
                'username' => strtolower('novita'),
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('password'),
                'remember_token' => Str::random(10),
                'role_id' => 2,
                'updated_by' => null,
                'created_at' => Carbon::now(),
                'deleted_at' => null,
            ],
            [
                'id' => strtoupper(Generator::uuid4()->toString()),
                'name' => 'Cabang',
                'email' => 'branch@branch.com',
                'username' => strtolower('branch01'),
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('password'),
                'remember_token' => Str::random(10),
                'role_id' => 3,
                'updated_by' => null,
                'created_at' => Carbon::now(),
                'deleted_at' => null,
            ]
        ];
        User::insert($users);
    }
}

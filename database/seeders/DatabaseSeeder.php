<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Customer;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\Customer::factory(1000)->create();
        //Customer::factory(150)->create(); //dummy dont seed on production

        $this->call(IndoRegionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(SettingSeeder::class);
        // $this->call(RegionSeeder::class);
        // $this->call(BranchSeeder::class);
        $this->call(TypeOutletSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(AccessMenuSeeder::class);
        $this->call(VariableComplainSeeder::class);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnVariableComplain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('variable_complains', 'is_item'))
        {
            Schema::table('variable_complains', function (Blueprint $table)
            {
                $table->tinyInteger('is_item')->default(0);
            });
        }
        if (!Schema::hasColumn('variable_complains', 'category'))
        {
            Schema::table('variable_complains', function (Blueprint $table)
            {
                $table->string('category', 100)->nullable();
            });
        }
        Schema::table('tickets', function (Blueprint $table) {
            $table->uuid('customer_id')->nullable()->change();
        });
        Schema::table('ticket_logs', function (Blueprint $table) {
            $table->uuid('customer_id')->nullable()->change();
        });
        if (!Schema::hasColumn('ticket_logs', 'realname'))
        {
            Schema::table('ticket_logs', function (Blueprint $table)
            {
                $table->string('realname', 100)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('variable_complains', 'is_item'))
        {
            Schema::table('variable_complains', function (Blueprint $table)
            {
                $table->dropColumn('is_item');
            });
        }
        if (Schema::hasColumn('variable_complains', 'category'))
        {
            Schema::table('variable_complains', function (Blueprint $table)
            {
                $table->dropColumn('category');
            });
        }
        if (Schema::hasColumn('tickets', 'customer_id'))
        {
            Schema::table('tickets', function (Blueprint $table)
            {
                $table->uuid('customer_id')->change();
            });
        }
        if (Schema::hasColumn('ticket_logs', 'customer_id'))
        {
            Schema::table('ticket_logs', function (Blueprint $table)
            {
                $table->uuid('customer_id')->change();
            });
        }
        if (Schema::hasColumn('ticket_logs', 'realname'))
        {
            Schema::table('ticket_logs', function (Blueprint $table)
            {
                $table->dropColumn('realname');
            });
        }
    }
}

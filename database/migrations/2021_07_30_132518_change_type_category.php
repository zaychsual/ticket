<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTypeCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('variable_complains', function (Blueprint $table) {
            $table->dropColumn('category');
        });
        Schema::table('variable_complains', function (Blueprint $table) {
            $table->tinyInteger('category')->default(0);
        });
        if (!Schema::hasColumn('ticket_logs', 'realname'))
        {
            Schema::table('ticket_logs', function (Blueprint $table)
            {
                $table->string('realname', 100)->nullable()->change();
            });
        } else {
            Schema::table('ticket_logs', function (Blueprint $table)
            {
                $table->dropColumn('realname');
            });
            Schema::table('ticket_logs', function (Blueprint $table)
            {
                $table->string('realname', 100)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variable_complains', function (Blueprint $table) {
            $table->dropColumn('category');
        });
    }
}

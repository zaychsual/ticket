<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketPhoneLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_phone_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('ticket_id');
            $table->string('phone_no', 100);
            $table->string('agent_number', 100)->nullable();
            $table->string('call_type', 100)->nullable();
            $table->string('campaign_id', 100)->nullable();
            $table->string('call_id', 100)->nullable();
            $table->string('remote_channel', 100)->nullable();
            $table->string('created_by', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_phone_logs');
    }
}

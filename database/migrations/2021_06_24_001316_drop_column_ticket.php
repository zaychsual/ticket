<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('no_ticket');
        });
        Schema::table('ticket_logs', function (Blueprint $table) {
            $table->dropColumn('no_ticket');
            $table->dropColumn('complain_category_id');
            $table->uuid('complain_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('tickets', function (Blueprint $table) {
        //     $table->string('no_ticket', 100);
        // });
        // Schema::table('ticket_logs', function (Blueprint $table) {
        //     $table->string('no_ticket', 100);
        //     $table->uuid('complain_category_id');
        // });
    }
}

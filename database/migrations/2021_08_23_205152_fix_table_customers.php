<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixTableCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('branch_id');
            $table->string('branch_code', 100)->nullable();
        });
        Schema::table('ticket_customers', function (Blueprint $table) {
            $table->dropColumn('branch_id');
            $table->string('branch_code', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->uuid('branch_id')->nullable();
            $table->dropColumn('branch_code');
        });
        Schema::table('ticket_customers', function (Blueprint $table) {
            $table->uuid('branch_id')->nullable();
            $table->dropColumn('branch_code');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_temps', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('no_pelanggan', 100);
            $table->string('nik', 100)->nullable();
            $table->string('realname', 100)->nullable();
            $table->string('outlet_name', 100)->nullable();
            $table->uuid('type_outlet_id')->nullable();
            $table->string('phone_no', 100)->nullable();
            $table->string('kelurahan_id', 100)->nullable();
            $table->string('kecamatan_id', 100)->nullable();
            $table->string('kabupaten_id', 100)->nullable();
            $table->string('provinsi_id', 100)->nullable();
            $table->text('alamat')->nullable();
            $table->string('kode_sales', 100)->nullable();
            $table->string('salesname', 100)->nullable();
            $table->string('spv_sales', 100)->nullable();
            $table->uuid('created_by')->default('a28d1b62-c9fc-4915-93af-a98fe0cfd166');
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_temps');
    }
}

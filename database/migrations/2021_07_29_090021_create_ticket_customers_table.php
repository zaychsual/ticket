<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_customers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('ticket_id');
            $table->uuid('branch_id');
            $table->string('no_pelanggan', 100)->nullable();
            $table->string('outlet_name', 100);
            $table->string('realname', 100);
            $table->text('alamat');
            $table->string('phone_no', 100);
            $table->string('salesname', 100)->nullable();
            $table->string('spv_sales', 100)->nullable();
            $table->uuid('type_outlet_id')->nullable();
            $table->uuid('created_by')->default('a28d1b62-c9fc-4915-93af-a98fe0cfd166');
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_customers');
    }
}

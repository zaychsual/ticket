<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnTableTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->renameColumn('code', 'no_ref');
            $table->uuid('branch_id')->after('nama_sales')->nullable();
            $table->time('jam_komplain')->after('nama_sales')->default('00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->renameColumn('no_ref', 'code');
            $table->dropColumn('branch_id');
            $table->dropColumn('jam_komplain');
        });
    }
}

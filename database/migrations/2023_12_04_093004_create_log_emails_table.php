<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_emails', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('email', 100);
            $table->text('email_cc', 100);
            $table->string('subject', 200);
            $table->string('title', 200);
            $table->text('content');
            $table->string('branch', 200);
            $table->enum('status', ['Pending', 'Failed', 'Success'])->default('Pending');
            $table->text('response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_emails');
    }
}

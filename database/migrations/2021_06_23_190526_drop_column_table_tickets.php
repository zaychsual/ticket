<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnTableTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('nama_pemilik_outlet');
            $table->dropColumn('nama_sales');
            $table->dropColumn('nama_spv_sales');
        });
        if (Schema::hasColumn('tickets', 'ticket_category'))
        {
            Schema::table('tickets', function (Blueprint $table)
            {
                $table->dropColumn('ticket_category');
            });
        }
        if (Schema::hasColumn('tickets', 'main_status_id'))
        {
            Schema::table('tickets', function (Blueprint $table)
            {
                $table->dropColumn('main_status_id');
            });
        }
        Schema::table('tickets', function (Blueprint $table) {
            $table->string('ticket_category', 100);
            $table->integer('main_status_id')->default(300);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->string('nama_pemilik_outlet', 100)->nullable();
            $table->string('nama_sales', 100)->nullable();
            $table->string('nama_spv_sales', 100)->nullable();
        });
        if (Schema::hasColumn('tickets', 'main_status_id'))
        {
            Schema::table('tickets', function (Blueprint $table)
            {
                $table->dropColumn('main_status_id');
            });
        }
        if (Schema::hasColumn('tickets', 'ticket_category'))
        {
            Schema::table('tickets', function (Blueprint $table)
            {
                $table->dropColumn('ticket_category');
            });
        }
    }
}

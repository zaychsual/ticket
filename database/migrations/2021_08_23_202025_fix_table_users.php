<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('branch_id');
            $table->string('email_bom', 100)->nullable();
            $table->string('email_admin', 100)->nullable();
            $table->string('branch_code', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->uuid('branch_id')->nullable();
            $table->dropColumn('email_bom');
            $table->dropColumn('email_admin');
            $table->dropColumn('branch_code');
        });
    }
}

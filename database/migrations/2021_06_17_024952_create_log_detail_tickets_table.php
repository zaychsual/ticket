<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogDetailTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_detail_tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('ticket_id');
            $table->uuid('detail_ticket_id');
            $table->text('description');
            $table->uuid('status')->default('293e5a68-1742-47c6-aaf3-bb2873c93a71');
            $table->uuid('created_by')->default('a28d1b62-c9fc-4915-93af-a98fe0cfd166');
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_detail_tickets');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTicketAction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_actions', function (Blueprint $table) {
            $table->renameColumn('action', 'cs_action');
            $table->renameColumn('sub_status', 'cs_status');
            $table->text('bm_action')->nullable();
            $table->integer('bm_status')->default(102);
        });
        Schema::table('tickets', function (Blueprint $table) {
            $table->integer('is_submit')->default(102);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_actions', function (Blueprint $table) {
            $table->renameColumn('cs_action', 'action');
            $table->renameColumn('cs_status', 'sub_status');
            $table->dropColumn('bm_action');
            $table->dropColumn('bm_status');
        });
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('is_submit');
        });
    }
}

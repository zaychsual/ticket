<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('code', 100)->nullable();
            $table->string('no_ticket', 100);
            $table->integer('ticket_category');
            $table->string('nama_penelpon', 100);
            $table->string('no_telp', 100);
            $table->uuid('customer_id');
            $table->string('nama_pemilik_outlet', 100)->nullable();
            $table->string('nama_sales', 100)->nullable();
            $table->string('nama_spv_sales', 100)->nullable();
            $table->text('description')->nullable();
            $table->uuid('complain_id');
            $table->uuid('main_status_id')->default('293e5a68-1742-47c6-aaf3-bb2873c93a71');
            $table->uuid('created_by');
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}

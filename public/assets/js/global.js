"use strict";
//========= start class ==============//
const PARTIAL = 3
const UNPAID = 2
const PAID   = 1

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})

$('.spin-saves').hide()
$("#paid-image").hide()
$('.loading-data').hide()
$('#index-table').DataTable({
    dom: 'Bfrtip',
    buttons: [
        'copy', 'excel', 'pdf', 'print'
    ]
});
$('#scroll_ver').DataTable({
    "scrollY": "300px",
    "scrollCollapse": true,
    "paging": false
});
$('#scroll-table').DataTable( {
    "scrollY":        "200px",
    "scrollCollapse": true,
    "paging":         false,
    "searching"    : false
} );
$('#order_table').on('length.dt', function (e, settings, len) {
    $('[data-toggle="tooltip"]').tooltip('hide');
});
$('.dt-button, .buttons-copy, .buttons-print, .buttons-pdf, .buttons-excel, .paginate_button').addClass('btn-primary');
$(".spin-save").hide()
$('.spin-filter').hide()
$('#save').on('click', function () {
    $("form").submit(function (e) {
        $(this).submit(function () {
            return false;
        });
        $(".spin-save").show()
        $('#save')
            .attr('disabled', 'disabled');

        return true;
    });
});

$('.add-index').click(function(e){
    e.preventDefault();
    $('#modal-store').find("input[type=text], textarea").val("");
    $('#createModal').modal('show');
});

//del-index
$(".del-index").click(function(e) {
    let id = $(this).data("id");
    let basename = $(this).data("name");
    // basename = e.target.dataset.name;
    swal({
        title: 'Are you sure want to delete this record '+basename,
        text: 'You will not be able to recover this record anymore!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $(`#delete_${id}`).submit();
        }
    });
});

$('.select-all').click(function () {
    let $select2 = $(this).parent().siblings('.select2')
    $select2.find('option').prop('selected', 'selected')
    $select2.trigger('change')
})

$('.deselect-all').click(function () {
    let $select2 = $(this).parent().siblings('.select2')
    $select2.find('option').prop('selected', '')
    $select2.trigger('change')
});

$(".select2").select2({
    dropdownParent: $("#createModal, #updateModal"),
    width: '100%'
});

$('#basic-select').select2();
$("#select2-with-placeholder").select2({
    placeholder: "Select a state",
    allowClear: true
});

// jQuery('#datepicker-autoclose').datepicker({
//     autoclose: true,
//     todayHighlight: true
// });

var currentYear = moment().year(); // This Year
var currentYearStart = moment({
    years: currentYear,
    // date: '1'
  }); // 1st Jan this year

var currentYearEnd = moment({
    years: currentYear,
    // date: '31'
});

var dateRange = {};
    dateRange['Hari ini']= [moment(), moment()];
    dateRange['Kemarin']= [moment().subtract(1, 'days'), moment().subtract(1, 'days')];
    dateRange['7 Hari yang lalu']= [moment().subtract(6, 'days'), moment()];
    dateRange['30 Hari yang lalu']= [moment().subtract(29, 'days'), moment()];
    dateRange['Bulan Ini']= [moment().startOf('month'), moment().endOf('month')];
    dateRange['Bulan Lalu']= [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')];
    // dateRange["Tahun " + (currentYear - 1)] = [moment(currentYearStart.subtract(1, "year")), moment(currentYearEnd.subtract(1, "year"))];
// $('.dateranges').daterangepicker({
//     ranges:dateRange,
//     "format": "DD/MM/YYYY",
//     locale: {
//         "format": "DD/MM/YYYY",
//         applyLabel: 'Filter',
//         fromLabel: 'Dari',
//         toLabel: 'Ke',
//         customRangeLabel: 'Rentang Waktu',
//         daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
//         monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
//         firstDay: 1
//     },
// });

// $('.mdateStart').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
// $('.mdateEnd').bootstrapMaterialDatePicker({ weekStart: 0, time: false });

$('.select2-icon').select2({
    templateSelection: formatText,
    templateResult: formatText
});

$(".modal").on("hidden.bs.modal", function() {
    $("#display-errors").html("")
});

$('.datepicker').datetimepicker(
        {timepicker:false,
        format:'Y-m-d',
        formatDate:'Y-m-d',}
    );

$(".timepicker").datetimepicker({
    datepicker:false,
    format:'H:i',
    step:5
});


/**
 *
 * @param {*} value
 * @param {*} element
 * @param {*} decimal
 * @returns
 */
window.leadingZero = function(value, element, decimal = false) {
    var convert_number = removeChar(value);
    if (convert_number) {
        element.val(parseInt(convert_number).toLocaleString('id-ID'))
        return
    }
    if (decimal) {
        if (value != '') {
            element.val(keyupFormatUangWithDecimal(value));
        } else {
            element.val(0);
        }
    } else {
        if (value != '') {
            element.val(keyupFormatUang(parseInt(convert_number)));
        } else {
            element.val(0);
        }
    }
}

/**
 *
 * @param {*} value
 * @returns
 */
function removeChar(value) {
    return value.toString().replace(/[.*+?^${}()|[\]\\]/g, '');
}

/**
 *
 * @param {*} value
 * @returns
 */
window.keyupFormatUang = function(value) {
    var number = '';
    var value_rev = value.toString().split('').reverse().join('');

    for (var i = 0; i < value_rev.length; i++) {
        if (i % 3 == 0) number += value_rev.substr(i, 3) + '.';
    }

    return number.split('', number.length - 1).reverse().join('');
}

/**
 *
 * @param {*} value
 * @returns
 */
window.keyupFormatUangWithDecimal = function(value) {
    return value.replace(/^0+/, '').replace(/(?!\.)\D/g, "").replace(/(?<=\..*)\./g, "").replace(/(?<=\.\d\d).*/g,
        "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/**
 *
 * @param {*} msg
 */
function printErrorMsg (msg) {
    $.each( msg, function( key, value ) {
        $('.'+key+'_err').text(value);
    });
}

/**
 *
 * @param {*} msg
 */
function printErrorMsgBootstrap(msg) {
    $('#display-errors').html('')
    $.each(msg, function(key , value) {
        $('#display-errors').append('<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"role="alert">'+
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                    '<span aria-hidden="true">&times;</span>'+
                '</button>'+
                '<strong>Error - </strong>'+value+
            '</div>')
    })
}

/**
 *
 * @param {*} formReset
 */
function closeModal(modal,formReset) {
    $(modal).on('hidden.bs.modal', function (e) {
        alert()
        $(".select2").val([]).trigger("change");
    $(this)
        .find("input,textarea,select,.select2")
            .val('')
            .end()
        .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })

    // $(formReset).reset()
}

/**
 *
 */
function refreshButtonDtables()
{
    $('.dt-button, .buttons-copy, .buttons-print, .buttons-pdf, .buttons-excel').addClass('.btn-custom-header')
}

/**
 *
 * @param {*} url
 */
function popUpDelete(url)
{
    Swal.fire({
        title: 'Apakah anda yakin ?',
        text: "Data yang sudah dihapus tidak dapat dikembalikan",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: url,
                type: 'DELETE',
                dataType: 'json',
                data: {method: '_DELETE', submit: true}
            }).always(function (data) {
                toastr.success(
                    data.message,
                    'Success !!!',
                    { "progressBar": true }
                );
                $('#order_table').DataTable().draw(false);
            });
        }
    })
}

/**
 *
 * @param {*} msg
 */
function showAlertError(msg)
{
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: msg,
    })
}

/**
 *
 * @param {*} str
 * @returns
 */
function formatToNumber(str) {
    return ~~(str.replaceAll(',', '').replaceAll('.', ''))
}

/**
 *
 * @param {*} txb
 */
function numericFilter(txb) {
    txb.value = txb.value.replace(/[^\0-9]/ig, "");
}

/**
 *
 * @param {*} icon
 * @returns
 */
function formatText (icon) {
    return $('<span><i class="fas ' + $(icon.element).data('icon') + '"></i> ' + icon.text + '</span>');
};

/**
 *
 * @param {*} timeout
 */
function blockUiPage(timeout)
{
    $.blockUI({
        message: '<i class="fas fa-spin fa-sync text-white"></i>',
        timeout: timeout, //unblock after 2 seconds
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.5,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'transparent'
        }
    });
}

/**
 *
 * @param {*} msg
 * @param {*} timeout
 */
function toastrSlide(msg,timeout)
{
    toastr.success(
        msg,
         'Success !!!',
         { "showMethod": "slideDown", "hideMethod": "slideUp",
        timeOut: timeout
    });
}

/**
 *
 * @param {*} formId
 * @param {*} type
 */
function resetForm(formId,type)
{
    if( type == 'edit' ) {
        var validator = $("#"+formId).validate();
        // validator.resetForm();
        $('.has-error').removeClass('has-error')
        $('.has-success').removeClass('has-success')
        $('.invalid').removeClass('invalid')
        $("em").remove()
    } else {
        var validator = $("#"+formId).validate();
            validator.resetForm();
            $("#"+formId)[0].reset()
        $('.has-error').removeClass('has-error')
        $('.has-success').removeClass('has-success')
    }
}

//validate same value
//ref : https://stackoverflow.com/questions/39365663/prevent-duplicate-choice-in-multiple-dropdowns
function disabledSameValue(i)
{
    $('#product_id_' + i).change(function () {
        if ($('select option[value="' + $(this).val() + '"]:selected').length > 1) {
            $(this).val('-1').change();
            showAlertError('Anda sudah memilih produk ini sebelumnya !!! silahkan pilih produk lain')
        }
    });
}

function touchSpin(param)
{
    $("input[name='"+param+"']").TouchSpin();
}

/**
 * format date
 * @param {*} date
 * @param {*} times
 * @returns
 */
function formatDateFromString(date,times = false)
{
    var date = new Date(date);
    if( times ) {
        return ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '-' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '-' + date.getFullYear()
            +' '+ date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    } else {
        return ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '-' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '-' + date.getFullYear()
    }
}

function initTableScroll(tableId)
{
    $('#'+tableId).DataTable( {
        "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,
        "searching"    : false,
        "bInfo" : false,
        // destroy: true,
    } );
}

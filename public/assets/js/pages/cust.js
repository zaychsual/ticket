const base_url = window.location.origin;

$(document).ready(function()
{
    // var indexTable;
    // indexTable =$('#ajax-table').DataTable({
    //     processing: true,
    //     serverside: true,
    //     ajax: {
    //         url: base_url + '/admin/cust-ajax',
    //     },
    //     // searchDelay: 750,
    //     pageLength: 10,
    //     dom: 'Bftrip',
    //     columns: [
    //         {
    //             data: 'no_pelanggan',
    //             name: 'no_pelanggan'
    //         },
    //         {
    //             data: 'realname',
    //             name: 'realname'
    //         },
    //         {
    //             data: 'outlet_name',
    //             name: 'outlet_name'
    //         },
    //         {
    //             data: 'phoneno',
    //             name: 'phoneno'
    //         },
    //         {
    //             data: 'createdby',
    //             name: 'createdby'
    //         },
    //         {
    //             data: 'updatedby',
    //             name: 'updatedby'
    //         },
    //         {
    //             data: 'ID',
    //             name: 'ID',
    //             orderable: false,
    //             searchable: false
    //         },
    //     ],
    // });

    // $('.dt-button, .buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn-primary');

    $('#add-index').on('click', function(e) {
        e.preventDefault();
        $('#modal-store').find("input[type=text], textarea").val("");
        $.ajax({
            url: base_url + `/admin/cust/create`,
            method: "GET",
            success: function(result) {
                $('#createModal').find('.modal-body').html(result);
                $('#createModal').find(".select2").select2({
                    dropdownParent: $('#createModal')
                });
                $('#createModal').modal('show');
            }
        });
    });

    $('#btn-save').on('click', function(e) {
        let formData = $("#modal-store").serialize();
        $('#name-error').html("");
        $('#outlet_name-error').html("");
        $('#phone_no-error').html("");
        $('#alamat-error').html("");
        // console.log(formData)
        $.ajax({
            url: base_url+'/admin/cust',
            method: "POST",
            data: formData,
            success: function(result) {
                // console.log(result)
                if(result.errors) {
                    if(result.errors.name) {
                        $('#name-error').html(data.errors.name[0]);
                    }
                    if(result.errors.outlet_name) {
                        $('#outlet_name-error').html(data.errors.outlet_name[0]);
                    }
                    if(result.errors.phone_no) {
                        $('#phone_no-error').html(data.errors.phone_no[0]);
                    }
                    if(result.errors.alamat) {
                        $('#alamat-error').html(data.errors.alamat[0]);
                    }
                } else {
                    $('#createModal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Successfully Saved'
                    });
                    indexTable.ajax.reload()
                }
            }
        });
    });

    $('#ajax-table').on('click', '.edit-index', function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        $('#modal-update').find("input[type=text], textarea").val("");
        $.ajax({
            url: base_url + `/admin/cust/${id}/edit`,
            method: "GET",
            success: function(data) {
                $('#updateModal').find('#modal-update').attr("action", base_url + "/admin/cust/"+id);
                $('#modal-update').find("input[type=text], textarea").val("");
                $('#updateModal').find('.modal-body').html(data);
                $('#updateModal').modal('show')
            },
            error: function(error){
                console.log(error)
            }
        });
    });

    $('#btn-update').on('click', function(e) {
        let id = $('#modal-update').find('#id').val();
        let formData = $('#modal-update').serialize();
        $('#name-error').html("");
        $('#outlet_name-error').html("");
        $('#phone_no-error').html("");
        $('#alamat-error').html("");
        $.ajax({
            url: base_url + '/admin/cust/'+id,
            method: "PUT",
            data: formData,
            success: function(result) {
                // console.log(result)
                if(result.errors) {
                    if(result.errors.name) {
                        $('#name-error').html(data.errors.name[0]);
                    }
                    if(result.errors.outlet_name) {
                        $('#outlet_name-error').html(data.errors.outlet_name[0]);
                    }
                    if(result.errors.phone_no) {
                        $('#phone_no-error').html(data.errors.phone_no[0]);
                    }
                    if(result.errors.alamat) {
                        $('#alamat-error').html(data.errors.alamat[0]);
                    }
                } else {
                    $('#updateModal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Updated Successfully'
                    });
                    indexTable.ajax.reload()
                }
            }
        });
    });

    $('#ajax-table').on('click', '.show-index', function (e) {
        e.preventDefault();
        let id = $(this).data('id');

        $.ajax({
            url: base_url + `/admin/cust/${id}`,
            method: "GET",
            success: function(data) {
                $('#showModal').find('.modal-body').html(data);
                $('#showModal').modal('show')
            },
            error: function(error){
                console.log(error)
            }
        });
    });
});

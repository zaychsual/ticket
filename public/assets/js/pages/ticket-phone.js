const base_url = window.location.origin;
let index = 0;
$(document).ready(function()
{
    $('#action_store').on('focus',".datepicker", function(){
        $(this).datepicker({
            timepicker:false,
            locale: 'es',
            dateFormat:'yy-mm-d',
            // formatDate:'YYYY-MM-DD',
        });
    });

    $('#form_ticket').find("#customer_id").select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'masukkan nama / nomor pelanggan',
        ajax: {
            dataType: 'json',
            url: base_url + `/admin/select-cust`,
            delay: 250,
            data: function(params) {
                return {
                    search: params.term
                }
            },
            processResults: function (response) {
                return {
                    results:  $.map(response, function (item) {
                        return {
                            text: item.no_pelanggan+" - "+item.realname,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    $('.btn-save').on('click', function(e) {
        let formData = $("#form_ticket").serialize();
        $('#name-error').html("");
        $('#outlet_name-error').html("");
        $('#phone_no-error').html("");
        $('#alamat-error').html("");
        // console.log(formData)

        $.ajax({
            url: base_url + `/admin/ticket`,
            method: "POST",
            data: formData,
            success: function(result) {
                // console.log(result)
                if(result.errors) {
                    if(result.errors.name) {
                        $('#name-error').html(data.errors.name[0]);
                    }
                    if(result.errors.outlet_name) {
                        $('#outlet_name-error').html(data.errors.outlet_name[0]);
                    }
                    if(result.errors.phone_no) {
                        $('#phone_no-error').html(data.errors.phone_no[0]);
                    }
                    if(result.errors.alamat) {
                        $('#alamat-error').html(data.errors.alamat[0]);
                    }
                } else {
                    Toast.fire({
                        icon: 'success',
                        title: 'Successfully Updated',
                        timer: 2000
                    });
                    window.location.replace(base_url + "/admin/ticket/");
                }
            }
        });
    });

    $('#cust_non_complain').on('change', '#branch_id', function(e) {
        let val = $(this).find(':selected').attr('data-id');
        $('#cust_non_complain').find('#branch_code').val(val);
    });

    $("#ticket_category").on('change', function(e) {
        e.preventDefault();
        var id = $("#ticket_category").val();
        let phone = $("#no_telp").val();

        if(id == 'FRM-CSD-007') {
            $("#cust_non_complain").hide();
            $('#cust_non_complain').find("input[type=text], textarea, select").val("");
            $('#cust_non_complain').find("input[type=text], textarea, select").prop("disabled", true);
            $('#cust_complain').find("input[type=text], textarea, select").prop("disabled", false);
            $("#cust_complain").show();
            $("#vb_noncomplain").hide();
            $("#vb_complain").show();
            $("#vb_noncomplain").find("input[type=text], textarea, select").prop("disabled", true);
            $("#vb_complain").find("input[type=text], textarea, select").prop("disabled", false);
        } else {
            $("#cust_non_complain").show();
            $("#cust_complain").hide();
            $('#cust_complain').find("input[type=text], textarea, select").val("");
            $('#cust_complain').find("input[type=text], textarea, select").prop("disabled", true);
            $('#cust_non_complain').find("input[type=text], textarea, select").prop("disabled", false);
            $("#cust_non_complain").find("#phone_no").val(phone);
            $("#vb_noncomplain").show();
            $("#vb_noncomplain").find("input[type=text], textarea, select").prop("disabled", false);
            $("#vb_complain").hide();
            $("#vb_complain").find("input[type=text], textarea, select").prop("disabled", true);
        }
    });

    $('#customer_id').on('change', function(e) {
        e.preventDefault()
        let val = this.value;
        /* if(!$(e.target).closest('#createModal').length && !$(e.target).is('#createModal')) {
            $(".modalDialog").hide();
        } */
        $.ajax({
            url: base_url + `/admin/cust-details?val=${val}`,
            method: "GET",
            success: function(result) {
                console.log(result);
                $('#customer_code').val(result.no_pelanggan);
                $('#phone_no').val(result.phone_no);
                $('#branch').val(result.code+"-"+result.branch);
                $('#branch_id').val(result.branch_id);
                $('#outlet_owner').val(result.outlet_name);
                $('#outlet_name').val(result.outlet_name);
                $('#alamat').val(result.alamat);
                $('#type_outlet').val(result.type_outlet);
                $('#sales_name').val(result.salesname);
                $('#spv_sales').val(result.spv_sales);
            },
            error: function(error){
                console.log(error);
                alert("data tidak ditemukan");
            }
        });
    });

    $('#add_item').on('click', function (e) {
        e.preventDefault()
        let html = `
                <tr data-id="${index}">
                    <td><input type="text" id="item_${index}" name="item[]" class="item form-control" value=""></td>
                    <td><input type="text" id="order_${index}" name="order[]" class="order form-control" value=""></td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

        $('#rn_items').append(html)
        index++
    });

    $('#action_store').on('click', '#add_item', function (e) {
        e.preventDefault()
        let html = `
                <tr data-id="${index}">
                    <td><input type="text" id="item_${index}" name="item[]" class="item form-control" value=""></td>
                    <td><input type="text" id="order_${index}" name="order[]" class="order form-control" value=""></td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

        $('#rn_items').append(html)
        index++
    });

    $('#action_store').on('click', '#add_analisa', function (e) {
        e.preventDefault()
        let html = `
                <tr data-id="${index}">
                    <td>
                        <textarea class="form-control show" id="analisa_${index}" name="analisa[]"></textarea>
                    </td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

        $('#tr_analisa').append(html)
        index++
    });

    $('#form_ticket').on('click', '#add_action', function (e) {
        e.preventDefault()
        let html = `
                <tr data-id="${index}">
                    <td>
                        <input type='text' class="form-control datepicker show" id="action_date_${index}" name="action_date[]" value="">
                    </td>
                    <td>
                        <textarea class="form-control show" cols="45" rows="10" style="width: 241px; height: 34px;" id="bm_action_${index}" name="bm_action[]"></textarea>
                    </td>
                    <td>
                        <textarea class="form-control show" cols="45" rows="10" style="width: 241px; height: 34px;" id="cs_action_${index}" name="cs_action[]" readonly></textarea>
                    </td>
                    <td>
                        <div class="form-check">
                            <input class="form-check-input" value="102" type="radio" name="bm_status[]" id="bm_status1_${index}">
                            <label class="form-check-label" for="bm_status1_${index}">
                                Progress
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" value="200" type="radio" name="bm_status[]" id="bm_status2_${index}">
                            <label class="form-check-label" for="bm_status2_${index}">
                                Done
                            </label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check">
                            <input class="form-check-input" value="102" type="radio" name="cs_status[]" id="cs_status1_${index}" disabled>
                            <label class="form-check-label" for="cs_status1_${index}">
                                Progress
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" value="200" type="radio" name="cs_status[]" id="cs_status2_${index}"disabled>
                            <label class="form-check-label" for="cs_status2_${index}">
                                Done
                            </label>
                        </div>
                    </td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

        $('#tr_action').append(html)
        index++
    });
});

function cancelAnalisa(param) {
    swal({
        title: 'Are you sure want to delete this record',
        text: 'You will not be able to recover this record anymore!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $("tr[data-id='" + param + "']").addClass('table-danger');
            $("#analisa_"+param).prop('readonly', true);
            $("#is_delete_"+param).val('1');
            $("#cancel_"+param).hide();
        }else{
            Toast.fire({
                icon: 'error',
                title: 'Action Canceled'
            });
        }
    });
}

function cancelAction(param) {
    swal({
        title: 'Are you sure want to delete this record',
        text: 'You will not be able to recover this record anymore!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $("tr[data-id='" + param + "']").addClass('table-danger');
            $("#masalah_"+param).prop('readonly', true);
            $("#is_delete_"+param).val('1');
            $("#cancel_"+param).hide();
        }else{
            Toast.fire({
                icon: 'error',
                title: 'Action Canceled'
            });
        }
    });
}

function cancelDetail(param) {
    swal({
        title: 'Are you sure want to delete this record',
        text: 'You will not be able to recover this record anymore!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $("tr[data-id='" + param + "']").addClass('table-danger');
            $("#item_"+param).prop('readonly', true);
            $("#order_"+param).prop('readonly', true);
            $("#is_delete_"+param).val('1');
            $("#cancel_"+param).hide();
        }else{
            Toast.fire({
                icon: 'error',
                title: 'Action Canceled'
            });
        }
    });
}

const base_url = window.location.origin;
let index = 0;
$(document).ready(function()
{
    $('#action_store').on('focus',".datepicker", function(){
        $(this).datepicker({
            timepicker:false,
            locale: 'es',
            dateFormat:'yy-mm-d',
            // formatDate:'YYYY-MM-DD',
        });
    });

    $("#customer_id").select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'masukkan nama outlet / nomor pelanggan',
        ajax: {
            dataType: 'json',
            url: base_url + `/admin/select-cust`,
            delay: 250,
            data: function(params) {
                return {
                    search: params.term
                }
            },
            processResults: function (response) {
                return {
                    results:  $.map(response, function (item) {
                        return {
                            text: item.no_pelanggan+" - "+item.outlet_name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    $('#customer_id').on('change', function(e) {
        e.preventDefault()
        let val = $(this).val();
        if(!$(e.target).closest('#createModal').length && !$(e.target).is('#createModal')) {
            $(".modalDialog").hide();
        }
        $.ajax({
            url: base_url + `/admin/cust-details?val=${val}`,
            method: "GET",
            success: function(result) {
                console.log(result);
                $('#customer_code').val(result.no_pelanggan);
                $('#phone').val(result.phone_no);
                $('#branch').val(result.code+"-"+result.branch);
                $('#branch_code').val(result.code);
                $('#outlet_owner').val(result.realname);
                $('#pemilik').val(result.realname);
                $('#alamat').val(result.alamat);
                $('#type_outlet').val(result.type_outlet);
                $('#sales_name').val(result.salesname);
                $('#spv_sales').val(result.spv_sales);
            },
            error: function(error){
                console.log(error);
                alert("data tidak ditemukan");
            }
        });
    });

    $('.btn-save').on('click', function(e) {
        let formData = $("#createTicket").serialize();
        $('#name-error').html("");
        $('#outlet_name-error').html("");
        $('#phone_no-error').html("");
        $('#alamat-error').html("");
        // console.log(formData)
        var nama = $("#nama_penelpon").val();
        var no_telp = $("#no_telp").val();
        var customer = $('#customer_id').val();
        var complain = $('#complain_id').val();
        var tgl_problem = $('#problem_date').val();
        var masalah = $('#masalah').val();
        var description = $('#description').val();
        //non complain
        var realname = $("#realname");
        var branch_id = $("#branch_id");
        var outlet_name = $("#outlet_name");
        var phone_no = $("#phone_no");
        var alamat = $("#alamat");

        if(nama == '') {
            alert("Nama Penelpon Harus Diisi");
            $('#nama_penelpon-error').html("Harus Diisi");
            return false;
        }
        if(no_telp == '') {
            alert("Nomor Penelpon Harus Diisi");
            $('#no_telp-error').html("Harus Diisi");
            return false;
        }
        if(customer == '') {
            alert("Customer Harus Diisi");
            $('#customer_id-error').html("Harus Diisi");
            return false;
        }
        if(complain == '') {
            alert("Variable Complain Harus Diisi");
            $('#complain_id-error').html("Harus Diisi");
            return false;
        }
        if(tgl_problem == '') {
            alert("Tanggal realisasi tindak lanjut Harus Diisi");
            $('#problem_date-error').html("Harus Diisi");
            return false;
        }
        if(masalah == '') {
            alert("Keterangan Tindak lanjut Harus Diisi");
            $('#masalah-error').html("Harus Diisi");
            return false;
        }
        if(description == '') {
            alert("Detail Permasalahan Harus Diisi");
            $('#description-error').html("Harus Diisi");
            return false;
        }
        if(realname.length){
            if (realname.val() == '') {
                alert("Nama Pelanggan Harus Diisi");
                $('#realname-error').html("Harus Diisi");
                return false;
            }
        }
        if(branch_id.length){
            if (branch_id.val() == '') {
                alert("Branch Pelanggan Harus Diisi");
                $('#branch_id-error').html("Harus Diisi");
                return false;
            }
        }
        if(outlet_name.length){
            if (outlet_name.val() == '') {
                alert("Outlet Name Harus Diisi");
                $('#outlet_name-error').html("Harus Diisi");
                return false;
            }
        }
        if(phone_no.length){
            if (phone_no.val() == '') {
                alert("Nomor Telepon Pelanggan Harus Diisi");
                $('#phone_no-error').html("Harus Diisi");
                return false;
            }
        }
        if(alamat.length){
            if (alamat.val() == '') {
                alert("Alamat Pelanggan Harus Diisi");
                $('#alamat-error').html("Harus Diisi");
                return false;
            }
        }

        $("#btn-save").attr("disabled", true);
        $.ajax({
            url: base_url+'/admin/ticket',
            method: "POST",
            data: formData,
            success: function(result) {
                console.log(result)
                Toast.fire({
                    icon: 'success',
                    title: 'Successfully Updated',
                    timer: 2000
                });
                window.location.replace(base_url + "/admin/ticket/");
            }
        });
    });

    $('#branch_id').on('change', function(e) {
        let val = $(this).find(':selected').attr('data-id');
        $('#branch_code').val(val);
    });

    $('#action_store').on('click', '#add_item', function (e) {
        e.preventDefault()
        let html = `
                <tr data-id="${index}">
                    <td><input type="text" id="item_${index}" name="item[]" class="item form-control" value=""></td>
                    <td><input type="text" id="order_${index}" name="order[]" class="order form-control" value=""></td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

        $('#rn_items').append(html)
        index++
    });

    $('#action_store').on('click', '#add_analisa', function (e) {
        e.preventDefault()
        let html = `
                <tr data-id="${index}">
                    <td>
                        <textarea class="form-control show" id="analisa_${index}" name="analisa[]"></textarea>
                    </td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

        $('#tr_analisa').append(html)
        index++
    });

    $('#action_store').on('click', '#add_action', function (e) {
        e.preventDefault()
        let html = `
                <tr data-id="${index}">
                    <td>
                        <input type='text' class="form-control datepicker show" id="action_date_${index}" name="action_date[]" value="">
                    </td>
                    <td>
                        <textarea class="form-control show" cols="45" rows="10" style="width: 241px; height: 34px;" id="bm_action_${index}" name="bm_action[]"></textarea>
                    </td>
                    <td>
                        <textarea class="form-control show" cols="45" rows="10" style="width: 241px; height: 34px;" id="cs_action_${index}" name="cs_action[]" readonly></textarea>
                    </td>
                    <td>
                        <select class="form-control show" style="width: 100%;" name="bm_status[]" id="bm_status_${index}">
                            <option value=""> Pilih </option>
                            <option value="102">In Progress</option>
                            <option value="200">Done</option>
                        </select>
                    </td>
                    <td>
                        <select class="form-control show" style="width: 100%;" name="cs_status[]" id="cs_status_${index}">
                            <option value=""> Pilih </option>
                            <option value="102">In Progress</option>
                            <option value="200">Done</option>
                        </select>
                    </td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

        $('#tr_action').append(html)
        index++
    });
});

function cancelAnalisa(param) {
    swal({
        title: 'Are you sure want to delete this record',
        text: 'You will not be able to recover this record anymore!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $("tr[data-id='" + param + "']").addClass('table-danger');
            $("#analisa_"+param).prop('readonly', true);
            $("#is_delete_analisa_"+param).val('1');
            $("#cancel_"+param).hide();
        }else{
            Toast.fire({
                icon: 'error',
                title: 'Action Canceled'
            });
        }
    });
}

function cancelAction(param) {
    swal({
        title: 'Are you sure want to delete this record',
        text: 'You will not be able to recover this record anymore!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $("tr[data-id='" + param + "']").addClass('table-danger');
            $("#masalah_"+param).prop('readonly', true);
            $("#is_delete_"+param).val('1');
            $("#cancel_"+param).hide();
        }else{
            Toast.fire({
                icon: 'error',
                title: 'Action Canceled'
            });
        }
    });
}

function cancelDetail(param) {
    swal({
        title: 'Are you sure want to delete this record',
        text: 'You will not be able to recover this record anymore!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $("tr[data-id='" + param + "']").addClass('table-danger');
            $("#item_"+param).prop('readonly', true);
            $("#order_"+param).prop('readonly', true);
            $("#is_delete_"+param).val('1');
            $("#cancel_"+param).hide();
        }else{
            Toast.fire({
                icon: 'error',
                title: 'Action Canceled'
            });
        }
    });
}

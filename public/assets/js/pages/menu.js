const base_url = window.location.origin;

$(document).ready(function()
{
    $('.add-new').on('click', function(e) {
        e.preventDefault();
        $('#modal-store').find("input[type=text], textarea").val("");
        $.ajax({
            url: base_url + `/admin/menu/create`,
            method: "GET",
            success: function(result) {
                $('#createModal').find('.modal-body').html(result);
                $('#createModal').find(".select2").select2({
                    dropdownParent: $('#createModal')
                });
                $('#createModal').modal('show');
            }
        });
    });

    $('#btn-save').on('click', function(e) {
        let formData = $("#modal-store").serialize();
        $('#name-error').html("");
        console.log(formData);
        $.ajax({
            url: base_url+'/admin/menu',
            method: "POST",
            data: formData,
            success: function(result) {
                console.log(result);
                if(result.errors) {
                    if(result.errors.name) {
                        $('#name-error').html(data.errors.name[0]);
                    }
                } else {
                    $('#createModal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Successfully Saved'
                    });
                    window.location.assign(base_url+'/admin/menu');
                }
            }
        });
    });

    $('#index-table').on('click', '.edit-index', function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        $('#modal-update').find("input[type=text], textarea").val("");
        $.ajax({
            url: base_url + `/admin/menu/${id}/edit`,
            method: "GET",
            success: function(result) {
                $('#updateModal').find('.modal-body').html(result);
                $('#updateModal').modal('show')
            },
            error: function(error){
                console.log(error)
            }
        });
    });

    $('#btn-update').on('click', function(e) {
        let id = $('#modal-update').find('#id').val();
        let formData = $('#modal-update').serialize();
        $('#name-error').html("");
        $.ajax({
            url: base_url + `/admin/menu/${id}`,
            method: "PUT",
            data: formData,
            success: function(result) {
                // console.log(result)
                if(result.errors) {
                    if(result.errors.name) {
                        $('#name-error').html(data.errors.name[0]);
                    }
                } else {
                    $('#updateModal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Updated Successfully'
                    });
                    window.location.assign(base_url+'/admin/menu');
                }
            }
        });
    });

    $('#index-table').on('click', '.show-index', function (e) {
        e.preventDefault();
        let id = $(this).data('id');

        $.ajax({
            url: base_url + '/admin/menu/'+id,
            method: "GET",
            success: function(data) {
                $('#showModal').find('.modal-body').html(data);
                $('#showModal').modal('show')
            },
            error: function(error){
                console.log(error)
            }
        });
    });

    $('#index-table').on('click', '.badge-danger', function (e) {
        let id = $(this).data("id");
        let basename = $(this).data("name");

        swal({
            title: 'Are you sure want to delete this record '+basename,
            text: 'You will not be able to recover this record anymore!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $(`#delete_${id}`).submit();
            }
        });
    });

});

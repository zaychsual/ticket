const base_url = window.location.origin;
let index = 0;
$(document).ready(function()
{
    $('#action_store').on('focus',".datepicker", function(){
        $(this).datepicker({
            timepicker:false,
            locale: 'es',
            dateFormat:'yy-mm-d',
            // formatDate:'YYYY-MM-DD',
        });
    });

    $('#add_analisa').on('click', function(e) {
        e.preventDefault()
        var analisa = $("#analisa_count").val();

        if(parseInt(analisa) < 5) {
            let html = `
                <tr data-id="${analisa}">
                    <td>
                        Why ${parseInt(analisa)+1}
                        <input type="text" id="analisa_masalah_${analisa}" name="analisa_masalah[]" class="item form-control" value="">
                    </td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove();reduceAnalisaCount(${analisa})">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

            $('#tr_analisa').append(html)
            $("#analisa_count").val(parseInt(analisa)+1);
        } else {
            alert('maksimal 5 analisa');
            return false;
        }
    });

    $('.btn-save').on('click', function(e) {
        let formData = $("#action_store").serialize();
        let id = $("#ticket_id").val();
        $('#name-error').html("");
        $('#outlet_name-error').html("");
        $('#phone_no-error').html("");
        $('#alamat-error').html("");
        var analisa1 = $("#analisa_masalah_0");
        var analisa2 = $("#analisa_masalah_1");
        var analisa3 = $("#analisa_masalah_2");
        var bm_action = $("#result_action");
        var pencegahan = $("#action_pencegahan");
        var file = $("#file");
        const oFile = document.getElementById("file").files[0];
        if(analisa1.length) {
            if (analisa1.val() == '') {
                alert("Analisa Masalah Harus Diisi");
                $('#analisa_masalah0-error').html("Harus Diisi");
                $(".btn-save").attr("disabled", false);
                return false;
            }
        }
        if(analisa2.length) {
            if (analisa2.val() == '') {
                alert("Analisa Masalah Harus Diisi");
                $('#analisa_masalah1-error').html("Harus Diisi");
                $(".btn-save").attr("disabled", false);
                return false;
            }
        }
        if(analisa3.length) {
            if (analisa3.val() == '') {
                alert("Analisa Masalah Harus Diisi");
                $('#analisa_masalah2-error').html("Harus Diisi");
                $(".btn-save").attr("disabled", false);
                return false;
            }
        }
        if(bm_action.length) {
            if (bm_action.val() == '') {
                alert("Hasil Tindak Lanjut Harus Diisi");
                $('#createModal').find('#branch_id-error').html("Harus Diisi");
                $(".btn-save").attr("disabled", false);
                return false;
            }
        }
        if(pencegahan.length) {
            if (pencegahan.val() == '') {
                alert("Tindakan Pencegahan Harus Diisi");
                $('#createModal').find('#branch_id-error').html("Harus Diisi");
                $(".btn-save").attr("disabled", false);
                return false;
            }
        }
        if(file.length) {
            if(file.val() == '') {
                alert("File harus di isi");
                $('#createModal').find('#file-error').html("Harus Diisi");
                $(".btn-save").attr("disabled", false);
                return false;
            }
            if (oFile.size > 2097152) { // 2 MiB for bytes.
                alert("File Tidak boleh lebih dari 2MB!");
                $(".btn-save").attr("disabled", false);
                return false;
            }
        }
        $.ajax({
            url: base_url + `/admin/ticket/${id}`,
            method: "PUT",
            data: formData,
            success: function(result) {
                // console.log(result)
                if(result.errors) {
                    if(result.errors.name) {
                        $('#name-error').html(data.errors.name[0]);
                    }
                    if(result.errors.outlet_name) {
                        $('#outlet_name-error').html(data.errors.outlet_name[0]);
                    }
                    if(result.errors.phone_no) {
                        $('#phone_no-error').html(data.errors.phone_no[0]);
                    }
                    if(result.errors.alamat) {
                        $('#alamat-error').html(data.errors.alamat[0]);
                    }
                } else {
                    Toast.fire({
                        icon: 'success',
                        title: 'Successfully Updated',
                        timer: 2000
                    });
                    window.location.replace(base_url + "/admin/ticket/");
                    // $('#createModal').modal('hide');
                }
            }
        });
    });

    $('#action_store').on('click', '#add_item', function (e) {
        e.preventDefault()
        let html = `
                <tr data-id="${index}">
                    <td><input type="text" id="item_${index}" name="item[]" class="item form-control" value=""></td>
                    <td><input type="text" id="order_${index}" name="order[]" class="order form-control" value=""></td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

        $('#rn_items').append(html)
        index++
    });

    $('#is_submit_bm').on('click', function (e) {
        if($(this).is(':checked')) {
            $(".btn-save").attr("disabled", false);
        } else {
            $(".btn-save").attr("disabled", true);
        }
    });

    $('#action_store').on('click', '#add_action', function (e) {
        e.preventDefault()
        let html = `
                <tr data-id="${index}">
                    <td>
                        <input type='text' class="form-control datepicker show" id="action_date_${index}" name="action_date[]" value="">
                    </td>
                    <td>
                        <textarea class="form-control show" cols="45" rows="10" style="width: 241px; height: 34px;" id="bm_action_${index}" name="bm_action[]"></textarea>
                    </td>
                    <td>
                        <textarea class="form-control show" cols="45" rows="10" style="width: 241px; height: 34px;" id="cs_action_${index}" name="cs_action[]" readonly></textarea>
                    </td>
                    <td>
                        <select class="form-control show" style="width: 100%;" name="bm_status[]" id="bm_status_${index}">
                            <option value=""> Pilih </option>
                            <option value="102">In Progress</option>
                            <option value="200">Done</option>
                        </select>
                    </td>
                    <td>
                        <select class="form-control show" style="width: 100%;" name="cs_status[]" id="cs_status_${index}">
                            <option value=""> Pilih </option>
                            <option value="102">In Progress</option>
                            <option value="200">Done</option>
                        </select>
                    </td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

        $('#tr_action').append(html)
        index++
    });
});

function cancelAnalisa(param) {
    swal({
        title: 'Are you sure want to delete this record',
        text: 'You will not be able to recover this record anymore!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $("tr[data-id='" + param + "']").addClass('table-danger');
            $("#analisa_"+param).prop('readonly', true);
            $("#is_delete_analisa_"+param).val('1');
            $("#cancel_"+param).hide();
        }else{
            Toast.fire({
                icon: 'error',
                title: 'Action Canceled'
            });
        }
    });
}

function cancelAction(param) {
    swal({
        title: 'Are you sure want to delete this record',
        text: 'You will not be able to recover this record anymore!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $("tr[data-id='" + param + "']").addClass('table-danger');
            $("#masalah_"+param).prop('readonly', true);
            $("#is_delete_"+param).val('1');
            $("#cancel_"+param).hide();
        }else{
            Toast.fire({
                icon: 'error',
                title: 'Action Canceled'
            });
        }
    });
}

function reduceAnalisaCount(param) {
    $("#analisa_count").val(param);
}

function cancelDetail(param) {
    swal({
        title: 'Are you sure want to delete this record',
        text: 'You will not be able to recover this record anymore!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $("tr[data-id='" + param + "']").addClass('table-danger');
            $("#item_"+param).prop('readonly', true);
            $("#order_"+param).prop('readonly', true);
            $("#is_delete_"+param).val('1');
            $("#cancel_"+param).hide();
        }else{
            Toast.fire({
                icon: 'error',
                title: 'Action Canceled'
            });
        }
    });
}

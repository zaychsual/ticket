const base_url = window.location.origin;

$(document).ready( function(){
    var report;

    $(document).on('change', '#start, #end, #kategori',  function(e) {
        e.preventDefault();
        var start = $('#start').val();
        var end = $('#end').val();
        var kategori = $('#kategori').val();

        $.ajax({
            url: base_url+'/admin/report-komplain-data',
            type: 'get',
            data: {
                start: start,
                end: end,
                kategori: kategori,
            },
            dataType: 'json',
            success: function(data) {
                // console.log(data);
                var html = '';
                var sum_newstatus = 0;
                var sum_proses = 0;
                var sum_revised = 0;
                var sum_closed = 0;
                $.each(data, function(i, item) {
                    console.log(item.kategori);
                    html += '<tr>';
                    html += '<td>'+item.kategori+'</td>';
                    html += '<td>'+item.newstatus+'</td>';
                    html += '<td>'+item.proses+'</td>';
                    html += '<td>'+item.revised+'</td>';
                    html += '<td>'+item.closed+'</td>';
                    html += '</tr>';
                    sum_newstatus += parseFloat(item.newstatus);
                    sum_proses += parseFloat(item.proses);
                    sum_revised += parseFloat(item.revised);
                    sum_closed += parseFloat(item.closed);
                });
                $('.tbody-data tr').remove();
                $('.tbody-data').append(html);
                // console.log(sum_newstatus);
                $('.sum_total_new_data').html(sum_newstatus);
                $('.sum_total_progress').html(sum_proses);
                $('.sum_total_revised').html(sum_revised);
                $('.sum_total_complete').html(sum_closed);
            }
        })
    });

    $(document).on('click','#export',function() {
        $("#export").attr("href", '/admin/report-komplain-export?rentang_tanggal='+$("#start").val()+"&end="+$("#end").val()+"&kategori="+$("#kategori").val())
    })
})

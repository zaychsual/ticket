const base_url = window.location.origin;
let index = 0;

$(document).ready(function()
{
    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "extract-date-pre": function ( a ) {
            var ukDatea = a.split('-');
            return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
        },
        "extract-date-asc": function(a, b) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
        "extract-date-desc": function(a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });

    var indexTable;
    indexTable =$('#ajax-table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'pdf', 'print'
        ],
        processing: true,
        serverside: true,
        ajax: {
            url: base_url + '/admin/ticket-ajax',
        },
        // searchDelay: 750,
        columnDefs: [ { type: 'extract-date', 'targets': [8] } ],
        order: [[ 8, "desc" ]],
        pageLength: 10,
        dom: 'Bftrip',
        columns: [
            {
                data: 'no_ref',
                name: 'no_ref'
            },
            {
                data: 'ticket_category',
                name: 'ticket_category'
            },
            {
                data: 'nama_penelpon',
                name: 'nama_penelpon'
            },
            {
                data: 'customer_id',
                name: 'customer_id'
            },
            {
                data: 'branch',
                name: 'branch'
            },
            {
                data: 'complain_id',
                name: 'complain_id'
            },
            {
                data: 'main_status_id',
                name: 'main_status_id'
            },
            {
                data: 'createdby',
                name: 'createdby'
            },
            {
                data: 'created_at',
                name: 'created_at',
            },
            {
                data: 'ID',
                name: 'ID',
                orderable: false,
                searchable: false
            },
        ],
    });

    $('.dt-button, .buttons-copy, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn-primary');

    $('#btn-save').on('click', function(e) {
        $(this).hide();

        let formData = $("#modal-store").serialize();
        $('#name-error').html("");
        $('#outlet_name-error').html("");
        $('#phone_no-error').html("");
        $('#alamat-error').html("");
        var nama = $('#createModal').find("#nama_penelpon").val();
        var no_telp = $('#createModal').find("#no_telp").val();
        var customer = $('#createModal').find('#customer_id').val();
        var complain = $('#createModal').find('#complain_id').val();
        var tgl_problem = $('#createModal').find('#problem_date').val();
        var masalah = $('#createModal').find('#masalah').val();
        var description = $('#createModal').find('#description').val();
        //non complain
        var realname = $('#createModal').find("#realname");
        var branch_id = $('#createModal').find("#branch_id");
        var outlet_name = $('#createModal').find("#outlet_name");
        var phone_no = $('#createModal').find("#phone_no");
        var alamat = $('#createModal').find("#alamat");

        if(nama == '') {
            $(this).show();
            alert("Nama Penelpon Harus Diisi");
            $('#createModal').find('#nama_penelpon-error').html("Harus Diisi");
            return false;
        }
        if(no_telp == '') {
            $(this).show();
            alert("Nomor Penelpon Harus Diisi");
            $('#createModal').find('#no_telp-error').html("Harus Diisi");
            return false;
        }
        if(customer == '') {
            $(this).show();
            alert("Customer Harus Diisi");
            $('#createModal').find('#customer_id-error').html("Harus Diisi");
            return false;
        }
        if(complain == '') {
            alert("Variable Complain Harus Diisi");
            $('#createModal').find('#complain_id-error').html("Harus Diisi");
            return false;
        }
        if(tgl_problem == '') {
            $(this).show();
            alert("Tanggal realisasi tindak lanjut Harus Diisi");
            $('#createModal').find('#problem_date-error').html("Harus Diisi");
            return false;
        }
        if(masalah == '') {
            $(this).show();
            alert("Keterangan Tindak lanjut Harus Diisi");
            $('#createModal').find('#masalah-error').html("Harus Diisi");
            return false;
        }
        if(description == '') {
            $(this).show();
            alert("Detail Permasalahan Harus Diisi");
            $('#createModal').find('#description-error').html("Harus Diisi");
            return false;
        }
        if(realname.length){
            $(this).show();
            if (realname.val() == '') {
                alert("Nama Pelanggan Harus Diisi");
                $('#createModal').find('#realname-error').html("Harus Diisi");
                return false;
            }
        }
        if(branch_id.length){
            if (branch_id.val() == '') {
                $(this).show();
                alert("Branch Pelanggan Harus Diisi");
                $('#createModal').find('#branch_id-error').html("Harus Diisi");
                return false;
            }
        }
        /* if(outlet_name.length){
            if (outlet_name.val() == '') {
                alert("Outlet Name Harus Diisi");
                $('#createModal').find('#outlet_name-error').html("Harus Diisi");
                return false;
            }
        } */
        if(phone_no.length){
            if (phone_no.val() == '') {
                $(this).show();
                alert("Nomor Telepon Pelanggan Harus Diisi");
                $('#createModal').find('#phone_no-error').html("Harus Diisi");
                return false;
            }
        }
        if(alamat.length){
            if (alamat.val() == '') {
                $(this).show();
                alert("Alamat Pelanggan Harus Diisi");
                $('#createModal').find('#alamat-error').html("Harus Diisi");
                return false;
            }
        }

        $.ajax({

            url: base_url+'/admin/ticket',
            method: "POST",
            data: formData,
            success: function(result) {
                // console.log(result)
                if(result.errors) {
                    if(result.errors.name) {
                        $('#name-error').html(data.errors.name[0]);
                    }
                    if(result.errors.outlet_name) {
                        $('#outlet_name-error').html(data.errors.outlet_name[0]);
                    }
                    if(result.errors.phone_no) {
                        $('#phone_no-error').html(data.errors.phone_no[0]);
                    }
                    if(result.errors.alamat) {
                        $('#alamat-error').html(data.errors.alamat[0]);
                    }
                } else {
                    $('#createModal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Successfully Saved'
                    });
                    indexTable.ajax.reload()
                }
            }
        });
    });

    $('#modal-update').on('click', '#is_submit_bm', function (e) {
        if($(this).is(':checked')) {
            $("#btn-update").attr("disabled", false);
        } else {
            $("#btn-update").attr("disabled", true);
        }
    });

    $('#ajax-table').on('click', '.edit-index', function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        $("#btn-update").attr("disabled", false);
        $.ajax({
            url: base_url + `/admin/ticket/${id}/edit`,
            method: "GET",
            success: function(data) {
                $('#updateModal').find('#modal-update').attr("action", base_url + "/admin/ticket/"+id);
                $('#modal-update').find("input[type=text], textarea, select").val("");
                $('#updateModal').find('.modal-body').html(data);
                $('#updateModal').find('#analisa_count').val(3);
                $('#updateModal').modal('show');
                $('#updateModal').find(".select2").select2({
                    dropdownParent: $('#updateModal')
                });
                $('#updateModal').find(".datepicker").datetimepicker({
                    timepicker:false,
                    format:'Y-m-d',
                    formatDate:'Y-m-d',
                });
                $('#updateModal').find(".timepicker").datetimepicker({
                    datepicker:false,
                    format:'H:i',
                    step:5
                });
            },
            error: function(error){
                console.log(error)
            }
        });
    });

    $('#btn-update').on('click', function(e) {
        let id = $('#modal-update').find('#id').val();
        let formData = $('#modal-update').serialize();
        $('#name-error').html("");
        $('#outlet_name-error').html("");
        $('#phone_no-error').html("");
        $('#alamat-error').html("");
        $("#btn-update").attr("disabled", true);
        $.ajax({
            url: base_url + '/admin/ticket/'+id,
            method: "PUT",
            data: formData,
            success: function(result) {
                // console.log(result)
                if(result.errors) {
                    if(result.errors.name) {
                        $('#name-error').html(data.errors.name[0]);
                    }
                    if(result.errors.outlet_name) {
                        $('#outlet_name-error').html(data.errors.outlet_name[0]);
                    }
                    if(result.errors.phone_no) {
                        $('#phone_no-error').html(data.errors.phone_no[0]);
                    }
                    if(result.errors.alamat) {
                        $('#alamat-error').html(data.errors.alamat[0]);
                    }
                } else {
                    $('#updateModal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Updated Successfully'
                    });
                    indexTable.ajax.reload()
                }
            }
        });
    });

    $('#ajax-table').on('click', '.show-index', function (e) {
        e.preventDefault();
        let id = $(this).data('id');

        $.ajax({
            url: base_url + `/admin/ticket/${id}`,
            method: "GET",
            success: function(data) {
                $('#showModal').find('.modal-body').html(data);
                $('#showModal').modal('show')
            },
            error: function(error){
                console.log(error)
            }
        });
    });

    $('#ajax-table').on('click', '.print-index', function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let win = window.open(base_url + `/admin/ticket/print/${id}`, '_blank');
        if (win) {
            //Browser has allowed it to be opened
            win.focus();
        } else {
            //Browser has blocked it
            alert('Please allow popups for this website');
        }
    });

    $('#ajax-table').on('click', '.del-index', function (e) {
        e.preventDefault();
        let id = $(this).data("id");
        let basename = $(this).data("name");
        let crfs = $(this).data("token");
        swal({
            title: 'Are you sure want to delete this record '+basename,
            text: 'You will not be able to recover this record anymore!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url : base_url+`/admin/ticket/${id}`,
                    type: "DELETE",
                    data: {
                        'id': id,
                        '_token': crfs
                    },
                    error: function(data) {
                        // console.log(data);
                        alert('Something is wrong');
                    },
                    success: function(data) {
                        Toast.fire({
                            icon: 'success',
                            title: 'Deleted Successfully'
                        });
                        indexTable.ajax.reload()
                    }
                });
            }else{
                Toast.fire({
                    icon: 'error',
                    title: 'Action Canceled'
                });
            }
        });
    });

    $('.add-new').on('click', function(e) {
        e.preventDefault();
        let id = $(this).data('id');
        var dateNow = new Date();
        $('#createModal').find("input[type=text], textarea, select").val("");
        $("#btn-save").show();
        $.ajax({
            url: base_url + `/admin/ticket/option/${id}`,
            method: "GET",
            success: function(result) {
                $('#createModal').find('.modal-body').html(result);
                $('#createModal').modal('show');
                $('#createModal').find(".select2").select2({
                    dropdownParent: $('#createModal')
                });
                $('#createModal').find("#customer_id").select2({
                    minimumInputLength: 3,
                    allowClear: true,
                    placeholder: 'masukkan nama outlet / nomor pelanggan',
                    ajax: {
                        dataType: 'json',
                        url: base_url + `/admin/select-cust`,
                        delay: 250,
                        data: function(params) {
                            return {
                                search: params.term
                            }
                        },
                        processResults: function (response) {
                            return {
                                results:  $.map(response, function (item) {
                                    return {
                                        text: item.no_pelanggan+" - "+item.outlet_name,
                                        id: item.id
                                    }
                                })
                            };
                        },
                        cache: true
                    }
                });
                $('#createModal').find(".datepicker").datetimepicker({
                    timepicker:false,
                    format:'Y-m-d',
                    formatDate:'Y-m-d',
                });
                $('#createModal').find(".timepicker").datetimepicker({
                    datepicker:false,
                    format:'H:i',
                    step:5
                });
            }
        })
    });

    $('#updateModal').on('click', '#btn_closed', function (e) {
        e.preventDefault()
        $('#updateModal').find('#main_status_id').val("200");
    });

    $('#updateModal').on('click', '#add_item', function (e) {
        e.preventDefault()
        let html = `
                <tr data-id="${index}">
                    <td><input type="text" id="item_${index}" name="item[]" class="item form-control" value=""></td>
                    <td><input type="text" id="order_${index}" name="order[]" class="order form-control" value=""></td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

        $('#rn_items').append(html)
        index++
    });

    $('#updateModal').on('click', '#add_analisa', function (e) {
        e.preventDefault()
        var analisa = $("#analisa_count").val();

        if(parseInt(analisa) < 5) {
            let html = `
                <tr data-id="${analisa}">
                    <td>
                        Why ${parseInt(analisa)+1}
                        <input type="text" id="analisa_masalah_${analisa}" name="analisa_masalah[]" class="item form-control" value="">
                    </td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove();reduceAnalisaCount(${analisa})">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

            $('#tr_analisa').append(html)
            $("#analisa_count").val(parseInt(analisa)+1);
        } else {
            alert('maksimal 5 analisa');
            return false;
        }
    });

    $('#createModal').on('click', '#add_item', function (e) {
        e.preventDefault()
        let html = `
                <tr data-id="${index}">
                    <td><input type="text" id="item_${index}" name="item[]" class="item form-control" value=""></td>
                    <td><input type="text" id="order_${index}" name="order[]" class="order form-control" value=""></td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

        $('#rn_items').append(html)
        index++
    });

    // $('#updateModal').on('click', '#add_analisa', function (e) {
    //     e.preventDefault()
    //     let html = `
    //             <tr data-id="${index}">
    //                 <td>
    //                     <textarea class="form-control show" id="analisa_${index}" name="analisa[]"></textarea>
    //                 </td>
    //                 <td>
    //                     <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
    //                         <i class="fa fa-times"></i>
    //                     </a>
    //                 </td>
    //             </tr>
    //         `

    //     $('#tr_analisa').append(html)
    //     index++
    // });

    $('#updateModal').on('click', '#add_action', function (e) {
        e.preventDefault()
        let html = `
                <tr data-id="${index}">
                    <td>
                        <input type='text' class="form-control datepicker show" id="action_date_${index}" name="action_date[]" value="">
                    </td>
                    <td>
                        <textarea class="form-control show" cols="45" rows="10" style="width: 241px; height: 34px;" id="bm_action_${index}" name="bm_action[]"></textarea>
                    </td>
                    <td>
                        <textarea class="form-control show" cols="45" rows="10" style="width: 241px; height: 34px;" id="cs_action_${index}" name="cs_action[]" readonly></textarea>
                    </td>
                    <td>
                        <select class="form-control show" style="width: 100%;" name="bm_status[]" id="bm_status_${index}">
                            <option value=""> Pilih </option>
                            <option value="102">In Progress</option>
                            <option value="200">Done</option>
                        </select>
                    </td>
                    <td>
                        <select class="form-control show" style="width: 100%;" name="cs_status[]" id="cs_status_${index}">
                            <option value=""> Pilih </option>
                            <option value="102">In Progress</option>
                            <option value="200">Done</option>
                        </select>
                    </td>
                    <td>
                        <a href="javascript:;" class="remove-item btn btn-danger btn-sm" onclick="this.parentNode.parentNode.remove()">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            `

        $('#tr_action').append(html)
        index++
    });

    $('#createModal').on('change', '#branch_id', function(e) {
        let val = $(this).find(':selected').attr('data-id');
        $('#createModal').find('#branch_code').val(val);
    });

    $('#createModal').on('change', '#customer_id', function(e) {
        e.preventDefault()
        let val = $(this).val();
        if(!$(e.target).closest('#createModal').length && !$(e.target).is('#createModal')) {
            $(".modalDialog").hide();
        }
        $.ajax({
            url: base_url + `/admin/cust-details?val=${val}`,
            method: "GET",
            success: function(result) {
                console.log(result);
                $('#createModal').find('#customer_code').val(result.no_pelanggan);
                $('#createModal').find('#phone_no').val(result.phone_no);
                $('#createModal').find('#phone').val(result.phone_no);
                $('#createModal').find('#branch').val(result.code+"-"+result.branch);
                $('#createModal').find('#branch_code').val(result.code);
                $('#createModal').find('#outlet_owner').val(result.realname);
                $('#createModal').find('#outlet_name').val(result.realname);
                $('#createModal').find('#alamat').val(result.alamat);
                $('#createModal').find('#type_outlet').val(result.type_outlet);
                $('#createModal').find('#sales_name').val(result.salesname);
                $('#createModal').find('#spv_sales').val(result.spv_sales);
            },
            error: function(error){
                console.log(error);
                alert("data tidak ditemukan");
            }
        });
    });
});

function reduceAnalisaCount(param) {
    $("#analisa_count").val(param);
}

function cancelAnalisa(param) {
    swal({
        title: 'Are you sure want to delete this record',
        text: 'You will not be able to recover this record anymore!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $("tr[data-id='" + param + "']").addClass('table-danger');
            $("#analisa_"+param).prop('readonly', true);
            $("#is_delete_analisa_"+param).val('1');
            $("#cancel_"+param).hide();
        }else{
            Toast.fire({
                icon: 'error',
                title: 'Action Canceled'
            });
        }
    });
}

function cancelDetail(param) {
    swal({
        title: 'Are you sure want to delete this record',
        text: 'You will not be able to recover this record anymore!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $("tr[data-id='" + param + "']").addClass('table-danger');
            $("#item_"+param).prop('readonly', true);
            $("#order_"+param).prop('readonly', true);
            $("#is_delete_"+param).val('1');
            $("#cancel_"+param).hide();
        }else{
            Toast.fire({
                icon: 'error',
                title: 'Action Canceled'
            });
        }
    });
}

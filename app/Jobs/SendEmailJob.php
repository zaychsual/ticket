<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\TryEmail;
use App\Models\LogEmail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $logEmail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(LogEmail $logEmail)
    {
        $this->logEmail = $logEmail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $email = $this->logEmail->email;
            $email_cc = explode(',', $this->logEmail->email_cc);
            $details = [
                'subject' => $this->logEmail->subject,
                'title' => $this->logEmail->title,
                'body'  => $this->logEmail->content,
                'branch' => $this->logEmail->branch,
            ];

            Mail::to($email)
            ->cc($email_cc)
            ->send(new TryEmail($details));
            $this->logEmail->update(['status' => 'Success']);
        } catch (\Throwable $th) {
            $this->logEmail->update([
                'status' => 'Failed',
                'response' => $th->getMessage(),
            ]);
        }
    }
}

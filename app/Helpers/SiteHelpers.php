<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DateTime;
use App\Models\Ticket;
use App\Models\Customer;
use App\Models\Setting;
use App\Models\AccessMenu;
use App\Models\Menu;
use App\Models\Branch;
use App\Models\User;
use App\Models\TypeOutlet;
use App\Models\TicketCustomer;
class SiteHelpers {

    const status = [
        'New Ticket' => 300,
        'Progress' => 102,
        'To Be Revised' => 103,
        'Closed' => 200,
    ];
    const TIKET_COMPLAIN = '11';
    const TIKET_NON_COMPLAIN = '22';

    public static function cekAkses($menu, $akses)
    {
        $data = AccessMenu::join('roles', 'access_menus.role_id', '=', 'roles.id')
            ->join('menus', 'access_menus.menu_id', '=', 'menus.id')
            ->where('access_menus.role_id', Auth::user()->role_id)
            ->where('menus.name', $menu)
            ->where('access_menus.'.$akses, 1)->first();

            if($data) {
            return true;
        } else {
            return false;
        }
    }

    public static function cekRole($data)
    {
        $result = User::where('id', Auth::user()->id)
            ->where('role_id', $data)->first();

        if($result) {
            return true;
        } else {
            return false;
        }
    }

    public static function getAllCC($data)
    {
        $cc = [];
        $head = self::getEmailHead()->prefix ?? 'no@email.com';
        $branch = self::getBranchUser($data)->email_bom ?? 'no@email.com';
        $cs = self::getMailCS();
        foreach($cs as $key => $raw) {
            $emailCS[] = $raw->email;
        }

        $data = User::where('branch_code','like', '%'.$data.'%')->first();
        if (str_contains($data->email_admin, ',')){
            $mailAdmins = explode(",",$data->email_admin ?? '');
        } else {
            $mailAdmins[] = $data->email_admin ?? '';
        }
        array_push($cc, $head, $branch);
        $email = array_merge($cc, $emailCS, $mailAdmins);

        return (array) $email;
    }

    public static function cekSubMenu($data)
    {
        $result = Menu::where('master_menu', $data)->get();

        if($result->isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

	public static function getMailCS()
    {
        $data = User::where('role_id', 2)
            ->select('email')
            ->get();

        return $data;
    }

    public static function cekMenuAktif($data)
    {
        $result = Menu::where('menu_aktif', $data)->first();
        if(empty($result)) {
            $result = '';
        }
        return $result;
    }

    public static function mainMenu()
    {
        $data = DB::table('access_menus')->join('menus', 'access_menus.menu_id', '=', 'menus.id')
            ->where('access_menus.role_id', Auth::user()->role_id)
            ->where('menus.level_menu', 'main_menu')
            ->where('access_menus.akses', 1)
            ->selectRaw('menus.*')
            ->orderBy('menus.no_urut')
            ->get();

        return $data;
    }

    public static function subMenu()
    {
        $data = AccessMenu::join('menus', 'access_menus.menu_id', '=', 'menus.id')
            ->where('access_menus.role_id', Auth::user()->role_id)
            ->where('menus.level_menu', 'sub_menu')
            ->where('access_menus.akses', 1)
            ->select('menus.*')
            ->orderBy('menus.no_urut')
            ->get();

        return $data;
    }

    public static function getBranchUser($q)
    {
        $data = User::where('branch_code', 'like', '%'.$q.'%')->first();

        return $data;
    }

    public static function getBranchName($q)
    {
        $data = Branch::where('code', $q)->first();

        return $data;
    }

    public static function templateEmail($id)
    {
        $data = Ticket::findOrFail($id);
        $link = route('admin.ticket.action', $data->id);
        $branchUser = self::getBranchUser($data->branch_code)->name ?? '';
        $branchName = self::getBranchName($data->branch_code)->name ?? '';
        if($data->ticket_category == Ticket::Complain) { //complain
            $dataCust = Customer::findOrFail($data->customer_id);
        } elseif ($data->ticket_category == Ticket::NonComplain) { //noncomplain
            $dataCust = TicketCustomer::where('ticket_id', $id)->first();
        }

        $body = '<p>
                    Kepada Yth,
                </p>
                <p>
                    <strong>Bpk/Ibu. '.ucfirst($branchUser).' (BM '.ucfirst($branchName).')</strong>
                </p>
                <p>
                    Di Tempat
                </p>
                <p>
                    Dengan Hormat,
                </p>
                <p>
                    Melalui ini kami sampaikan Informasi yang masuk melalui saluran bebas pulsa
                    CSD adalah sebagai berikut :
                </p>
                <p>
                    Nama Toko : '.$dataCust->outlet_name.'
                </p>
                <p>
                    Nama Pemilik : '.$dataCust->realname.'
                </p>
                <p>
                    Nomor Pelanggan : '.$dataCust->no_pelanggan.'
                </p>
                <p>
                    Alamat : '.$dataCust->alamat.'
                </p>
                <p>
                    Perihal : <p>'.$data->description.'</p>
                </p>
                <p>
                    Link Tiket komplain : <a href="'.$link.'" target="_blank">Ticketing.</a>
                </p>
                <p>
                    Mohon mengisi kolom
                    <strong>
                        <em>
                            “analisa akar masalah”, “tindaklanjut”, “hasil tindaklanjut”, dan
                            “tindakan pencegahan”
                        </em>
                    </strong>
                yang telah dilakukan oleh cabang    <strong><em>&amp; mengirimkan bukti tindak lanjut</em></strong>
                </p>
                <p>
                    Pelaporan kembali Tiket Komplain paling lambat <strong><em>'.date('d M y',strtotime('+1 day')).'</em></strong>
                </p>
                <p>
                    <strong><em> </em></strong>
                </p>
                <p>
                    Atas perhatiannya kami ucapkan terima kasih
                </p>
                <p>
                    Regards,
                </p>
                <p>
                    '.Auth::user()->name ?? ''.'
                </p>
                <p>
                    Customer Service
                </p>
                ';
        return $body;
    }

    public static function getEmailHead()
    {
        $data = Setting::where('name', 'HEAD_EMAIL')->first();

        return $data;
    }

    public static function cekNotif()
    {
        $query = Ticket::leftJoin('ticket_customers', 'ticket_customers.ticket_id', '=', 'tickets.id');

        if(self::cekRole(3)) {
            $query->whereIn('tickets.main_status_id', [Ticket::NewTicket, Ticket::ToBeRevised]);
            $query->where('tickets.branch_code', Auth::user()->branch_code);
        } elseif (self::cekRole(2)) {
            $query-> whereIn('main_status_id', [Ticket::ToBeRevised, Ticket::Progress]);
            $query->where('tickets.created_by', Auth::user()->id);
        }
        $query->select('tickets.id', 'tickets.main_status_id',
                'tickets.no_ref', 'tickets.customer_id',
                'tickets.created_at', 'ticket_customers.realname');
        $query->orderBy('tickets.created_at', 'desc');
        $data = $query->get();

        return $data;
    }

    public static function getOutlet($data)
    {
        $qry = TypeOutlet::where('name', $data)->first();

        return $qry;
    }

    public static function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'tahun',
            'm' => 'bulan',
            'w' => 'minggu',
            'd' => 'hari',
            'h' => 'jam',
            'i' => 'menit',
            's' => 'detik',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                // $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                $v = $diff->$k . ' ' . $v;
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' yang lalu' : 'just now';
    }

    public static function time_elapsed_print($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'tahun',
            'm' => 'bulan',
            'w' => 'minggu',
            'd' => 'hari',
            'h' => 'jam',
            'i' => 'menit',
            's' => 'detik',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                // $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                $v = $diff->$k . ' ' . $v;
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . '' : '.';
    }

    public static function dmy($data) {
        return date_format($data,"d-m-Y");
    }

    public static function cleanCode($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

    public static function getBulanName()
    {
        $bulan = [
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember'
        ];

        return $bulan;
    }

    public static function fakePrefixCust($data)
    {
        $maxCode  = Customer::max('code');
        if (empty($maxCode)) {
            $NextCode = 1;
        } else {
            $NextCode = substr($maxCode, 5) + 1;
        }
        // dd($NextCode);
        $prefix         = Setting::where('name', $data)->first();
        // $invoiceNumber  = sprintf("%07s", $NextCode);

        // return $prefix->prefix . Carbon::now()->format('m') . "/" . Carbon::now()->format('y') . "/" . $invoiceNumber;
        return $prefix->prefix.$NextCode;

    }

    public static function prefixCust($data)
    {
        $maxCode = Customer::whereRaw("no_pelanggan like '".$data."%'")
                ->whereRaw("year(created_at) = '".date('Y')."'")
                ->max('no_pelanggan');
        if(empty($maxCode)) {
            $nexCode = 1;
        } else {
            $nexCode = substr($maxCode, 7) + 1;
        }
        $prefix = str_pad((int) $nexCode, 7, '0', STR_PAD_LEFT);

        return $data.'-'.$prefix;

    }

    public static function generateNoRef($data)
    {
        $maxCode = Ticket::selectRaw("max(right(no_ref, 5)) as max")
            ->where('ticket_category', $data->ticket_category)
			->whereYear('created_at', date("Y"))
            ->first();

        if(empty($maxCode->max)) {
            $nexCode = 0;
        } else {
            $nexCode = substr($maxCode->max, -5, 5);
        }

        $yy = Carbon::now();
        $yy->isoFormat('YY');
        $prefix = Setting::where('name', $data->ticket_category)->first();
        $result = str_pad((int) $nexCode +1, 5, '0', STR_PAD_LEFT);

        return $yy->isoFormat('YY')."/".$prefix->prefix."/".$result;
    }

    public static function getName($data)
    {
        $result = DB::table('users')->where('id', $data)->first();

        return $result;
    }

    public static function getCust($data)
    {
        $result = DB::table('customers')->where('id', $data)->first();

        return $result;
    }
}

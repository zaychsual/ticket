<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogDetailTicket extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'ticket_id',
        'detail_ticket_id',
        'description',
        'status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketPhoneLog extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'ticket_id',
        'phone_no',
        'agent_number',
        'call_type',
        'campaign_id',
        'call_id',
        'remote_channel',
        'created_by',
        'created_at',
        'updated_at',
    ];
}

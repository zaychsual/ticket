<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class BaseModel extends \Illuminate\Database\Eloquent\Model
{
    // use HasFactory;
    public const Deleted = 1;
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $user              = Auth::user();
            $model->created_by = $user->id ?? '8924c6fa-3065-4541-8609-8bc1b86a1349';
            $model->created_at = Carbon::now();
            $model->updated_at = Carbon::now();
        });
        static::updating(function ($model) {
            $user              = Auth::user();
            $model->updated_by = $user->id ?? '8924c6fa-3065-4541-8609-8bc1b86a1349';
            $model->updated_at = Carbon::now();
        });
    }
}

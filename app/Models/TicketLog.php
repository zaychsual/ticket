<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketLog extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'ticket_id',
        'customer_id',
        'complain_id',
        'main_status',
        'sub_status',
        'realname',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];
}

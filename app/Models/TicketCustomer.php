<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketCustomer extends UUIDModel
{
    use HasFactory, SoftDeletes;

    public $incrementing = true;
    protected $keyType = 'string';
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = "id";

    protected $fillable = [
        'ticket_id',
        'branch_code',
        'no_pelanggan',
        'outlet_name',
        'realname',
        'alamat',
        'phone_no',
        'salesname',
        'spv_sales',
        'type_outlet_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function getOutlet()
    {
        return $this->belongsTo(\App\Models\TypeOutlet::class, 'type_outlet_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid as Generator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Hash;
class User extends Authenticatable
{
    use SoftDeletes, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'branch_code',
        'role_id',
        'deleted_at',
        'email_bom',
        'email_admin',
    ];

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->id = strtoupper(Generator::uuid4()->toString());
            $user              = Auth::user();
            $model->created_by = $user->id ?? '8924c6fa-3065-4541-8609-8bc1b86a1349';
            $model->created_at = Carbon::now();
            $model->updated_at = Carbon::now();
        });
        static::updating(function ($model) {
            $user              = Auth::user();
            $model->updated_by = $user->id ?? '8924c6fa-3065-4541-8609-8bc1b86a1349';
            $model->updated_at = Carbon::now();
        });
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    public function roles()
    {
        return $this->belongsToMany(\App\Models\Role::class);
    }

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getBranch()
    {
        return $this->belongsTo(\App\Models\Branch::class, 'branch_code', 'code');
    }

    public function getRole()
    {
        return $this->belongsTo(\App\Models\Role::class, 'role_id');
    }
}

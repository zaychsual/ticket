<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends UUIDModel
{
    use HasFactory, SoftDeletes;

    public const CATEGORY = [
        'FRM-CSD-007'  => 'Tiket Komplain',
        'FRM-CSD-019'  => 'Tiket Non Komplain'
    ];

    public const Complain = 'FRM-CSD-007';
    public const NonComplain = 'FRM-CSD-019';

    public const status = [
        300 => 'New Ticket',
        102 => 'Progress',
        103 => 'To be Revised',
        200 => 'Closed',
    ];
    public const TypeStatus = [
        300 => 'New Ticket',
        102 => 'Progress',
        103 => 'To be Revised',
        200 => 'Closed',
    ];

    public const NewTicket = 300;
    public const Progress = 102;
    public const ToBeRevised = 103;
    public const Closed = 200;

    public $incrementing = true;
    protected $keyType = 'string';
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = "id";

    protected $fillable = [
        'no_ref',
        'no_ticket',
        'nama_penelpon',
        'no_telp',
        'customer_id',
        'description',
        'complain_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at',
        'branch_code',
        'jam_komplain',
        'ticket_category',
        'main_status_id',
        'hasil_investigasi',
        'tindak_pencegahan',
        'notes',
        'is_submit',
        'file',
        'analisa_masalah',
    ];

    public function getCust()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function details()
    {
        return $this->belongsTo(\App\Models\TicketCustomer::class, 'id', 'ticket_id');
    }

    public function getComplain()
    {
        return $this->belongsTo(\App\Models\VariableComplain::class, 'complain_id');
    }

    public function getBranch()
    {
        return $this->belongsTo(\App\Models\Branch::class, 'branch_code', 'code');
    }
}

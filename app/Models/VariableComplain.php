<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;;

class VariableComplain extends UUIDModel
{
    use HasFactory, SoftDeletes;

    public const NonComplain = 1;
    public const Globals = 0;
    public const Complain = 2;
    public const status = [
        0 => 'Global',
        1 => 'Non Complain',
        2 => 'Complain',
    ];
    public $incrementing = true;
    protected $keyType = 'string';
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = "id";

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    protected $fillable = [
        'name',
        'category',
        'is_item',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}

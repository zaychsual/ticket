<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Customer extends UUIDModel
{
    use HasFactory, SoftDeletes;

    const type_outlet = [
        'AP','BB','BC','BG','BS','BU','CF','CS','DC','ES'
        ,'EX','FF','FS','FT','GH','GK','GM','GR','HB','HM'
        ,'HR','HS','HT','IS','KA','KI','KK','KR','KS','KT'
        ,'KU','LR','MM','MR','NC','OR','OT','PB','PK','PT'
        ,'RD','RK','RT','SE','SG','SK','SL','SM','SN','SP'
        ,'SR','TD','WT','N/A'
    ];

    public $incrementing = true;
    protected $keyType = 'string';
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = "id";

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    protected $fillable = [
        'id',
        'no_pelanggan',
        'nik',
        'realname',
        'outlet_name',
        'type_outlet_id',
        'phone_no',
        'kelurahan_id',
        'kecamatan_id',
        'kabupaten_id',
        'provinsi_id',
        'alamat',
        'kode_sales',
        'salesname',
        'spv_sales',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function getOutlet()
    {
        return $this->belongsTo(\App\Models\TypeOutlet::class, 'type_outlet_id');
    }
}

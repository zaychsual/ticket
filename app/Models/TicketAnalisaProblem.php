<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class TicketAnalisaProblem extends UUIDModel
{
    use HasFactory;

    public $incrementing = true;
    protected $keyType = 'string';
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = "id";

    protected $fillable = [
        'ticket_id',
        'analisa_masalah',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at',
        'trx_date'
    ];
}

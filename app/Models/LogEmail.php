<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid as Generator;
use Carbon\Carbon;

class LogEmail extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'email',
        'email_cc',
        'subject',
        'title',
        'content',
        'branch',
        'status',
        'response',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->id         = strtoupper(Generator::uuid4()->toString());
            $model->created_at = Carbon::now();
            $model->updated_at = Carbon::now();
        });
        static::updating(function ($model) {
            $model->updated_at = Carbon::now();
        });
    }
}

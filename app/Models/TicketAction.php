<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketAction extends UUIDModel
{
    use HasFactory, SoftDeletes;

    public const status = [
        102 => 'Progress',
        200 => 'Done',
    ];

    public $incrementing = true;
    protected $keyType = 'string';
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = "id";

    protected $fillable = [
        'ticket_id',
        'cs_action',
        'cs_status',
        'bm_action',
        'bm_status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at',
        'trx_date'
    ];
}

<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Models\Branch;
use App\Models\TypeOutlet;
use App\Helpers\SiteHelpers;
use DataTables;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Customer::latest()->when(request()->q, function($data) {
            $data = $data->orWhere('outlet_name', 'like', '%'.request()->q.'%')
                    ->orWhere('no_pelanggan', 'like', '%'.request()->q.'%');
        })->paginate(10);

        return view('pages.cust.index', compact('data'));
    }

    public function custAjax()
    {
        ini_set('memory_limit', '-1');
        $data = Customer::orderBy('realname')->get();

        return DataTables::of($data)
            ->editColumn("no_pelanggan", function($data) {
                return $data->no_pelanggan;
            })
            ->editColumn("realname", function($data) {
                return $data->realname;
            })
            ->editColumn("outlet_name", function($data) {
                return $data->outlet_name;
            })
            ->editColumn("phoneno", function($data) {
                return $data->phone_no;
            })
            ->editColumn("createdby", function($data) {
                return SiteHelpers::getName($data->created_by)->name ?? '' ;
            })
            ->editColumn("updatedby", function($data) {
                return SiteHelpers::getName($data->updated_by)->name ?? '' ;
            })
            ->addColumn("ID", function($data) {
                // if(Gate::allows('transaction_show')) {
                $btn = '<a class="badge badge-info show-index" id="show_'.$data->id.'" data-id="'. $data->id .'" href="#">
                            <i class="fa fa-eye"></i> Show
                        </a> &nbsp;';
                $btn = $btn.'<a class="badge badge-warning edit-index" data-name="update" id="edit_'.$data->id.'" data-id="'. $data->id .'" href="#">
                                <i class="fa fa-edit"></i> Edit
                            </a> &nbsp;';
                $btn = $btn.'<a class="badge del-index badge-danger" data-name="'.$data->realname.'" data-id="'.$data->id.'" data-token="'.csrf_token().'" href="#">
                                <i class="fa fa-trash"></i> Delete
                            </a>';

                return $btn;
                // }
            })
            ->rawColumns(['ID'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branch = Branch::select('name','code', 'id')->get();
        $type_outlet = TypeOutlet::all()->pluck('name', 'id');

        return view('pages.cust.create', compact('branch', 'type_outlet'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->_validation($request);
            $branch = Branch::FindOrFail($request->branch_id);
            $store = Customer::create($request->all());
            DB::commit();

            return response()->json(['success' => '1']);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        $type_outlet = TypeOutlet::all()->pluck('name', 'id');

        return view('pages.cust.show', compact('customer', 'type_outlet'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        $type_outlet = TypeOutlet::all()->pluck('name', 'id');

        return view('pages.cust.edit', compact('customer', 'type_outlet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $this->_validation($request);
            // $this->_validation($request);
            $update = Customer::findOrFail($request->id);
            $update->realname = $request->realname;
            $update->outlet_name = $request->outlet_name;
            $update->spv_sales = $request->spv_sales;
            $update->salesname = $request->salesname;
            $update->type_outlet_id = $request->type_outlet_id;
            $update->alamat = $request->alamat;
            $update->update();

            DB::commit();

            return response()->json(['success' => '1']);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $del = Customer::findOrFail($id);
            $del->updated_by = Auth::user()->id;
            $del->deleted_at = Carbon::now();
            $del->update();

            DB::commit();


            return back();
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    private function _validation(Request $request)
    {
        $validation = $request->validate(
            [
                'no_pelanggan' => 'required',
                'realname' => 'required|min:3',
                'outlet_name' => 'required|min:3',
                'phone_no' => 'required|min:3|numeric',
                'alamat' => 'required',
            ],
            [
                'no_pelanggan.required' => 'Cannot be null!',
                'realname.required' => 'Cannot be null!',
                'realname.min' => 'Minimal 3 digit',
                'outlet_name.required' => 'Cannot be null!',
                'outlet_name.min' => 'Minimal 3 digit',
                'phone_no.required' => 'Cannot be null!',
                'phone_no.min' => 'Minimal 3 digit',
                'phone_no.numeric' => 'Must Number',
                'alamat.required' => 'Cannot be null!',
            ]
        );

    }
}

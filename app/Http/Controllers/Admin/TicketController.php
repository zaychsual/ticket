<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid as Generator;
use Illuminate\Support\Facades\Input;
use App\Models\Ticket;
use App\Models\TicketLog;
use App\Models\TicketAction;
use App\Models\TicketComplainDetail;
use App\Models\TicketAnalisaProblem;
use App\Models\TicketCustomer;
use App\Models\TicketPhoneLog;
use App\Models\Customer;
use App\Models\Branch;
use App\Models\User;
use App\Models\TypeOutlet;
use App\Models\VariableComplain;
use App\Helpers\SiteHelpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use App\Models\File;
use Carbon\Carbon;
use App\Mail\TryEmail;
use Excel;
use App\Exports\exportTicket;
use DataTables;

class TicketController extends Controller
{
    const status = [
        300 => [
            'progress' => 'New Ticket',
            'badge' => 'danger'
        ],
        102 => [
            'progress' => 'Progress',
            'badge' => 'warning'
        ],
        103 => [
            'progress' => 'To be Revised',
            'badge' => 'warning'
        ],
        200 => [
            'progress' => 'Closed',
            'badge' => 'success'
        ]
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.ticket.index');
    }

    public function ticketAjax()
    {
        // $query = Ticket::leftJoin('ticket_customers', 'ticket_customers.ticket_id', '=', 'tickets.id')
        //         ->select('tickets.id', 'tickets.no_ref', 'tickets.ticket_category', 'tickets.nama_penelpon', 'tickets.customer_id',
        //                 'complain_id', 'tickets.main_status_id', 'tickets.created_by', 'tickets.created_at', 'ticket_customers.outlet_name');
        if(SiteHelpers::cekRole(3)) {
            $data = Ticket::whereIn('tickets.branch_code', explode(",", Auth::user()->branch_code ?? ''));
        } elseif(SiteHelpers::cekRole(2)) {
            $data = Ticket::where('tickets.created_by', Auth::user()->id);
        } else {
            $data = Ticket::select('*');
        }

        return DataTables::of($data)
        // return DataTables::queryBuilder($data)
            ->addIndexColumn()
            ->editColumn("no_ref", function($data) {
                return $data->no_ref;
            })
            ->editColumn("ticket_category", function($data) {
                return Ticket::CATEGORY[$data->ticket_category];
            })
            ->editColumn("nama_penelpon", function($data) {
                return $data->nama_penelpon;
            })
            ->editColumn("customer_id", function($data) {
                return $data->getCust->outlet_name ?? $data->details->outlet_name;
            })
            ->editColumn("branch", function($data) {
                return $data->getBranch->name ?? '';
            })
            ->editColumn("complain_id", function($data) {
                return $data->getComplain['name'];
            })
            ->editColumn("main_status_id", function($data) {
                $badge = '<div class="badge badge-'.self::status[$data->main_status_id]['badge'].'">
                            <i class="fas fa-ticket-alt"></i>'.Ticket::TypeStatus[$data->main_status_id].'
                        </div>';

                return $badge;
            })
            ->editColumn("createdby", function($data) {
                return SiteHelpers::getName($data->created_by)->name ?? '' ;
            })
            ->editColumn("created_at", function($data) {
                return SiteHelpers::dmy($data->created_at);
            })
            ->addColumn("ID", function($data) {
                // if(Gate::allows('transaction_show')) {
                $xls = route('admin.ticket.xls',$data->id);
                $pdf = route('admin.ticket.print',$data->id);
                $action = route('admin.ticket.action', $data->id);

                $btn = '';
                if (SiteHelpers::cekAkses('Tiket','edit') && $data->main_status_id != Ticket::Closed) {
                    if(SiteHelpers::cekRole(3)) {
                        if($data->updated_by != Auth::user()->id) {
                            $btn = $btn.'<a class="badge badge-warning action-index" data-name="update" id="edit_'.$data->id.'" data-id="'. $data->id .'" href="'.$action.'">
                                    <i class="fa fa-edit"></i> Reply
                                </a> &nbsp;';
                        }
                    } else {
                        $btn = $btn.'<a class="badge badge-warning edit-index" data-name="update" id="edit_'.$data->id.'" data-id="'. $data->id .'" href="#">
                                    <i class="fa fa-edit"></i> Edit
                                </a> &nbsp;';
                    }
                }
                if (SiteHelpers::cekAkses('Tiket','destroy')) {
                    $btn = $btn.'<a class="badge del-index badge-danger" data-name="'.$data->code.'" data-id="'.$data->id.'" data-token="'.csrf_token().'" href="#">
                                <i class="fa fa-trash"></i> Delete
                            </a> &nbsp;';
                }
                if (SiteHelpers::cekAkses('Tiket','akses')) {
                    $btn = $btn.'<a class="badge badge-info show-index" id="show_'.$data->id.'" data-id="'. $data->id .'" href="#">
                                    <i class="fa fa-eye"></i> Show
                                </a> &nbsp;';
                    if($data->main_status_id == Ticket::Closed) {
                        $btn = $btn.'<div class="dropdown d-inline">
                                    <a class="badge badge-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Print
                                    </a>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 29px, 0px);">
                                        <a class="dropdown-item has-icon" href="'.$xls.'" target="_blank"><i class="fas fa-file-excel"></i>xls</a>
                                        <a class="dropdown-item has-icon" href="'.$pdf.'" target="_blank"><i class="fas fa-file-pdf"></i>pdf</a>
                                    </div>
                                </div>';
                    }
                }


                return $btn;
            })
            ->rawColumns(['ID', 'main_status_id'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function ticketOption($id)
    {
        // $cust = Customer::select('realname','no_pelanggan', 'id')->get();
        $qry = VariableComplain::select('id', 'name');
        if($id == 11) {
            $qry->whereIn('category', [VariableComplain::Globals, VariableComplain::Complain]);
        } else {
            $qry->whereIn('category', [VariableComplain::Globals, VariableComplain::NonComplain]);
        }
        $complain = $qry->get();
        $branch = Branch::select('name','code', 'id')->get();
        $outlet = TypeOutlet::all()->pluck('name', 'id');
        // dd($cust);
        if($id == 11) {
            return view('pages.ticket.create-complain', compact('branch', 'complain'));
        } else {
            return view('pages.ticket.create-noncomplain', compact('branch', 'complain', 'outlet'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function _validation(Request $request)
    {
        $validation = $request->validate(
            [
                'nama_penelpon' => 'required',
                'no_telp' => 'required',
                'customer_id' => 'required',
                'complain_id' => 'required',
                'description' => 'required',
            ],
            [
                'nama_penelpon.required' => 'Cannot be null!',
                'no_telp.required' => 'Cannot be null!',
                'customer_id.required' => 'Cannot be null!',
                'complain_id.required' => 'Cannot be null!',
                'description.required' => 'Cannot be null!',
            ]
        );

    }

    public function tiketPhone($id,$agent,$call,$campaign,$callid,$remote)
    {
        $main = Customer::where('phone_no', $id)->first();
        // $cust = Customer::select('realname','no_pelanggan', 'id')->get();
        $dtCust = Customer::leftJoin('branches', DB::Raw("left(customers.no_pelanggan, 4)"), '=', 'branches.code')
                ->join('type_outlets', 'customers.type_outlet_id', '=', 'type_outlets.id')
                ->where('customers.phone_no', $id)
                ->select('branches.code' ,'branches.name as branch', 'type_outlets.name as type_outlet', 'branches.code as branch_code', 'customers.*')
                ->first();
        $qryComplain = VariableComplain::whereIn('category', [VariableComplain::Globals, VariableComplain::Complain])
                    ->select('id', 'name')->get();
        $qryNonComplain = VariableComplain::whereIn('category', [VariableComplain::Globals, VariableComplain::NonComplain])
                    ->select('id', 'name')->get();
        $branch = Branch::select('name','code', 'id')->get();
        $outlet = TypeOutlet::all()->pluck('name', 'id');
        // dd($credential);
        return view('pages.ticket.create-phone', compact('id','main',
                    'dtCust','branch', 'outlet', 'qryComplain', 'qryNonComplain',
                    'agent','call','campaign','callid','remote'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            if($request->ticket_category == Ticket::Complain) {
                $this->_validation($request);
            }
            $user = Auth::user();
            $store_id = strtoupper(Generator::uuid4()->toString());
            $no_reff = SiteHelpers::generateNoRef($request);

            $store = Ticket::insertGetId([
                'id'         => $store_id,
                'created_by' => $user->id ?? '8924c6fa-3065-4541-8609-8bc1b86a1349',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'no_ref' => $no_reff,
                'ticket_category' => $request->ticket_category,
                'jam_komplain' => $request->trx_time ?? Carbon::now()->format('H:i'),
                'nama_penelpon' => $request->nama_penelpon ?? '',
                'no_telp' => $request->no_telp,
                'branch_code' => $request->branch_code,
                'customer_id' => $request->customer_id ?? '',
                'complain_id' => $request->complain_id,
                'description' => $request->description ?? '',
            ]);

            if($request->ticket_category == Ticket::NonComplain) {
                $cs = TicketCustomer::create([
                    'ticket_id' => $store_id,
                    'branch_code' => $request->branch_code,
                    'no_pelanggan' => $request->branch_code.'-'.$request->no_pelanggan ?? '',
                    'outlet_name' => $request->outlet_name ?? '',
                    'realname' => $request->realname ?? '',
                    'alamat' => $request->alamat ?? '',
                    'phone_no' => $request->phone_no ?? '',
                    'salesname' => $request->sales_name ?? '',
                    'spv_sales' => $request->spv_sales ?? '',
                    'type_outlet_id' => $request->type_outlet_id ?? '',
                ]);
            }

            if($request->has('item')) {
                foreach($request->item as $key => $dt) {
                    $dt = TicketComplainDetail::create([
                        'ticket_id' => $store_id,
                        'item'  => $request->item[$key],
                        'qty' => $request->order[$key]
                    ]);
                }
            }

            if($request->masalah != '' || !empty($request->masalah)) {
                $action = TicketAction::create([
                    'ticket_id' => $store_id,
                    'cs_action'    => $request->masalah ?? '',
                    'trx_date'  => $request->problem_date ?? Carbon::now()->format('Y-m-d'),
                    'cs_status'=> $request->sub_status ?? 102
                ]);
            }

            $log = TicketLog::create([
                'ticket_id'  => $store_id,
                'customer_id' => $request->customer_id ?? '',
                'realname' => $request->realname ?? '',
                'complain_id' => $request->complain_id,
                'main_status' => 300,
            ]);

            if($request->no_telp != '') {
                $phonelog = TicketPhoneLog::create([
                    'ticket_id' => $store_id,
                    'phone_no' => $request->no_telp,
                    'agent_number' => $request->agent,
                    'call_type' => $request->call,
                    'campaign_id' => $request->campaign,
                    'call_id' => $request->callid,
                    'remote_channel' => $request->remote
                ]);
            }


            if($request->ticket_category == Ticket::Complain) {
                $dataCust = Customer::findOrFail($request->customer_id);
                $subject = 'Komplain';
                $branchName = SiteHelpers::getBranchName($request->branch_code)->name ?? '';
                $cust_id = $request->customer_id;
                $outlet_name = $request->outlet_name;
                $desc = $request->description;
                $outlet_name = $dataCust->outlet_name;
                $email = SiteHelpers::templateEmail($store_id);
            } else {
                $subject = 'Non Komplain';
                $branchName = SiteHelpers::getBranchName($request->branch_code)->name ?? '';
                $cust_id = $request->realname;
                $desc = $request->description;
                $outlet_name = $request->outlet_name;
                $alamat = $request->alamat;
                $email = SiteHelpers::templateEmail($store_id);
            }

            $details = [
                'subject' => 'Tiket '.$subject.' '.$outlet_name,
                'title' => 'Tiket '.$subject.' '.$outlet_name,
                'body'  => $email,
                'branch' => $branchName,
            ];

            DB::commit();
            $mainSender = trim(SiteHelpers::getBranchUser($request->branch_code)->email);
            $emails = SiteHelpers::getAllCC($request->branch_code);
            $ccEmails = array_map('trim', $emails);
            \Mail::to($mainSender ?? 'no@email.com')
                ->cc($ccEmails)
                ->send(new TryEmail($details));

            return redirect()->route('admin.branch.index');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Ticket::findOrFail($id);
        // $cust = Customer::select('realname','no_pelanggan', 'id')->get();
        if($data->ticket_category == Ticket::Complain) { //complain
            $dataCust = Customer::join('type_outlets', 'type_outlets.id', '=', 'customers.type_outlet_id')
                ->select('customers.*', 'type_outlets.name as outlet')
                ->findOrFail($data->customer_id);
        } elseif ($data->ticket_category == Ticket::NonComplain) { //noncomplain
            $dataCust = TicketCustomer::leftJoin('type_outlets', 'type_outlets.id', '=', 'ticket_customers.type_outlet_id')
                ->select('ticket_customers.*', 'type_outlets.name as outlet')
                ->where('ticket_id', $id)->first();
        }
        $outlet = TypeOutlet::all()->pluck('name', 'id');
        $dataBranch = Branch::where('code', $data->branch_code)->first();
        $branch =  Branch::select('id','code', 'name')->get();
        $qry = VariableComplain::select('id', 'name');
        if($data->ticket_category == Ticket::Complain) {
            $qry->whereIn('category', [VariableComplain::Globals, VariableComplain::Complain]);
        } else {
            $qry->whereIn('category', [VariableComplain::Globals, VariableComplain::NonComplain]);
        }
        $complain = $qry->get();
        $dt = TicketComplainDetail::where('ticket_id', $id)->get();
        $action = TicketAction::where('ticket_id', $id)->orderBy('created_at', 'asc')->get();
        $analisa = TicketAnalisaProblem::where('ticket_id', $id)->get();

        return view('pages.ticket.show', compact('data', 'branch', 'dataBranch', 'dataCust', 'complain', 'dt', 'action', 'branch', 'outlet', 'analisa'));
    }

    public function print($id)
    {
        $data = Ticket::findOrFail($id);
        // $cust = Customer::select('realname','no_pelanggan', 'id')->get();

        if($data->ticket_category == Ticket::Complain) { //complain
            $dataCust = Customer::findOrFail($data->customer_id);
            $dataBranch = Branch::where('code', $data->branch_code)->first();
            $variable = VariableComplain::whereIn('category', [VariableComplain::Globals, VariableComplain::Complain])
                    ->select('id', 'name')->get();
        } elseif ($data->ticket_category == Ticket::NonComplain) { //noncomplain
            $dataCust = TicketCustomer::where('ticket_id', $id)->first();
            $branch =  Branch::select('id','code', 'name')->get();
            $outlet = TypeOutlet::all()->pluck('name', 'id');
            $complain = VariableComplain::all()->pluck('name', 'id');
        }
        $dt = TicketComplainDetail::where('ticket_id', $id)->get();
        $action = TicketAction::where('ticket_id', $id)->orderBy('created_at', 'asc')->get();
        $analisa = TicketAnalisaProblem::where('ticket_id', $id)->get();
        // dd($data);
        if($data->ticket_category == Ticket::NonComplain) {
            return view('pages.ticket.print', compact('data', 'branch', 'dataCust', 'complain','analisa', 'dt', 'action', 'outlet'));
        } else {
            return view('pages.ticket.print-complain', compact('data', 'dataCust', 'variable','analisa', 'dt', 'action','dataBranch'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Ticket::findOrFail($id);

        if($data->ticket_category == Ticket::Complain) { //complain
            $dataCust = Customer::join('type_outlets', 'type_outlets.id', '=', 'customers.type_outlet_id')
                ->select('customers.*', 'type_outlets.name as outlet')
                ->findOrFail($data->customer_id);
            $dataBranch = Branch::where('code', $data->branch_code)->first();
        } elseif ($data->ticket_category == Ticket::NonComplain) { //noncomplain
            $dataCust = TicketCustomer::leftJoin('type_outlets', 'type_outlets.id', '=', 'ticket_customers.type_outlet_id')
                ->select('ticket_customers.*', 'type_outlets.name as outlet')
                ->where('ticket_id', $id)->first();
            $branch =  Branch::select('id','code', 'name')->get();
            $outlet = TypeOutlet::all()->pluck('name', 'id');
        }
        $qry = VariableComplain::select('id', 'name');
        if($data->ticket_category == Ticket::Complain) {
            $qry->whereIn('category', [VariableComplain::Globals, VariableComplain::Complain]);
        } else {
            $qry->whereIn('category', [VariableComplain::Globals, VariableComplain::NonComplain]);
        }
        $complain = $qry->get();
        $dt = TicketComplainDetail::where('ticket_id', $id)->get();
        $action = TicketAction::where('ticket_id', $id)->orderBy('created_at', 'asc')->get();
        $analisa = TicketAnalisaProblem::where('ticket_id', $id)->get();

        if (SiteHelpers::cekRole(3) && $data->ticket_category == Ticket::Complain) {
            return view('pages.ticket.edit-branch', compact('data','dataBranch', 'dataCust', 'complain','analisa', 'dt', 'action'));
        } elseif (SiteHelpers::cekRole(3) && $data->ticket_category == Ticket::NonComplain) {
            return view('pages.ticket.edit-branch-non-complain', compact('data','branch', 'dataCust', 'complain','analisa', 'dt', 'action', 'outlet'));
        } elseif ($data->ticket_category == Ticket::Complain) {
            return view('pages.ticket.edit', compact('data','dataBranch', 'dataCust', 'complain','analisa', 'dt', 'action'));
        } elseif ($data->ticket_category == Ticket::NonComplain) {
            return view('pages.ticket.edit-non-complain', compact('data','branch', 'dataCust', 'complain','analisa', 'dt', 'action', 'outlet'));
        }
    }

    public function action($id)
    {
        $data = Ticket::findOrFail($id);
        if(SiteHelpers::cekRole(3) && $data->updated_by == Auth::user()->id || $data->main_status_id == Ticket::Closed) {
            return redirect()->route('admin.ticket.index');
        }
        // $cust = Customer::select('realname','no_pelanggan', 'id')->get();
        $complain = VariableComplain::all()->pluck('name', 'id');
        if($data->ticket_category == Ticket::Complain) { //complain
            $dataCust = Customer::join('type_outlets', 'type_outlets.id', '=', 'customers.type_outlet_id')
                ->select('customers.*', 'type_outlets.name as outlet')
                ->findOrFail($data->customer_id);
            $dataBranch = Branch::where('code', $data->branch_code)->first();
        } elseif ($data->ticket_category == Ticket::NonComplain) { //noncomplain
            $dataCust = TicketCustomer::leftJoin('type_outlets', 'type_outlets.id', '=', 'ticket_customers.type_outlet_id')
                ->select('ticket_customers.*', 'type_outlets.name as outlet')
                ->where('ticket_id', $id)->first();
            $branch =  Branch::select('id','code', 'name')->get();
            $outlet = TypeOutlet::all()->pluck('name', 'id');
        }
        $dt = TicketComplainDetail::where('ticket_id', $id)->get();
        $action = TicketAction::where('ticket_id', $id)->orderBy('created_at', 'asc')->get();
        $analisa = TicketAnalisaProblem::where('ticket_id', $id)->get();

        if (SiteHelpers::cekRole(3) && $data->ticket_category == Ticket::Complain) {
            return view('pages.ticket.action-branch', compact('data', 'dataBranch', 'dataCust', 'complain','analisa', 'dt', 'action'));
        }elseif (SiteHelpers::cekRole(3) && $data->ticket_category == Ticket::NonComplain) {
            return view('pages.ticket.action-branch-non-complain', compact('data', 'dataCust', 'complain','analisa', 'dt', 'action', 'branch', 'outlet'));
        } elseif($data->ticket_category == Ticket::Complain) {
            return view('pages.ticket.action-agent', compact('data', 'dataBranch', 'dataCust', 'complain','analisa', 'dt', 'action'));
        } elseif($data->ticket_category == Ticket::NonComplain) {
            return view('pages.ticket.action-agent-non-complain', compact('data', 'dataCust', 'complain','analisa', 'dt', 'action', 'branch', 'outlet'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            //Realisasi Tindak Lanjut
            if($request->has('cs_status')) {
                foreach($request->cs_status as $key => $action) {
                    if(isset($request->action_id[$key])) {
                        $action = TicketAction::findOrFail($request->action_id[$key]);
                        $action->cs_action = $request->cs_action[$key] ?? '';
                        $action->bm_action = $request->bm_action[$key] ?? '';
                        $action->cs_status = $request->cs_status[$key] ?? 102;
                        $action->bm_status = $request->bm_status[$key] ?? 102;
                        $action->trx_date = $request->action_date[$key] ?? Carbon::now()->format('Y-m-d');
                        if($request->is_delete[$key] == 1) {
                            $action->deleted_at = Carbon::now();
                        }
                        $action->update();
                    } else {
                        $action = TicketAction::create([
                            'ticket_id' => $id,
                            'cs_action'    => $request->cs_action[$key] ?? '',
                            'bm_action'    => $request->bm_action[$key] ?? '',
                            'cs_status'    => $request->cs_status[$key] ?? 102,
                            'bm_status'    => $request->bm_status[$key] ?? 102,
                            'trx_date'  => $request->problem_date[$key] ?? Carbon::now()->format('Y-m-d'),
                        ]);
                    }
                }
            }
            //ticket customer
            if($request->cs_id) {
                $cs = TicketCustomer::findOrFail($request->cs_id);
                $cs->ticket_id = $id;
                $cs->branch_code = $request->branch_code;
                $cs->no_pelanggan = $request->branch_code.'-'.$request->no_pelanggan ?? '';
                $cs->outlet_name = $request->outlet_name ?? '';
                $cs->realname = $request->realname ?? '';
                $cs->alamat = $request->alamat ?? '';
                $cs->phone_no = $request->phone_no ?? '';
                $cs->salesname = $request->sales_name ?? '';
                $cs->spv_sales = $request->spv_sales ?? '';
                $cs->type_outlet_id = $request->type_outlet_id ?? '';
                $cs->update();
            }

            if($request->has('item')) {
                foreach($request->item as $key => $dt) {
                    if(isset($request->ticket_complain_detail_id[$key])) {
                        $dt = TicketComplainDetail::findOrFail($request->ticket_complain_detail_id[$key]);
                        $dt->item = $request->item[$key];
                        $dt->qty = $request->order[$key];
                        if($request->is_delete[$key] == 1) {
                            $dt->deleted_at = Carbon::now();
                        }
                        $dt->update();
                    } else {
                        $dt = TicketComplainDetail::create([
                            'ticket_id' => $id,
                            'item'  => $request->item[$key],
                            'qty' => $request->order[$key]
                        ]);
                    }
                }
            }

            if($request->file('file')) {
                $file = $request->file('file');
                $filename = time().$file->getClientOriginalName();
                $file->move(public_path().'/uploads/', $filename);
            }

            if(SiteHelpers::cekRole(3)) {
                $tiket = Ticket::findOrFail($id);
                $tiket->hasil_investigasi = $request->result_action ?? '';
                $tiket->tindak_pencegahan = $request->action_pencegahan ?? '';
                // $tiket->analisa_masalah = $request->analisa_masalah ?? '';
                $tiket->is_submit = $request->is_submit_bm ?? 102;
                $tiket->main_status_id = Ticket::Progress;
                if($request->file('file')) {
                    $tiket->file = $filename;
                }
                $tiket->update();

                if($request->has('analisa_masalah')) {
                    TicketAnalisaProblem::where('ticket_id', $id)->delete();
                    foreach($request->analisa_masalah as $key => $am) {

                        $am = TicketAnalisaProblem::create([
                            'ticket_id' => $id,
                            'analisa_masalah'  => $request->analisa_masalah[$key],
                        ]);
                    }
                }

                $log = TicketLog::create([
                    'ticket_id'  => $id,
                    'customer_id' => $request->customer_id,
                    'complain_id' => $request->complain_id,
                    'main_status' => Ticket::Progress,
                ]);

            } else {
                $tiket = Ticket::findOrFail($id);
                $tiket->hasil_investigasi = $request->result_action ?? '';
                $tiket->tindak_pencegahan = $request->action_pencegahan ?? '';
                $tiket->notes = $request->notes ?? '';
                $tiket->main_status_id = $request->main_status_id ?? Ticket::NewTicket;
                $tiket->update();

                $log = TicketLog::create([
                    'ticket_id'  => $id,
                    'customer_id' => $request->customer_id,
                    'complain_id' => $request->complain_id,
                    'main_status' => $request->main_status_id ?? Ticket::NewTicket,
                ]);
            }
            if($request->main_status_id == Ticket::Closed) {
                $aksi = DB::table('ticket_actions')->where('ticket_id', $id)
                    ->update([
                        'bm_status' => Ticket::Closed,
                        'cs_status' => Ticket::Closed,
                    ]);
            }

            if($request->ticket_category == Ticket::Complain) {
                $subject = 'Komplain';
            } else {
                $subject = 'Non Komplain';
            }
            $details = [
                'subject' => $subject,
                'branch' => SiteHelpers::getBranchName($request->branch_code)->name ?? '',
                'title' => 'Email Update E-ticket '.$subject,
                'body'  => 'Tiket '.$subject.' telah di update. dengan nomor '.$tiket->no_ref.' silahkan login ke https://ticket.kalapatec.id, untuk menindak lanjuti',
            ];

            \Mail::to('aziz.zaind@yahoo.com')
            ->cc(['dina.novianti@fastratabuana.co.id', 'pelanggan@fastratabuana.co.id', 'novita.kusuma@fastratabuana.co.id'])
            ->send(new TryEmail($details));
            DB::commit();
            if(empty($request->is_ajax)) {
                return redirect()->route('admin.ticket.index');
            }
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            // dd($id);
            $del = Ticket::findOrFail($id);
            $del->updated_by = Auth::user()->id;
            $del->update();

            Ticket::findOrFail($id)->delete();
            DB::commit();


            return response()->json(array('success' => true));
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function xls($id)
    {
        $data = Ticket::findOrFail($id);


        if($data->ticket_category == Ticket::Complain) { //complain
            $dataCust = Customer::findOrFail($data->customer_id);
            $dataBranch = Branch::where('code', $data->branch_code)->first();
            $variable = VariableComplain::whereIn('category', [VariableComplain::Globals, VariableComplain::Complain])
                    ->select('id', 'name')->get();
        } elseif ($data->ticket_category == Ticket::NonComplain) { //noncomplain
            $dataCust = TicketCustomer::where('ticket_id', $id)->first();
            $branch =  Branch::select('id','code', 'name')->get();
            $outlet = TypeOutlet::all()->pluck('name', 'id');
            $complain = VariableComplain::all()->pluck('name', 'id');
        }
        $dt = TicketComplainDetail::where('ticket_id', $id)->get();
        $action = TicketAction::where('ticket_id', $id)->orderBy('created_at', 'asc')->get();
        $analisa = TicketAnalisaProblem::where('ticket_id', $id)->get();

        if($data->ticket_category == Ticket::Complain) {
            $this->xlsComplain($data, $dataCust, $variable,$analisa,$dt,$action,$dataBranch);
        } else {
            $this->xlsNonComplain($data, $branch, $dataCust, $complain,$analisa, $dt, $action, $outlet);
        }
    }

    public function xlsNonComplain($data, $branch, $dataCust, $complain,$analisa, $dt, $action, $outlet)
    {
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\HeaderFooterDrawing();
        $drawing->setName('logo');
        $drawing->setDescription('logo');
        $drawing->setPath(public_path().'/avatars/logo-fastrata.png');
        $drawing->setCoordinates('B1');
        $drawing->getShadow()->setVisible(true);
        $drawing->getShadow()->setDirection(45);
        $drawing->setWidth(90);                 //set width, height
        $drawing->setHeight(54);
        $tmplt = public_path().'/xls/noncomplain.xlsx';
        $spreadsheet = IOFactory::load($tmplt);
        $sheet = $spreadsheet->getActiveSheet();
        $drawing->setWorksheet($spreadsheet->getActiveSheet());
        $sheet->setCellValue('C5', SiteHelpers::getName($data->created_by)->name);
        $sheet->setCellValue('C7', $data->no_ref);
        $sheet->setCellValue('C8', SiteHelpers::dmy($data->created_at));
        $sheet->setCellValue('C9', $data->jam_komplain);
        $sheet->setCellValue('C10', $data->getBranch['name']);
        $sheet->setCellValue('C11', $dataCust->no_pelanggan);
        $sheet->setCellValue('C12', $dataCust->no_pelanggan);
        $sheet->setCellValue('C13', $data->nama_penelpon);
        $sheet->setCellValue('C14', $dataCust->outlet_name);
        $sheet->setCellValue('C15', $dataCust->getOutlet['name'] ?? '');
        $sheet->setCellValue('C16', $dataCust->realname);
        $sheet->setCellValue('C17', $dataCust->alamat);
        $sheet->setCellValue('C18', $data->no_telp);
        $sheet->setCellValue('C19', $dataCust->salesname);
        $sheet->setCellValue('C20', $dataCust->spv_sales);
        if(count($dt) > 0) {
            $item = 23;
            foreach ($dt as $key => $var) {
                $sheet->setCellValue('B'.$item, $var->item);
                $sheet->setCellValue('C'.$item, $var->qty);
            }
        }
        $sheet->setCellValue('C29', $data->description);
        $sheet->setCellValue('C33', $data->getComplain['name']);
        if (count($action) > 0) {
            $aksiTanggal = 35;
            $cs = 36;
            $bm = 37;
            foreach ($action as $key => $var) {
                $tglindo = date_create($var->trx_date);

                $sheet->setCellValue('C'.$aksiTanggal, date_format($tglindo,"d M Y"));
                $sheet->setCellValue('C'.$cs, "CS :".$var->cs_action ?? '');
                $sheet->setCellValue('C'.$bm, "BM :".$var->bm_action ?? '');
                $aksiTanggal+2;
                $cs+2;
                $bm+2;
            }
        }
        $sheet->setCellValue('C44', $data->hasil_investigasi);
        $sheet->setCellValue('C46', Ticket::TypeStatus[$data->main_status_id]);
        $sheet->setCellValue('C47', SiteHelpers::time_elapsed_print($data->created_at));
        $sheet->setCellValue('C50', SiteHelpers::getName($data->created_by)->name);

        $spreadsheet->getActiveSheet()->getStyle('E11')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B18')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B25')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B38')->getAlignment()->setWrapText(true);
        $writer = IOFactory::createWriter($spreadsheet, "Xlsx");
        $file_name = time() . '.xlsx';
        $writer->save($file_name);
        header('Content-Type: application/x-www-form-urlencoded');
        header('Content-Transfer-Encoding: utf-8');
        header("Content-disposition: attachment; filename=\"".$file_name."\"");
        readfile($file_name);
        unlink($file_name);
        exit;
    }

    public function xlsComplain($data, $dataCust, $variable,$analisa,$dt,$action,$dataBranch)
    {
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\HeaderFooterDrawing();
        $drawing->setName('logo');
        $drawing->setDescription('logo');
        $drawing->setPath(public_path().'/avatars/logo-fastrata.png');
        $drawing->setCoordinates('B2');
        $drawing->getShadow()->setVisible(true);
        $drawing->getShadow()->setDirection(45);
        $drawing->setWidth(90);                 //set width, height
        $drawing->setHeight(54);
        $tmplt = public_path().'/xls/complain.xlsx';
        $spreadsheet = IOFactory::load($tmplt);
        $sheet = $spreadsheet->getActiveSheet();
        $drawing->setWorksheet($spreadsheet->getActiveSheet());
        $sheet->setCellValue('P2', $data->no_ref);
        $sheet->setCellValue('P3', $data->getBranch['name']);
        $sheet->setCellValue('P4', SiteHelpers::dmy($data->created_at));
        $sheet->setCellValue('D5', $data->nama_penelpon);
        $sheet->setCellValue('D6', $data->no_telp);
        $sheet->setCellValue('D7', $data->jam_komplain);
        $sheet->setCellValue('E9', $dataCust->no_pelanggan);
        // $sheet->setCellValue('M9', $dataCust->outlet_name);
        // $sheet->setCellValue('E10', $dataCust->realname);
        $sheet->setCellValue('M9', $dataCust->realname);
        $sheet->setCellValue('E10', $dataCust->outlet_name);
        $sheet->setCellValue('M10', $dataCust->salesname);
        $sheet->setCellValue('E11', $dataCust->alamat);
        $sheet->setCellValue('M11', $dataCust->spv_sales);
        $sheet->setCellValue('E15', $data->getComplain['name']);
        $sheet->setCellValue('B18', $data->description);
        if(count($dt) > 0) {
            $item = 21;
            foreach ($dt as $key => $var) {
                $sheet->setCellValue('B'.$item, $var->item);
                $sheet->setCellValue('I'.$item, $var->qty);
            }
        }
        if(count($analisa) > 0) {
            foreach ($analisa as $item => $lisa) {
                $analisa_masalah[] = $lisa->analisa_masalah;
            }
            $sheet->setCellValue('B25', implode("\n",$analisa_masalah) ?? '');
        } else {
            $sheet->setCellValue('B25', $data->analisa_masalah ?? '');
        }

        if (count($action) > 0) {
            $aksiTanggal = 29;
            $cs = 31;
            $bm = 32;
            $status = 30;
            foreach ($action as $key => $var) {
                $tglindo = date_create($var->trx_date);

                $sheet->setCellValue('F'.$aksiTanggal, date_format($tglindo,"d M Y"));
                $sheet->setCellValue('C'.$cs, "CS :".$var->cs_action ?? '');

                $sheet->setCellValue('Q'.$status, TicketAction::status[$var->cs_status] ?? '');
                $aksiTanggal+4;
                $cs+4;
                $bm+4;
            }
        }
        foreach($action as $key => $bm) {
            $updatedat = date_create($bm->updated_at);

            $sheet->setCellValue('F33', date_format($updatedat,"d M Y"));
            $sheet->setCellValue('C35', "BM :".$var->bm_action ?? '');
            $sheet->setCellValue('Q33', TicketAction::status[$var->bm_status] ?? '');
        }

        $sheet->setCellValue('B38', $data->hasil_investigasi);
        $sheet->setCellValue('B41', $data->tindak_pencegahan);
        $sheet->setCellValue('I47', SiteHelpers::dmy($data->created_at));
        $sheet->setCellValue('N47', SiteHelpers::dmy($data->updated_at));
        $sheet->setCellValue('U47', SiteHelpers::dmy($data->updated_at));
        $sheet->setCellValue('I48', SiteHelpers::getName($data->created_by)->name);
        $sheet->setCellValue('N48', SiteHelpers::getBranchUser($data->branch_code)->name);

        $spreadsheet->getActiveSheet()->getStyle('E11')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B18')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B25')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('B38')->getAlignment()->setWrapText(true);
        $writer = IOFactory::createWriter($spreadsheet, "Xlsx");
        $file_name = time() . '.xlsx';
        $writer->save($file_name);
        header('Content-Type: application/x-www-form-urlencoded');
        header('Content-Transfer-Encoding: utf-8');
        header("Content-disposition: attachment; filename=\"".$file_name."\"");
        readfile($file_name);
        unlink($file_name);
        exit;
    }
}

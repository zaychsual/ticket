<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;
use App\Models\Menu;
use App\Models\AccessMenu;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = Role::paginate(2);
        $data = Role::all();

        return view('pages.role.index', compact('data'));
    }

    private function _validation(Request $request)
    {
        $validation = $request->validate(
            [
                'name' => 'required|min:3',
            ],
            [
                'name.required' => 'Cannot be null!',
                'name.min' => 'Minimal 3 character',
            ]
        );

    }

    public function create()
    {
        $akses = Menu::all();

        return view('pages.role.create', compact('akses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->_validation($request);

            $store = Role::create([
                'name' => $request->name
            ]);

            if($request->has('menu_id')) {
                foreach($request->menu_id as $key => $row) {
                    $row  = AccessMenu::create([
                        'role_id' => $store->id,
                        'menu_id' => $request->menu_id[$key] ?? 0,
                        'akses' => $request->akses[$key] ?? 0,
                        'add' => $request->add[$key] ?? 0,
                        'edit' => $request->edit[$key] ?? 0,
                        'destroy' => $request->destroy[$key] ?? 0,
                    ]);
                }
            }
            DB::commit();

            return redirect()->route('admin.role.index');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $cek = AccessMenu::where('role_id', $role->id)->get();

        if($cek->isEmpty()) {
            $akses = Menu::all();

            $result = $akses;
        } else {
            $akses = Menu::join('access_menus', 'menus.id', '=', 'access_menus.menu_id')
                ->where('access_menus.role_id', $role->id)
                ->select('menus.id' ,'menus.name as menu' , 'access_menus.role_id','access_menus.id as akses_id' ,'access_menus.akses' ,'access_menus.add' ,'access_menus.edit' ,'access_menus.destroy');
            $nonAkses = Menu::leftJoin('access_menus', 'menus.id', '=', 'access_menus.menu_id')
                    ->whereRaw('menus.id not in (select menu_id from access_menus where role_id='.$role->id.')')
                    ->select('menus.id' ,'menus.name as menu' , 'access_menus.role_id','access_menus.id as akses_id' ,'access_menus.akses' ,'access_menus.add' ,'access_menus.edit' ,'access_menus.destroy')
                    ->union($akses)
                    ->get();

            $result = $nonAkses;
        }

        return view('pages.role.show', compact('role', 'result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $cek = AccessMenu::where('role_id', $role->id)->get();

        if($cek->isEmpty()) {
            $akses = Menu::all();

            $result = $akses;
        } else {
            $akses = Menu::join('access_menus', 'menus.id', '=', 'access_menus.menu_id')
                ->where('access_menus.role_id', $role->id)
                ->select('menus.id' ,'menus.name as menu' , 'access_menus.role_id','access_menus.id as akses_id' ,'access_menus.akses' ,'access_menus.add' ,'access_menus.edit' ,'access_menus.destroy');
            $nonAkses = Menu::leftJoin('access_menus', 'menus.id', '=', 'access_menus.menu_id')
                    ->whereRaw('menus.id not in (select menu_id from access_menus where role_id='.$role->id.')')
                    ->select('menus.id' ,'menus.name as menu' , 'access_menus.role_id','access_menus.id as akses_id' ,'access_menus.akses' ,'access_menus.add' ,'access_menus.edit' ,'access_menus.destroy')
                    ->union($akses)
                    ->get();

            $result = $nonAkses;
        }

        return view('pages.role.edit', compact('result', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        DB::beginTransaction();
        try {
            $this->_validation($request);

            $update = Role::findOrFail($role->id);
            $update->name = $request->name;
            $update->update();
            if($request->has('menu_id')) {
                foreach($request->menu_id as $key => $row) {
                    if(isset($request->akses_id[$key])) {
                        $row = AccessMenu::findOrFail($request->akses_id[$key]);
                        $row->akses = $request->akses[$key] ?? 0;
                        $row->add = $request->add[$key] ?? 0;
                        $row->edit = $request->edit[$key] ?? 0;
                        $row->destroy = $request->destroy[$key] ?? 0;
                        $row->update();
                    } else {
                        $row  = AccessMenu::create([
                            'role_id' => $role->id,
                            'menu_id' => $request->menu_id[$key] ?? 0,
                            'akses' => $request->akses[$key] ?? 0,
                            'add' => $request->add[$key] ?? 0,
                            'edit' => $request->edit[$key] ?? 0,
                            'destroy' => $request->destroy[$key] ?? 0,
                        ]);
                    }
                }
            }
            DB::commit();

        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->update(['updated_by' => Auth::user()->id]);
        $role->delete();

        return back();
    }
}

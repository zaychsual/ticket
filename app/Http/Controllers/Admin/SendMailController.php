<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\TryEmail;

class SendMailController extends Controller
{
    public function send()
    {
        $details = [
            'title' => 'Email dari E-ticket',
            'body'  => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit deserunt laborum dolores at officia blanditiis commodi cupiditate nobis, totam quibusdam ullam numquam hic dolorem fugiat exercitationem consectetur, quia praesentium quas!'
        ];

        try {
            \Mail::to('aziz.zaind@yahoo.com')
            ->cc(['dina.novianti@fastratabuana.co.id', 'pelanggan@fastratabuana.co.id', 'novita.kusuma@fastratabuana.co.id'])
            ->send(new TryEmail($details));

        } catch (\Exception $th) {
            echo "Email gagal dikirim karena $th.";
        }
    }
}

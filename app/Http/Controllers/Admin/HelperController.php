<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;

class HelperController extends Controller
{
    public function custDetail(Request $request)
    {
        $data = Customer::leftJoin('branches', DB::Raw("left(customers.no_pelanggan, 4)"), '=', 'branches.code')
                ->join('type_outlets', 'customers.type_outlet_id', '=', 'type_outlets.id')
                ->where('customers.id', $request->val)
                ->select('branches.code' ,'branches.name as branch', 'type_outlets.name as type_outlet', 'branches.id as branch_id', 'customers.*')
                ->first();

        return \Response::json($data);
    }

    public function selectCust(Request $request)
    {
        $data = [];

        if($request->has('search')) {
            $data = Customer::where('outlet_name', 'LIKE', '%'.strtoupper($request->search).'%')
                ->orWhere('no_pelanggan', 'LIKE', '%'.strtoupper($request->search).'%')
                ->select('id', 'no_pelanggan', 'outlet_name')
                ->get();
        }

        return \Response::json($data);
    }

}

<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Branch;
use App\Models\Role;
use App\Helpers\SiteHelpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(SiteHelpers::cekRole(1)) {
            $data = User::all();
        } else {
            $data = User::where('id', Auth::user()->id)->get();
        }

        return view('pages.user.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branch = Branch::select('name','code', 'id')->get();
        $role = Role::all()->pluck('name', 'id');

        return view('pages.user.create', compact('branch', 'role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    private function _validation(Request $request)
    {
        $validation = $request->validate(
            [
                'name' => 'required|max:255',
                'username' => 'required|unique:users',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'c_password' => 'required|same:password',
                'role_id'   => 'required',
            ],
            [
                'name.required' => 'Cannot be null!',
                'username.required' => 'Cannot be null!',
                'email.required' => 'Cannot be null!',
                'password.required' => 'Cannot be null!',
                'role_id.required' => 'Cannot be null!',
            ]
        );

    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            // dd($request);
            $this->_validation($request);
            if($request->role_id != 3) {
                $store = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'username' => $request->username,
                    'password' => Hash::make($request->password),
                    'role_id' => $request->role_id,
                ]);
            } else {
                $store = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'username' => $request->username,
                    'password' => Hash::make($request->password),
                    'role_id' => $request->role_id,
                    'branch_code' => implode(',', $request->branch_id ?? ''),
                    'email_bom' => $request->email_bom ?? '',
                    'email_admin' => $request->email_admin ?? '',
                ]);
            }
            DB::commit();

            return redirect()->route('admin.user.index');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $branch = Branch::select('name','code', 'id')->get();
        $role = Role::all()->pluck('name', 'id');

        return view('pages.user.show', compact('branch', 'role', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $branch = Branch::select('name','code', 'id')->get();
        $role = Role::all()->pluck('name', 'id');

        return view('pages.user.edit', compact('branch', 'role', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    private function _updatevalidation(Request $request)
    {
        $validation = $request->validate(
            [
                'name' => 'required|max:255',
                'username' => 'required',
                'email' => 'required|email',
            ],
            [
                'name.required' => 'Cannot be null!',
                'username.required' => 'Cannot be null!',
                'email.required' => 'Cannot be null!',
            ]
        );

    }

    public function update(Request $request, User $user)
    {
        DB::beginTransaction();
        try {
            $this->_updatevalidation($request);

            $update = User::findOrFail($user->id);
            $update->name = $request->name;
            $update->username = $request->username;
            $update->email = $request->email;
            if($request->password != '' || $request->password != null) {
                $update->password = Hash::make($request->password);
            }
            if($request->has('role_id')) {
                $update->role_id = $request->role_id;
            }
            if ($request->role_id == 3) {
                $update->branch_code = implode(',', $request->branch_id ?? '');
                $update->email_bom = $request->email_bom ?? '';
                $update->email_admin = $request->email_admin ?? '';
            }
            $update->update();

            DB::commit();

        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $del = User::findOrFail($user->id);
        $del->deleted_at = Carbon::now();
        $del->update();

        return back();
    }
}

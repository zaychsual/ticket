<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Branch::all();

        return view('pages.branch.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $region = Region::all()->pluck('name', 'id');

        return view('pages.branch.create', compact('region'));
    }

    private function _validation(Request $request)
    {
        $validation = $request->validate(
            [
                'region_id' => 'required',
                'code' => 'required|max:4',
                'name' => 'required|min:3',
            ],
            [
                'region_id.required' => 'Cannot be null!',
                'code.required' => 'Cannot be null!',
                'name.required' => 'Cannot be null!',
            ]
        );

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->_validation($request);

            $store = Branch::create($request->all());
            DB::commit();

            return redirect()->route('admin.branch.index');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        $region = Region::all()->pluck('name', 'id');
        $branch = Branch::findOrFail($branch->id);

        return view('pages.branch.show', compact('branch', 'region'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit(Branch $branch)
    {
        $region = Region::all()->pluck('name', 'id');

        return view('pages.branch.edit', compact('branch', 'region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {
        DB::beginTransaction();
        try {
            $this->_validation($request);

            $update = Branch::findOrFail($branch->id);
            $update->region_id = $request->region_id;
            $update->code = $request->code;
            $update->name = $request->name;
            $update->update();

            DB::commit();

        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        $del = Branch::findOrFail($branch->id);
        $del->deleted_at = Carbon::now();
        $del->update();

        return back();
    }
}

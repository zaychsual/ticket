<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Helpers\SiteHelpers;

class DashboardController extends Controller
{
    public function index()
    {
        if(SiteHelpers::cekRole(1)) {
            $open = Ticket::where('main_status_id', 300)->count();
            $progress = Ticket::where('main_status_id', 102)->count();
            $revised = Ticket::where('main_status_id', 103)->count();
            $closed = Ticket::where('main_status_id', 200)->count();
        } elseif (SiteHelpers::cekRole(2)) {
            $open = Ticket::where('main_status_id', 300)
                ->where('created_by', Auth::user()->id)
                ->count();
            $progress = Ticket::where('main_status_id', 102)
                ->where('created_by', Auth::user()->id)
                ->count();
            $revised = Ticket::where('main_status_id', 103)
                ->where('created_by', Auth::user()->id)
                ->count();
            $closed = Ticket::where('main_status_id', 200)
                ->where('created_by', Auth::user()->id)
                ->count();
        } elseif(SiteHelpers::cekRole(3)) {
            $open = Ticket::where('main_status_id', 300)
                ->whereIn('branch_code', explode(",",Auth::user()->branch_code ?? ''))
                ->count();
            $progress = Ticket::where('main_status_id', 102)
                ->whereIn('branch_code', explode(",",Auth::user()->branch_code ?? ''))
                ->count();
            $revised = Ticket::where('main_status_id', 103)
                ->whereIn('branch_code', explode(",",Auth::user()->branch_code ?? ''))
                ->count();
            $closed = Ticket::where('main_status_id', 200)
                ->whereIn('branch_code', explode(",",Auth::user()->branch_code ?? ''))
                ->count();
        }


        return view('pages.dashboard', compact('open', 'progress', 'closed', 'revised'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\VariableComplain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ComplainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = Role::paginate(2);
        $data = VariableComplain::orderBy('name')->get();

        return view('pages.complain.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            // $this->_validation($request);

            $store = VariableComplain::create($request->all());
            DB::commit();

            return redirect()->route('admin.complain.index');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VariableComplain  $variableComplain
     * @return \Illuminate\Http\Response
     */
    public function show(VariableComplain $variableComplain)
    {
        return view('pages.complain.show', compact('variableComplain'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VariableComplain  $variableComplain
     * @return \Illuminate\Http\Response
     */
    public function edit(VariableComplain $id)
    {
        $edit = VariableComplain::findOrFail($id);

        return view('pages.complain.edit', compact('edit'));
    }

    public function editAlterNative($id)
    {
        $edit = VariableComplain::findOrFail($id);

        return view('pages.complain.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VariableComplain  $variableComplain
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VariableComplain $variableComplain)
    {
        DB::beginTransaction();
        try {
            // $this->_validation($request);

            $update = VariableComplain::findOrFail($request->id);
            $update->name = $request->name;
            $update->update();

            DB::commit();

        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VariableComplain  $variableComplain
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = VariableComplain::findOrFail($id);
        $del->deleted_at = Carbon::now();
        $del->update();

        return back();
    }
}

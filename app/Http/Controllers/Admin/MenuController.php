<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Menu;
use App\Models\AccessMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Menu::all();

        return view('pages.menu.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $main = Menu::where('level_menu', 'main_menu')->pluck('name', 'id');

        return view('pages.menu.create', compact('main'));
    }

    private function _validation(Request $request)
    {
        $validation = $request->validate(
            [
                'name' => 'required',
                'level_menu' => 'required',
                'no_urut' => 'required',
            ],
            [
                'name.required' => 'Cannot be null!',
                'level_menu.required' => 'Cannot be null!',
                'no_urut.required' => 'Cannot be null!',
            ]
        );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->_validation($request);

            $store = Menu::create($request->all());
            DB::commit();

            return redirect()->route('admin.branch.index');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        $main = Menu::where('level_menu', 'main_menu')->pluck('name', 'id');

        return view('pages.menu.show', compact('menu', 'main'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        $main = Menu::where('level_menu', 'main_menu')->pluck('name', 'id');

        return view('pages.menu.edit', compact('menu', 'main'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        DB::beginTransaction();
        try {
            $this->_validation($request);

            $update = Menu::findOrFail($menu->id);
            $update->name = $request->name;
            $update->level_menu = $request->level_menu;
            $update->master_menu = $request->master_menu;
            $update->no_urut = $request->no_urut;
            $update->menu_aktif = $request->menu_aktif;
            $update->url = $request->url;
            $update->icon = $request->icon;
            $update->update();

            DB::commit();

        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->update(['updated_by' => Auth::user()->id]);
        $menu->delete();

        return back();
    }
}

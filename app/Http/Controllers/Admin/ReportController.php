<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Region;
use App\Models\Ticket;
use App\Models\Branch;
use App\Models\VariableComplain;
use App\Exports\ReportKategoriExport;
use App\Exports\ReportWilayahExport;
use App\Exports\ReportKomplainExport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function reportKomplain()
    {
        return view('pages.report.komplain');
    }

    public function reportKomplainData(Request $request)
    {
        $query = Ticket::selectRaw("CASE
                WHEN ticket_category = 'FRM-CSD-007' THEN 'Complain'
                WHEN ticket_category = 'FRM-CSD-019' THEN 'Non Complain'
            END AS kategori,
            SUM(CASE WHEN main_status_id=300 THEN 1 ELSE 0 END) AS newstatus,
            SUM(CASE WHEN main_status_id=102 THEN 1 ELSE 0 END) AS proses,
            SUM(CASE WHEN main_status_id=103 THEN 1 ELSE 0 END) AS revised,
            SUM(CASE WHEN main_status_id=200 THEN 1 ELSE 0 END) AS closed");
        if($request->start && $request->end) {
            $query->where('created_at', '>=', $request->start.' 00:00:00');
            $query->where('created_at', '<=', $request->end.' 23:00:00');
        }
        if($request->kategori) {
            $query->where('ticket_category', $request->kategori);
        }
        $query->groupBy('ticket_category');
        $data = $query->get();

        return \Response::json($data);
    }

    public function reportKomplainExport(Request $request)
    {
        $data = [
            'start' => $request->start,
            'end' => $request->end,
            'kategori' => $request->kategori,
        ];

        $fileName = 'REPORT_KOMPLAIN'.date('Y-m-d').'_'.date('H:i').'.xlsx';

        return Excel::download(new ReportKomplainExport($data), $fileName);
    }

    public function reportWilayah()
    {
        $data = Branch::all();

        return view('pages.report.wilayah', compact('data'));
    }

    public function reportWilayahData(Request $request)
    {
        $query = Branch::join('tickets', 'branches.code', '=', 'tickets.branch_code')
            ->selectRaw("branches.name as branch,
                COUNT(tickets.id) as total_tiket,
                SUM(CASE WHEN tickets.main_status_id=300 THEN 1 ELSE 0 END) AS newstatus,
                SUM(CASE WHEN tickets.main_status_id=102 THEN 1 ELSE 0 END) AS proses,
                SUM(CASE WHEN tickets.main_status_id=103 THEN 1 ELSE 0 END) AS revised,
                SUM(CASE WHEN tickets.main_status_id=200 THEN 1 ELSE 0 END) AS closed");
        if($request->start && $request->end) {
            $query->where('tickets.created_at', '>=', $request->start.' 00:00:00');
            $query->where('tickets.created_at', '<=', $request->end.' 23:00:00');
        }

        if($request->kategori) {
            $query->where('branches.code', $request->kategori);
        }

        $query->groupBy('branches.name');
        $data = $query->get();

        return \Response::json($data);
    }

    public function reportWilayahExport(Request $request)
    {
        $data = [
            'start' => $request->rentang_tanggal,
            'end' => $request->end,
            'kategori' => $request->kategori,
        ];

        $fileName = 'REPORT_WILAYAH'.date('Y-m-d').'_'.date('H:i').'.xlsx';

        return Excel::download(new ReportWilayahExport($data), $fileName);
    }

    public function reportKategori()
    {
        $data = VariableComplain::all()->pluck('name', 'id');

        return view('pages.report.kategori', compact('data'));
    }

    public function reportKategoriData(Request $request)
    {
        $query = Ticket::join('variable_complains', 'tickets.complain_id', '=', 'variable_complains.id')
            ->selectRaw("variable_complains.name as variable,
                COUNT(tickets.id) as total_tiket,
                SUM(CASE WHEN tickets.main_status_id=300 THEN 1 ELSE 0 END) AS newstatus,
                SUM(CASE WHEN tickets.main_status_id=102 THEN 1 ELSE 0 END) AS proses,
                SUM(CASE WHEN tickets.main_status_id=103 THEN 1 ELSE 0 END) AS revised,
                SUM(CASE WHEN tickets.main_status_id=200 THEN 1 ELSE 0 END) AS closed");
        if($request->start && $request->end) {
            $query->where('tickets.created_at', '>=', $request->start.' 00:00:00');
            $query->where('tickets.created_at', '<=', $request->end.' 23:00:00');
        }
        if($request->kategori) {
            $query->where('tickets.complain_id', $request->kategori);
        }
        $query->groupBy('variable_complains.name');
        $data = $query->get();

        return \Response::json($data);
    }

    public function reportKategoriExport(Request $request)
    {
        $data = [
            'start' => $request->start,
            'end' => $request->end,
            'kategori' => $request->kategori,
        ];

        $fileName = 'REPORT_VARIABLE_COMPLAIN'.date('Y-m-d').'_'.date('H:i').'.xlsx';

        return Excel::download(new ReportKategoriExport($data), $fileName);
    }
}

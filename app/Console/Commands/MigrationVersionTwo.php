<?php

namespace App\Console\Commands;

use Ramsey\Uuid\Uuid as Generator;
use App\Models\Ticket;
use App\Models\TicketAction;
use App\Models\TicketAnalisaProblem;
use App\Models\TicketComplainDetail;
use App\Models\TicketCustomer;
use App\Models\TicketLog;
use App\Models\Customer;
use App\Models\TypeOutlet;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as Reader;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\DB;
use App\Helpers\SiteHelpers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use App\Models\Branch;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class MigrationVersionTwo extends Command
{
    protected $signature = 'migrate:v2';

    protected $description = 'Migrasi Data Konsumen Fastrata nih sama Truncate table';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('99. Truncate Semua table transaksi dan Customer');
        $this->info('1. Import Customer');
        $this->info('2. Import Branch');
        $this->info('3. Import User Branch');
        $this->info('4. Delete Duplicate');
        $this->info('5. Fix Branch');

        $cek = $this->ask('Pilih yang mana ?');

        if( $cek == 1) {
            $this->importCust();
        } elseif($cek == 2) {
            $this->importBranch();
        } elseif ($cek == 3) {
            $this->importEmailBranch();
        } elseif ($cek == 4) {
            $this->info('Sorry belum ada coy');
        } elseif ($cek == 5) {
            $this->fixBranch();
        } elseif($cek == 99 ) {
            $this->info('1.Iya');
            $this->info('2.Tidak');
            $askLagi = $this->ask('Yakin ?');
            if( $askLagi == 1 ) {
                $this->info('truncate table Ticket');
                \App\Models\Ticket::truncate();
                $this->info('done....');
                $this->info('truncate table Ticket Action');
                \App\Models\TicketAction::truncate();
                $this->info('done....');
                $this->info('truncate table Ticket Analisa Problem');
                \App\Models\TicketAnalisaProblem::truncate();
                $this->info('done....');
                $this->info('truncate table Ticket Complain Detail');
                \App\Models\TicketComplainDetail::truncate();
                $this->info('done....');
                $this->info('truncate table Tiket Customer');
                \App\Models\TicketCustomer::truncate();
                $this->info('done....');
                $this->info('truncate table Tiket Log');
                \App\Models\TicketLog::truncate();
                $this->info('done....');
                /* $this->info('truncate table Customer');
                \App\Models\Customer::truncate();
                $this->info('done....'); */
                $this->info('truncate table Customer temp');
                DB::table('customer_temps')->truncate();
                $this->info('done....');
            }
        } else {
            $this->info('Pilih dulu yang mana -_-');
        }
        $this->info('Kelar bosque');
    }

    public function importCust()
    {
        DB::beginTransaction();
        try {
            ini_set('memory_limit', '-1');
            Schema::disableForeignKeyConstraints();
            Customer::query()->truncate();
            Schema::enableForeignKeyConstraints();
            for($i=1;$i<5;$i++) {
                $this->info('.................=gw Baca Data nya Dulu coy=........................');
                $importxls = public_path().'/importxls/AREANOM'.$i.'.xlsx';
                if (file_exists($importxls)) {
                    $spreadsheet = IOFactory::load($importxls);
                    $sheet = $spreadsheet->getActiveSheet()->toArray();
                    for($j=1;$j<count($sheet);$j++) {
                        $no_pelanggan = $sheet[$j]['0'];
                        $realname = $sheet[$j]['2'];
                        $nik = $sheet[$j]['6'];
                        $outlet_name = $sheet[$j]['1'];
                        $phone_no = $sheet[$j]['4'];
                        $alamat = $sheet[$j]['3'];
                        $kode_sales = $sheet[$j]['9'];
                        $salesname = $sheet[$j]['10'];
                        $type_outlet = SiteHelpers::getOutlet($sheet[$j]['5'])->id ?? '';
                        $this->info('.................=Proses Masukin ke DB Nih Tungguin ya, JANGAN DI CLOSE!!=........................');
                        // $check = Customer::where('no_pelanggan', $no_pelanggan)->first();
                        // if(!$check) {
                            $cust = DB::table('customers')->insert([
                                'id' => Generator::uuid4()->toString(),
                                'no_pelanggan' => $no_pelanggan,
                                'realname' => strtoupper($realname),
                                'nik' => $nik,
                                'outlet_name' => strtoupper($outlet_name),
                                'phone_no' => $phone_no,
                                'alamat' => $alamat,
                                'kode_sales' => $kode_sales,
                                'salesname' => $salesname,
                                'type_outlet_id' => $type_outlet,
                                'created_at' => Carbon::now(),
                                'created_by' => '8924c6fa-3065-4541-8609-8bc1b86a1349',
                            ]);
                        // }
                    }
                } else {
                    $this->info('.................=File nya GA ada nih,gimana sih??=........................');
                }
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function fixBranch()
    {
        DB::beginTransaction();
        try {
            ini_set('memory_limit', '-1');
            $this->info('.................=gw Baca Data nya Dulu coy=........................');
            $importxls = public_path().'/importxls/FIXBRANCH.xlsx';
            if (file_exists($importxls)) {
                $spreadsheet = IOFactory::load($importxls);
                $sheet = $spreadsheet->getActiveSheet()->toArray();
                for($j=1;$j<count($sheet);$j++) {
                    $id = $sheet[$j]['1'];
                    $name = $sheet[$j]['2'];
                    $this->info('.................=Proses Masukin ke DB Nih Tungguin ya, JANGAN DI CLOSE!!=........................');
                    // DB::table('users')->where('id', $id)->update('name', $name);
                    $branch = User::where('username', $id);
                    $branch->update(['name' => $name]);
                }
                DB::commit();
            } else {
                $this->info('.................=File nya GA ada nih,gimana sih??=........................');
            }
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
            $this->info(".................=Yah gw ga import nih, error ini = {$th}........................");
        }
    }

    public function importBranch()
    {
        DB::beginTransaction();
        try {
            ini_set('memory_limit', '-1');
            $this->info('.................=gw Baca Data nya Dulu coy=........................');
            $importxls = public_path().'/importxls/BRANCH.xlsx';

            if (file_exists($importxls)) {
                //delete data nya dulu
                Schema::disableForeignKeyConstraints();
                Branch::query()->truncate();
                Schema::enableForeignKeyConstraints();
                if(env('DB_CONNECTION') == 'pgsql') {
                    DB::statement('ALTER SEQUENCE branches_id_seq RESTART WITH 1;');
                } elseif (env('DB_CONNECTION') == 'mysql') {
                    DB::statement('ALTER TABLE branches AUTO_INCREMENT = 1;');
                }
                $spreadsheet = IOFactory::load($importxls);
                $sheet = $spreadsheet->getActiveSheet()->toArray();
                for($j=1;$j<count($sheet);$j++) {
                    $code = $sheet[$j]['1'];
                    $name = $sheet[$j]['0'];
                    $this->info('.................=Proses Masukin ke DB Nih Tungguin ya, JANGAN DI CLOSE!!=........................');
                    $branch = Branch::create([
                        'code' => $code,
                        'name' => $name,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => '8924c6fa-3065-4541-8609-8bc1b86a1349',
                    ]);
                }
                DB::commit();
            } else {
                $this->info('.................=File nya GA ada nih,gimana sih??=........................');
            }
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
            $this->info(".................=Yah gw ga import nih, error ini = {$th}........................");
        }
    }

    public function importEmailBranch()
    {
        DB::beginTransaction();
        try {
            ini_set('memory_limit', '-1');
            $this->info('.................=gw Baca Data nya Dulu coy=........................');
            $importxls = public_path().'/importxls/BRANCHEMAIL.xlsx';
            DB::table('users')->where('role_id', 3)->delete();
            if (file_exists($importxls)) {

                $spreadsheet = IOFactory::load($importxls);
                $sheet = $spreadsheet->getActiveSheet()->toArray();
                for($j=1;$j<count($sheet);$j++) {
                    $name = $sheet[$j]['0'];
                    $username = $sheet[$j]['1'];
                    $email = $sheet[$j]['2'];
                    $email_bom = $sheet[$j]['3'];
                    $email_admin = $sheet[$j]['4'];
                    $code = $sheet[$j]['5'];
                    // dd($code);
                    $this->info('.................=Proses Masukin ke DB Nih Tungguin ya, JANGAN DI CLOSE!!=........................');
                    $user = User::create([
                        'name' => $name,
                        'email' => $email,
                        'username' => $username,
                        'email_verified_at' => Carbon::now(),
                        'password' => Hash::make('password'),
                        'remember_token' => Str::random(10),
                        'role_id' => 3,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                        'email_bom' => $email_bom,
                        'email_admin' => $email_admin,
                        'branch_code' => $code,
                        'created_by' => '8924c6fa-3065-4541-8609-8bc1b86a1349',
                    ]);
                }
                DB::commit();
            } else {
                $this->info('.................=File nya GA ada nih,gimana sih??=........................');
            }
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
            $this->info(".................=Yah gw ga import nih, error ini = {$th}........................");
        }
    }

    public function delDuplicate()
    {
        DB::beginTransaction();
        try {
            $this->info('.................=OK GW Coba Benerin dulu ya=........................');
            $dup = DB::table('customers')
                    ->select('no_pelanggan', DB::raw('count(`no_pelanggan`) as nopelanggan'))
                    ->groupBy('no_pelanggan')
                    ->having('nopelanggan', '>', 1)
                    ->get();
            foreach ($dup as $raw) {
                $this->info('.................=Lagi Proses nih satu-satu, tungguin ya JANGAN DI CLOSE!!=........................');
                $exclude = DB::table('customers')
                    ->where('no_pelanggan', $raw->no_pelanggan)
                    ->select('id')
                    ->first();
                DB::table('customers')
                    ->where('no_pelanggan', $raw->no_pelanggan)
                    ->where('id', '!=', $exclude->id)
                    ->delete();
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
            $this->info(".................=Yah gw ga bisa benerin coy, error ini = {$th}........................");
        }
    }
}

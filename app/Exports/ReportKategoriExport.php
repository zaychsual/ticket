<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Ticket;

class ReportKategoriExport implements FromCollection
{
    private $data;


    public function __construct($data)
    {
        $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $exportData = collect([]);

        //set header
        $header = [
            'Variabel Komplain',
            'Total Tiket',
            'New Data',
            'Progress',
            'Complete',
        ];

        $exportData->push($header);

        $query = Ticket::join('variable_complains', 'tickets.complain_id', '=', 'variable_complains.id')
            ->selectRaw("variable_complains.name as variable,
                COUNT(tickets.id) as total_tiket,
                SUM(CASE WHEN tickets.main_status_id=300 THEN 1 ELSE 0 END) AS newstatus,
                SUM(CASE WHEN tickets.main_status_id=102 THEN 1 ELSE 0 END) AS proses,
                SUM(CASE WHEN tickets.main_status_id=103 THEN 1 ELSE 0 END) AS revised,
                SUM(CASE WHEN tickets.main_status_id=200 THEN 1 ELSE 0 END) AS closed");
        if($this->data['start'] && $this->data['end']) {
            $query->where('tickets.created_at', '>= '.$this->data['start'].' 00:00:00');
            $query->where('tickets.created_at', '<= '.$this->data['end'].' 23:00:00');
        }
        if($this->data['kategori']) {
            $query->where('tickets.complain_id', $this->data['kategori']);
        }
        $query->groupBy('variable_complains.name');

        $data = $query->get();
        $i = 1;
        foreach($data as $row) {
            $exportData->push([
                $row->variable,
                $row->total_tiket,
                $row->newstatus,
                $row->proses,
                $row->closed
            ]);
            $i++;
        }

        return $exportData;
    }
}

<?php

namespace App\Exports;

use App\Models\Ticket;
use App\Models\Customer;
use App\Models\Branch;
use App\Models\VariableComplain;
use App\Models\TypeOutlet;
use App\Models\TicketCustomer;
use App\Models\TicketAction;
use App\Models\TicketComplainDetail;
use App\Models\TicketAnalisaProblem;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Http\Request;
use Config;

class exportTicket implements FromView
{
    use Exportable;
    protected $data;

    public function __construct($id)
    {
        $this->input = $id;
    }

    public function view(): View
    {
        $data = $this->input;

        if($this->input['ticket_category'] == Ticket::Complain) {
            $dataCust = Customer::findOrFail($this->input['customer_id']);
            $dataBranch = Branch::findOrFail($this->input['branch_id']);
            $variable = VariableComplain::whereIn('category', [VariableComplain::Globals, VariableComplain::Complain])
                    ->select('id', 'name')->get();
        } elseif ($this->input['ticket_category'] == Ticket::NonComplain) {
            $dataCust = TicketCustomer::where('ticket_id', $this->input['id'])->first();
            $branch =  Branch::select('id','code', 'name')->get();
            $outlet = TypeOutlet::all()->pluck('name', 'id');
            $complain = VariableComplain::all()->pluck('name', 'id');
        }
        $dt = TicketComplainDetail::where('ticket_id', $this->input['id'])->get();
        $action = TicketAction::where('ticket_id', $this->input['id'])->get();
        $analisa = TicketAnalisaProblem::where('ticket_id', $this->input['id'])->get();

        if($this->input['ticket_category'] == Ticket::NonComplain) {
            return view('export.ticket.xls-non-complain', compact('data', 'branch', 'dataCust', 'complain','analisa', 'dt', 'action', 'outlet'))->with('no_asset', true);
        } else {
            return view('export.ticket.xls-complain', compact('data', 'dataCust', 'variable','analisa', 'dt', 'action','dataBranch'))->with('no_asset', true);
        }

        // return view('export.ticket.xls-complain', [
        //     'cuk' => $this->input,
        // ]);
    }
}

<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Ticket;

class ReportKomplainExport implements FromCollection
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $exportData = collect([]);

        //set header
        $header = [
            'Tiket Kategori',
            'New Data',
            'Progress',
            'Complete',
        ];

        $exportData->push($header);

        $query = Ticket::selectRaw("CASE
                WHEN ticket_category = 'FRM-CSD-007' THEN 'Complain'
                WHEN ticket_category = 'FRM-CSD-019' THEN 'Non Complain'
            END AS kategori,
            SUM(CASE WHEN main_status_id=300 THEN 1 ELSE 0 END) AS newstatus,
            SUM(CASE WHEN main_status_id=102 THEN 1 ELSE 0 END) AS proses,
            SUM(CASE WHEN main_status_id=103 THEN 1 ELSE 0 END) AS revised,
            SUM(CASE WHEN main_status_id=200 THEN 1 ELSE 0 END) AS closed");
        if($this->data['start'] && $this->data['end']) {
            $query->where('created_at', '>= '.$this->data['start'].' 00:00:00');
            $query->where('created_at', '<= '.$this->data['end'].' 23:00:00');
        }
        if($this->data['kategori']) {
            $query->where('ticket_category', $this->data['kategori']);
        }
        $query->groupBy('ticket_category');
        $data = $query->get();
        $i = 1;
        foreach($data as $row) {
            $exportData->push([
                $row->kategori,
                $row->newstatus,
                $row->proses,
                $row->closed
            ]);
            $i++;
        }

        return $exportData;
    }
}

<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Branch;
use Illuminate\Support\Facades\DB;

class ReportWilayahExport implements FromCollection
{
    private $data;


    public function __construct($data)
    {
        $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $exportData = collect([]);

        //set header
        $header = [
            'Branch',
            'Total Tiket',
            'New Data',
            'Progress',
            'Complete',
        ];

        $exportData->push($header);

        $query = Branch::join('tickets', 'branches.code', '=', 'tickets.branch_code')
            ->selectRaw("branches.name as branch,
                COUNT(tickets.id) as total_tiket,
                SUM(CASE WHEN tickets.main_status_id=300 THEN 1 ELSE 0 END) AS newstatus,
                SUM(CASE WHEN tickets.main_status_id=102 THEN 1 ELSE 0 END) AS proses,
                SUM(CASE WHEN tickets.main_status_id=103 THEN 1 ELSE 0 END) AS revised,
                SUM(CASE WHEN tickets.main_status_id=200 THEN 1 ELSE 0 END) AS closed");

        if($this->data['start'] && $this->data['end']) {
            $query->where('tickets.created_at', '>=', $this->data['start'].' 00:00:00');
            $query->where('tickets.created_at', '<=', $this->data['end'].' 23:00:00');
        }

        if($this->data['kategori']) {
            $query->where('branches.code', $this->data['kategori']);
        }
        $query->groupBy('branches.name');
        $data = $query->get();
        $i = 1;
        foreach($data as $row) {
            $exportData->push([
                $row->branch,
                $row->total_tiket,
                $row->newstatus,
                $row->proses,
                $row->closed
            ]);
            $i++;
        }

        return $exportData;
    }
}
